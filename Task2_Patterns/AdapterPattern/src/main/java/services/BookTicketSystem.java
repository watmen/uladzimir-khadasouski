package services;

public class BookTicketSystem implements BookTicket {


    private static final int PRICE_OF_TICKET = 100;

    public BookTicketSystem(){}

    public void getPrice() {
        System.out.println("Price of the ticket = " + PRICE_OF_TICKET);
    }

    public void validate(){
        System.out.println("You can order ticket");
    }

    public void bookTicket(){
        System.out.println("Ticket booked");
    }
}
