package services;

public interface BookTicket {
    void getPrice();
    void validate();
    void bookTicket();
}
