package services;

public interface NewBookTicket {
    void calculate();
    void validateTicket();
    void book();
}
