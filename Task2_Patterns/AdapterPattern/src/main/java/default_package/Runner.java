package default_package;

import adapters.BookTicketAdapter;
import services.BookTicket;

public class Runner {

    public static void main(String[] args){
        BookTicket bookTicket = new BookTicketAdapter();

        bookTicket.validate();
        bookTicket.getPrice();
        bookTicket.bookTicket();
    }
}
