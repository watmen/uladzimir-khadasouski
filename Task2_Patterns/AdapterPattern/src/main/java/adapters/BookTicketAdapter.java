package adapters;

import services.BookTicket;
import services.NewBookTicketSystem;

public class BookTicketAdapter implements BookTicket {
    private NewBookTicketSystem newBookTicketSystem = new NewBookTicketSystem();


    public void getPrice() {
        newBookTicketSystem.calculate();
    }

    public void validate(){
        newBookTicketSystem.validateTicket();
    }

    public void bookTicket(){
        newBookTicketSystem.book();
    }
}
