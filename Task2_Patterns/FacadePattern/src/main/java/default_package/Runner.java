package default_package;

import services.Delivery;
import services.Order;
import services.Shop;
import services.Storage;

public class Runner {
    public static void main(String[] args){
        Shop shop = new Shop();
        Delivery delivery = new Delivery();
        Storage storage = new Storage();

        Order order = new Order(storage,shop,delivery);

        order.makeOrder();
    }
}
