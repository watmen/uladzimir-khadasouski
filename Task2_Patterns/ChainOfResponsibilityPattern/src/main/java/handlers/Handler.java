package handlers;

public abstract class Handler {
    private Handler nextHandler;


    public Handler linkWith(Handler nextHandler) {
        this.nextHandler = nextHandler;
        return nextHandler;
    }


    public abstract boolean check();


    public boolean checkNext() {
        if (nextHandler == null) {
            return true;
        }
        return nextHandler.check();
    }
}
