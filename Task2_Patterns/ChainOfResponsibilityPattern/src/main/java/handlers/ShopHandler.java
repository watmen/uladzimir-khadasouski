package handlers;

import services.Shop;

public class ShopHandler extends Handler {
    private Shop shop;

    public ShopHandler(Shop shop) {
        this.shop = shop;
    }

    public boolean check() {
        if(shop.isWork()){
            if(shop.isPayment()){
                return checkNext();
            }
            else {
                System.out.println("Customer doesn't pay");
            }
        }
        else{
            System.out.println("Shop doesn't work");
        }
        return false;
    }
}
