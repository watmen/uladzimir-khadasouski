package handlers;

import services.Storage;

public class StorageHandler extends Handler {
    private Storage storage;

    public StorageHandler(Storage storage) {
        this.storage = storage;
    }

    public boolean check() {
        if (storage.isWork()){
            if (storage.isItemExist()){
                if (storage.isPackagingExist()){
                    if(storage.isTransportExist()){
                        return checkNext();
                    }else {
                        System.out.println("Transport doesn't work");
                    }
                }else{
                    System.out.println("Package isn't exist");
                }
            }else{
                System.out.println("Item doesn't found");
            }
        }else {
            System.out.println("Storage doesn't work");
        }
        return false;
    }
}
