package handlers;

import services.Delivery;

public class DeliveryHandler extends Handler {
    private Delivery delivery;

    public DeliveryHandler(Delivery delivery) {
        this.delivery = delivery;
    }

    public boolean check() {
        if (delivery.isWork()){
            return checkNext();
        }
        else{
            System.out.println("Delivery doesn't work");
        }
        return false;
    }
}
