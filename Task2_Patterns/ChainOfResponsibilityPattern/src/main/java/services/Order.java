package services;

import services.Delivery;
import services.Shop;
import services.Storage;

public class Order {
    private Storage storage;
    private Shop shop;
    private Delivery delivery;

    public Order(Storage storage, Shop shop, Delivery delivery) {
        this.storage = storage;
        this.shop = shop;
        this.delivery = delivery;
    }

    public void makeOrder(){
        storage.deliveredToStorage();
        storage.wrapUp();
        shop.deliveredToShop();
        shop.payment();
        delivery.deliveredToCustomer();
    }
}
