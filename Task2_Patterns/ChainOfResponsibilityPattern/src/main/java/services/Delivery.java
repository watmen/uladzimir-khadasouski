package services;

public class Delivery {
    private boolean isWork;

    public Delivery(boolean isWork) {
        this.isWork = isWork;
    }

    public boolean isWork() {
        return isWork;
    }

    public void deliveredToCustomer(){
        System.out.println("Your item delivered to your house");
    }
}
