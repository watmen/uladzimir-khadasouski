package services;

public class Shop {
    private boolean isWork;
    private boolean isPayment;

    public Shop(boolean isWork, boolean isPayment) {
        this.isWork = isWork;
        this.isPayment = isPayment;
    }

    public boolean isWork() {
        return isWork;
    }

    public boolean isPayment() {
        return isPayment;
    }

    public void deliveredToShop(){
        System.out.println("Your item delivered to the shop");
    }

    public void payment(){
        System.out.println("Your item paid");
    }
}
