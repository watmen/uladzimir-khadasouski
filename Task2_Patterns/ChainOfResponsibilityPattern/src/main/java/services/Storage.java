package services;

public class Storage {
    private boolean isWork;
    private boolean isItemExist;
    private boolean isPackagingExist;
    private boolean isTransportExist;

    public Storage(boolean isWork, boolean isItemExist, boolean isPackageExist, boolean isTransportExist) {
        this.isWork = isWork;
        this.isItemExist = isItemExist;
        this.isPackagingExist = isPackageExist;
        this.isTransportExist = isTransportExist;
    }

    public boolean isWork() {
        return isWork;
    }

    public boolean isItemExist() {
        return isItemExist;
    }

    public boolean isPackagingExist() {
        return isPackagingExist;
    }

    public boolean isTransportExist() {
        return isTransportExist;
    }

    public void deliveredToStorage(){
        System.out.println("Your item delivered to the storage");
    }

    public void wrapUp(){
        System.out.println("Your item wrapped up");
    }
}
