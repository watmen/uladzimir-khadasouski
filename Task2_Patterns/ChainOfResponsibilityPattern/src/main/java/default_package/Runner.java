package default_package;

import handlers.DeliveryHandler;
import handlers.Handler;
import handlers.ShopHandler;
import handlers.StorageHandler;
import services.Order;
import services.Delivery;
import services.Shop;
import services.Storage;

public class Runner {
    public static void main(String[] args){
        Shop shop = new Shop(false,true);
        Delivery delivery = new Delivery(true);
        Storage storage = new Storage(true,true,true,true);

        Order order = new Order(storage,shop,delivery);
        Handler handler = new StorageHandler(storage);
        handler.linkWith(new ShopHandler(shop)).linkWith(new DeliveryHandler(delivery));

        if (handler.check()){
            order.makeOrder();
        }
    }
}

