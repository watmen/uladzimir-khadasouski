package factories;

import drinks.Order;
import supplements.Cream;
import supplements.Milk;
import supplements.Sugar;

public class SupplementTypeFactory {
    public enum TypeOfSupplement{
        SUGAR{
            public Order getDrinkWithSupplement(Order order){
                return new Sugar(order);
            }
        },
        MILK{
            public Order getDrinkWithSupplement(Order order){
                return new Milk(order);
            }
        },
        CREAM{
            public Order getDrinkWithSupplement(Order order){
                return new Cream(order);
            }
        };

        public abstract Order getDrinkWithSupplement(Order order);
    }

    public static Order getDrinkWithSupplement(int typeOfSupplement,Order order){
        int idOfSupplement = typeOfSupplement - 1;
        return TypeOfSupplement.values()[idOfSupplement].getDrinkWithSupplement(order);
    }
}
