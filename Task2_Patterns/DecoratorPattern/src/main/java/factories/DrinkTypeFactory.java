package factories;

import drinks.Coffee;
import drinks.Tea;
import drinks.Order;

public class DrinkTypeFactory {
    public enum TypeOfDrink {
        TEA{
            public Order getDrink(){
                return new Tea();
            }
        },
        COFFEE{public Order getDrink(){
            return new Coffee();
        }};

        public abstract Order getDrink();
    }


    public static Order getDrink(int typeOfDrink){
        int idOfDrink = typeOfDrink - 1;
        return TypeOfDrink.values()[idOfDrink].getDrink();
    }
}
