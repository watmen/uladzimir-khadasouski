package drinks;

public interface Order {
    int getCost();
    String getDescription();
}
