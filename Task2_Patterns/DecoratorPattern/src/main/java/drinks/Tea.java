package drinks;

public class Tea implements Order {

    private static final String MESSAGE_ABOUT_TEA = "It's regular tea";
    private static int COST_OF_REGULAR_TEA = 6;

    public Tea() {
    }


    public int getCost() {
        return COST_OF_REGULAR_TEA;
    }


    public String getDescription() {
        return MESSAGE_ABOUT_TEA;
    }
}
