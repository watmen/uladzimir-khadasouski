package drinks;

public class Coffee implements Order {

    private static final String MESSAGE_ABOUT_COFFEE = "It's regular coffee";
    private static int COST_OF_REGULAR_COFFEE = 10;

    public Coffee() {
    }


    public int getCost() {
        return COST_OF_REGULAR_COFFEE;
    }


    public String getDescription() {
        return MESSAGE_ABOUT_COFFEE;
    }


}
