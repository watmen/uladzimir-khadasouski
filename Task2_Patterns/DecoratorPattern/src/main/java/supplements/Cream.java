package supplements;

import drinks.Order;

public class Cream extends Decorator {

    private static final String MESSAGE_WITH_CREAM = "  with cream";
    private static int COST_OF_CREAM = 4;

    public Cream() {
    }

    public Cream(Order order) {
        super(order);
    }

    @Override
    public int getCost() {
        return order.getCost() + COST_OF_CREAM;
    }

    @Override
    public String getDescription() {
        return order.getDescription() + MESSAGE_WITH_CREAM;
    }
}
