package supplements;

import drinks.Order;

public class Sugar extends Decorator {

    private static final int COST_OF_SUGAR = 2;
    private static final String MESSAGE_WITH_SUGAR = "  with sugar";

    public Sugar() {
    }

    public Sugar(Order order) {
        super(order);
    }

    @Override
    public int getCost() {
        return order.getCost() + COST_OF_SUGAR;
    }

    @Override
    public String getDescription() {
        return order.getDescription() + MESSAGE_WITH_SUGAR;
    }
}
