package supplements;

import drinks.Order;

public abstract class Decorator implements Order {
    protected Order order;

    public Decorator() {
    }

    public Decorator(Order order) {
        this.order = order;
    }

    @Override
    public abstract int getCost();

    @Override
    public abstract String getDescription();
}
