package supplements;

import drinks.Order;

public class Milk extends Decorator {

    private static final String MESSAGE_WITH_MILK = "  with milk";
    private static final int COST_OF_MILK = 3;

    public Milk() {
    }

    public Milk(Order order) {
        super(order);
    }

    @Override
    public int getCost() {
        return order.getCost() + COST_OF_MILK;
    }

    @Override
    public String getDescription() {
        return order.getDescription() + MESSAGE_WITH_MILK;
    }
}
