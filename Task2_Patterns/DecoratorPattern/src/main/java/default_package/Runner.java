package default_package;


import factories.DrinkTypeFactory;
import factories.SupplementTypeFactory;
import drinks.Order;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {
    private static final int POSITIVE_LIMIT = 0;
    private static final int RIGHT_LIMIT_OF_USER_ANSWER= 2;
    private static final int RIGHT_LIMIT_OF_DRINK_ID= 2;
    private static final int RIGHT_LIMIT_OF_SUPPLEMENT_ID= 3;

    private static final String MESSAGE_WRONG_ANSWER_OF_USER = "You input wrong number.\n" +
            "Re-enter, please ";

    private static boolean answerOfUserIsWrong(int answer){
        return answer < POSITIVE_LIMIT || answer > RIGHT_LIMIT_OF_USER_ANSWER;
    }

    private static boolean idOfDrinkIsWrong(int idOfDrink){
        return idOfDrink < POSITIVE_LIMIT || idOfDrink > RIGHT_LIMIT_OF_DRINK_ID;
    }

    private static boolean idOfSupplementIsWrong(int idOfSupplement){
        return idOfSupplement < POSITIVE_LIMIT || idOfSupplement > RIGHT_LIMIT_OF_SUPPLEMENT_ID;
    }

    public static void main(String[] args){
        try {
            Scanner in = new Scanner(System.in);
            System.out.println("What you want to drink: \n" +
                    "1 - tea\n" +
                    "2 - coffee");
            int idOfDrink = in.nextInt();
            while (idOfDrinkIsWrong(idOfDrink)){
                System.out.println("You input wrong id of drink.\n" +
                        "Re-enter, please ");
                idOfDrink = in.nextInt();
            }
            Order order = DrinkTypeFactory.getDrink(idOfDrink);
            System.out.println("Do you want to add some supplement? \n" +
                    "1 - yes\n" +
                    "2 - no");
            int answer = in.nextInt();
            while (answerOfUserIsWrong(answer)){
                System.out.println(MESSAGE_WRONG_ANSWER_OF_USER);
                answer = in.nextInt();
            }
            if (answer == 1){
                do {
                    System.out.println("What supplement you want? \n" +
                            "1 - sugar\n" +
                            "2 - milk\n" +
                            "3 - cream");
                    int  idOfSupplement = in.nextInt();
                    while (idOfSupplementIsWrong(idOfSupplement)){
                        System.out.println("You input wrong id of supplement.\n" +
                                "Re-enter, please ");
                        idOfSupplement = in.nextInt();
                    }
                    order = SupplementTypeFactory.getDrinkWithSupplement(idOfSupplement,order);
                    System.out.println("Do you want other supplement? \n" +
                            "1 - yes\n" +
                            "2 - no");
                    answer = in.nextInt();
                    while (answerOfUserIsWrong(answer)){
                        System.out.println(MESSAGE_WRONG_ANSWER_OF_USER);
                        answer = in.nextInt();
                    }
                }while (answer == 1);
            }
            System.out.println("You order: " + order.getDescription() + "; cost = " + order.getCost() + ";");
        }catch (InputMismatchException e){
            System.err.println("You input wrong dara!!!");
        }

    }
}
