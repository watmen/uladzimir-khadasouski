public class ProfileCaretaker {
    private Object obj;

    public void save(Profile profile){
        this.obj=profile.save();
    }

    public void undo(Profile profile){
        profile.undoToLastSave(obj);
    }
}
