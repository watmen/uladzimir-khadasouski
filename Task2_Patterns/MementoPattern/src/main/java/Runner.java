import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {

    private static final int POSITIVE_LIMIT = 0;
    private static final int RIGHT_LIMIT_OF_USER_ANSWER= 2;

    private static final String MESSAGE_WRONG_ANSWER_OF_USER = "You input wrong number.\n" +
            "Re-enter, please ";

    private static boolean answerOfUserIsWrong(int answer){
        return answer < POSITIVE_LIMIT || answer > RIGHT_LIMIT_OF_USER_ANSWER;
    }

    public static void main(String[] args){
        try {
            Scanner in = new Scanner(System.in);
            ProfileCaretaker profileCaretaker = new ProfileCaretaker();
            Profile profile = new Profile(20,"man","student");
            profileCaretaker.save(profile);
            System.out.println(profile);
            System.out.println("Do you want to change information in your profile? \n" +
                    "1 - yes\n" +
                    "2 - no");
            int answer = in.nextInt();
            while (answerOfUserIsWrong(answer)){
                System.out.println(MESSAGE_WRONG_ANSWER_OF_USER);
                answer = in.nextInt();
            }
            if (answer == 1){
                System.out.println("Add your age: ");
                int age = in.nextInt();
                profile.setAge(age);
                System.out.println("Add your female: ");
                String female = in.next();
                profile.setFemale(female);
                System.out.println("Add some information about you: ");
                String personalInformation = in.next();
                profile.setPersonalInformation(personalInformation);
                System.out.println(profile);
                System.out.println("Do you want to save it? \n" +
                        "1 - yes\n" +
                        "2 - no");
                answer = in.nextInt();
                if(answer == 1){
                    profileCaretaker.save(profile);
                }
                else{
                    profileCaretaker.undo(profile);
                }
            }
            System.out.println(profile);
        }catch (InputMismatchException e){
            System.err.println("You input wrong data !!!");
        }

    }
}
