public class Profile {
    private int age;
    private String female;
    private String personalInformation;

    public Profile(){}
    public Profile(int dateOfBirth, String female, String personalInformation) {
        this.age = dateOfBirth;
        this.female = female;
        this.personalInformation = personalInformation;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFemale() {
        return female;
    }

    public void setFemale(String female) {
        this.female = female;
    }

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public Memento save(){
        return new Memento(this.age,this.female,this.personalInformation);
    }

    public void undoToLastSave(Object obj){
        Memento memento = (Memento) obj;
        this.age = memento.age;
        this.female=memento.female;
        this.personalInformation = memento.personalInformation;
    }

    @Override
    public String toString() {
        return "Your age:  " + age + "\n" +
                "Your female: " + female + "\n" +
                "Some information about you: " + personalInformation;
    }

    private class Memento{
        private int age;
        private String female;
        private String personalInformation;

        public Memento(int dateOfBirth, String female, String personalInformation) {
            this.age = dateOfBirth;
            this.female = female;
            this.personalInformation = personalInformation;
        }
    }
}
