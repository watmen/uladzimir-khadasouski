package default_package;

import accounts.UserAccount;

import java.util.InputMismatchException;
import java.util.Scanner;


public class Runner {

    private static final int POSITIVE_LIMIT = 0;
    private static final int RIGHT_LIMIT_TYPE_OF_REGISTRATION = 2;

    private static final String MESSAGE_CHOOSE_TYPE_OF_REGISTRATION = "Choose type of registration: \n" +
            "1 - fast\n" +
            "2 - normal";
    private static final String MESSAGE_WRONG_TYPE_OF_REGISTRATION = "Please, input right type of registration";

    private static boolean answerOfUserIsWrong(int answer){
        return answer < POSITIVE_LIMIT || answer > RIGHT_LIMIT_TYPE_OF_REGISTRATION;
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        UserAccount account;
        try {
            System.out.println(MESSAGE_CHOOSE_TYPE_OF_REGISTRATION);
            int answer = in.nextInt();
            while (answerOfUserIsWrong(answer)){
                System.out.println(MESSAGE_WRONG_TYPE_OF_REGISTRATION);
                answer = in.nextInt();
            }

            if (answer == 1){
                System.out.println("Add your nickname: ");
                String nickname = in.next();
                System.out.println("Add your password: ");
                String password = in.next();
                System.out.println("Add your mail: ");
                String mail = in.next();
                account = new UserAccount.UserAccountBuilder().
                        setNickname(nickname).
                        setPassword(password).
                        setMailOfUser(mail).
                        build();
                System.out.println(account.shortRegistration());
            }
            else{
                System.out.println("Add your name: ");
                String realNameOfUser = in.next();
                System.out.println("Add your surname: ");
                String realSurnameOfUser = in.next();
                System.out.println("Add your nickname: ");
                String nickname = in.next();
                System.out.println("Add your password: ");
                String password = in.next();
                System.out.println("Add your mail: ");
                String mail = in.next();
                System.out.println("Add your phone number: ");
                String phoneNumberOfUser = in.next();
                System.out.println("Add your age: ");
                int age = in.nextInt();
                account = new UserAccount.UserAccountBuilder().
                        setRealNameOfUser(realNameOfUser).
                        setRealSurnameOfUser(realSurnameOfUser).
                        setNickname(nickname).
                        setPassword(password).
                        setMailOfUser(mail).
                        setPhoneNumberOfUser(phoneNumberOfUser).
                        setAge(age).
                        build();
                System.out.println(account.normalRegistration());
            }

        }catch (InputMismatchException e){
            System.err.println("You input wrong data!!!");
        }


    }
}
