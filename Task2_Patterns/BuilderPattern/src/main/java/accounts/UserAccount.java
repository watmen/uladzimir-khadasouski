package accounts;

public class UserAccount {
    private String nickname;
    private String realNameOfUser;
    private String realSurnameOfUser;
    private String password;
    private String phoneNumberOfUser;
    private String mailOfUser;
    private int age;

    private UserAccount(){}



    public static class UserAccountBuilder {
        private UserAccount userAccount;


        public UserAccountBuilder() {
            userAccount = new UserAccount();
        }

        public UserAccountBuilder setNickname(String nickname){
            userAccount.nickname = nickname;
            return this;
        }
        public UserAccountBuilder setRealNameOfUser(String realNameOfUser){
            userAccount.realNameOfUser = realNameOfUser;
            return this;
        }
        public UserAccountBuilder setRealSurnameOfUser(String realSurnameOfUser){
            userAccount.realSurnameOfUser = realSurnameOfUser;
            return this;
        }
        public UserAccountBuilder setPassword(String password){
            userAccount.password = password;
            return this;
        }
        public UserAccountBuilder setMailOfUser(String mailOfUser){
            userAccount.mailOfUser = mailOfUser;
            return this;
        }
        public UserAccountBuilder setPhoneNumberOfUser(String phoneNumberOfUser){
            userAccount.phoneNumberOfUser = phoneNumberOfUser;
            return this;
        }
        public UserAccountBuilder setAge(int age){
            userAccount.age = age;
            return this;
        }

        public UserAccount build(){
            return userAccount;
        }
    }

    public String shortRegistration() {
        return  "Your nickname: " + nickname + '\n' +
                "Your mail: " + mailOfUser;
    }

    public String normalRegistration() {
        return  "Your nickname: " + nickname + '\n' +
                "Your name: " + realNameOfUser + '\n' +
                "Your surname: " + realSurnameOfUser + '\n' +
                "Your phone number: " + phoneNumberOfUser + '\n' +
                "Your mail: " + mailOfUser + '\n' +
                "Your age: " + age ;
    }
}
