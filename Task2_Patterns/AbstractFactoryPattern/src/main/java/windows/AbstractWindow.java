package windows;

public abstract class AbstractWindow {
    private int height;
    private int width;

    public AbstractWindow(){}
    public AbstractWindow(int height, int width){
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int widht) {
        this.width = widht;
    }

    @Override
    public String toString() {
        return
                "height of window = " + height + ", " +
                "width of window = " + width;
    }
}
