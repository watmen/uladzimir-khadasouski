package factories;

import windows.AbstractWindow;
import works.AbstractWorks;
import windows.PlasticWindow;
import works.HomelWorks;

public class HomelWorksPlasticWindowFactory implements AbstractFactory {
    public AbstractWindow createWindow(int height, int width) {
        return new PlasticWindow(height,width);
    }

    public AbstractWorks createWorks() {
        return new HomelWorks();
    }
}
