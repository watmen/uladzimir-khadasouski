package factories;


import windows.AbstractWindow;
import works.AbstractWorks;
import windows.WoodenWindow;
import works.HomelWorks;

public class HomelWorksWoodenWindowFactory implements AbstractFactory {


    public AbstractWindow createWindow(int height, int width) {
        return new WoodenWindow(height,width);
    }

    public AbstractWorks createWorks() {
        return new HomelWorks();
    }
}
