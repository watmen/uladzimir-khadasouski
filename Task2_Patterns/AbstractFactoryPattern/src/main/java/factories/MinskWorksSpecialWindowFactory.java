package factories;

import windows.AbstractWindow;
import works.AbstractWorks;
import windows.SpecialWindow;
import works.MinskWorks;

public class MinskWorksSpecialWindowFactory implements AbstractFactory {

    public AbstractWindow createWindow(int height, int width) {
        return new SpecialWindow(height,width);
    }

    public AbstractWorks createWorks() {
        return new MinskWorks();
    }
}
