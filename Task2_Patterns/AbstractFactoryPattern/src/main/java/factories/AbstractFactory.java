package factories;

import windows.AbstractWindow;
import works.AbstractWorks;

public interface AbstractFactory {
    AbstractWindow createWindow(int height, int width);
    AbstractWorks createWorks();
}
