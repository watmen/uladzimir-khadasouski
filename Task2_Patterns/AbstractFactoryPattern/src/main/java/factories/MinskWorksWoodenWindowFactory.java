package factories;


import windows.AbstractWindow;
import works.AbstractWorks;
import windows.WoodenWindow;
import works.MinskWorks;

public class MinskWorksWoodenWindowFactory implements AbstractFactory {


    public AbstractWindow createWindow(int height, int width) {
        return new WoodenWindow(height,width);
    }

    public AbstractWorks createWorks() {
        return new MinskWorks();
    }
}
