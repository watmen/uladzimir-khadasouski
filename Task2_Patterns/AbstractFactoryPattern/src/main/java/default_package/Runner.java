package default_package;

import factories.*;
import factories.AbstractFactory;
import windows.AbstractWindow;
import works.AbstractWorks;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Runner {
    private static final int POSITIVE_LIMIT = 0;
    private static final int RIGHT_LIMIT_OF_WORKS_ID = 2;
    private static final int RIGHT_LIMIT_OF_WINDOW_ID = 3;

    private static boolean idOfWorksIsWrong(int idOfWorks){
        return idOfWorks < POSITIVE_LIMIT || idOfWorks > RIGHT_LIMIT_OF_WORKS_ID;
    }

    private static boolean idOfWindowIsWrong(int idOfWorks){
        return idOfWorks < POSITIVE_LIMIT || idOfWorks > RIGHT_LIMIT_OF_WINDOW_ID;
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        AbstractWorks works;
        AbstractWindow window;
        AbstractFactory factory;
        try {
            System.out.println("Input id of works:\n" +
                    "1 - Homel Works\n" +
                    "2 - Minsk Works");
            int idOfWorks = in.nextInt();
            while(idOfWorksIsWrong(idOfWorks)){
                System.out.println("You input wrong id of works\n" +
                        "Re-enter, please");
                idOfWorks = in.nextInt();
            }
            System.out.println("Input id of window: \n" +
                    "1 - wooden window\n" +
                    "2 - plastic window\n" +
                    "3 - special window");
            int idOfWindow = in.nextInt();
            while(idOfWindowIsWrong(idOfWorks)){
                System.out.println("You input wrong id of window\n" +
                        "Re-enter, please");
                idOfWindow = in.nextInt();
            }
            System.out.println("Input height of window: ");
            int heightOfWindow = in.nextInt();
            System.out.println("Input width of window: ");
            int widthOfWindow = in.nextInt();
            if (idOfWorks == 1){
                if (idOfWindow == 1){
                    factory = new HomelWorksWoodenWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
                else if(idOfWindow == 2){
                    factory = new HomelWorksPlasticWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
                else {
                    factory = new HomelWorksSpecialWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
            }
            else{
                if (idOfWindow == 1){
                    factory = new MinskWorksWoodenWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
                else if(idOfWindow == 2){
                    factory = new MinskWorksPlasticWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
                else {
                    factory = new MinskWorksSpecialWindowFactory();
                    works = factory.createWorks();
                    window = factory.createWindow(heightOfWindow,widthOfWindow);
                }
            }
            System.out.println("Works is " + works.getClass().getSimpleName() + "\n" +
                            "Window is " + window.getClass().getSimpleName() + ": " + window);
        }catch (InputMismatchException e){
            System.err.println("You input wrong data !!!");
        }

    }
}
