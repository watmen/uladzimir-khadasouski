package com.epamlabs;

import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    private static final int MIN_TEMPERATURE = -10;
    private static final int MAX_TEMPERATURE = 40;
    private static Logger logger = Logger.getRootLogger();

    public static void main(String[] args) {
        AtomicInteger day = new AtomicInteger(1);
        List<Temperature> temperatureList = Stream.
                generate(()->new Temperature(ThreadLocalRandom.current().nextInt(MIN_TEMPERATURE, MAX_TEMPERATURE),
                        LocalDate.ofYearDay(2019, day.getAndIncrement()))).
                limit(365).
                collect(Collectors.toList());
        temperatureList.forEach(logger::info);
        logger.info("Max temperature for the year: ");
        logger.info(temperatureList.stream().
                max(Comparator.comparing(Temperature::getValue)).get());
        logger.info("Min temperature for the year: ");
        logger.info(temperatureList.stream().
                min(Comparator.comparing(Temperature::getValue)).get());
        logger.info("Average of temperature for the year" +
                temperatureList.stream().map(Temperature::getValue).mapToInt(x->x).average().toString());
        logger.info("Max temperature for each month: ");
        temperatureList.stream().collect(Collectors.groupingBy(x -> x.getLocalDate().getMonth(),
                Collectors.maxBy(Comparator.comparing(Temperature::getValue)))).
                entrySet().
                stream().
                sorted(Comparator.comparing(Map.Entry::getKey)).
                forEach(logger::info);
        logger.info("Min temperature for each month: ");
        temperatureList.stream().collect(Collectors.groupingBy(x -> x.getLocalDate().getMonth(),
                Collectors.minBy(Comparator.comparing(Temperature::getValue)))).
                entrySet().
                stream().
                sorted(Comparator.comparing(Map.Entry::getKey)).
                forEach(logger::info);
        logger.info("Average temperature for each month: ");
        temperatureList.stream().
                collect(Collectors.groupingBy(x -> x.getLocalDate().getMonth(),
                Collectors.summarizingInt(Temperature::getValue))).
                entrySet().
                stream().
                sorted(Comparator.comparing(Map.Entry::getKey)).
                forEach((x)->{
                    logger.info(x.getKey() + ":" + x.getValue().getAverage());
        });
        logger.info("Max temperature for each month: ");
        temperatureList.parallelStream().
                collect(Collectors.toMap(x -> x.getLocalDate().getMonth(),
                        Function.identity(),
                        BinaryOperator.maxBy(Comparator.comparing(Temperature::getValue)))).
                entrySet().
                forEach(logger::info);
    }
}
