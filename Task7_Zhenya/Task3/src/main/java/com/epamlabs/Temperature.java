package com.epamlabs;

import java.time.LocalDate;

public class Temperature {
    private int value;
    private LocalDate localDate;

    public Temperature() {
    }

    public Temperature(int value, LocalDate localDate) {
        this.value = value;
        this.localDate = localDate;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "value=" + value +
                ", localDate=" + localDate +
                '}';
    }
}
