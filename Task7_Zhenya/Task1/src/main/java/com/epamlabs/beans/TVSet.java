package com.epamlabs.beans;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TVSet  {
    private String model;
    private int price;
    private int diagonal;
    private Producer producer;
    private List<String> listOfModel = Arrays.asList("LEA-24K39P", "43LEX-5023/FT2C",
            "Electronics L24S690VКE", "20LEK85T2",
            "24LES90T2", "55ULES85T2 Smart");
    private List<Integer> listOfDiagonal = Arrays.asList(82,60,94,102,107,127);

    public TVSet() {
    }

    public TVSet(Producer producer) {
        this.model = listOfModel.get(ThreadLocalRandom.current().nextInt(0,5));
        this.price = ThreadLocalRandom.current().nextInt(700,1500);
        this.diagonal = listOfDiagonal.get(ThreadLocalRandom.current().nextInt(0,5));
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(int diagonal) {
        this.diagonal = diagonal;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }


    @Override
    public String toString() {
        return "TVSet{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", diagonal=" + diagonal +
                ", producer=" + producer +
                '}';
    }
}
