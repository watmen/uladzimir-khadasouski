package com.epamlabs.beans;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Producer {
    private String name;
    private String address;
    private String owner;
    private List<String> listOfName = Arrays.asList("SAMSUNG", "APPLE",
            "LENOVO", "ASUS",
            "LG", "HUAWEI");
    private List<String> listOfAddress = Arrays.asList("BELARUS", "USA",
            "Japan", "China",
            "UK", "Russia");
    private List<String> listOfOwner = Arrays.asList("VolodNine", "Yahor",
            "Stepan", "Ivan",
            "Anton", "Zhenya",
            "Ilya", "Egor",
            "Edik", "Vlad");


    public Producer() {
        this.name = listOfName.get(ThreadLocalRandom.current().nextInt(0,5));
        this.address = listOfAddress.get(ThreadLocalRandom.current().nextInt(0,5));
        this.owner = listOfOwner.get(ThreadLocalRandom.current().nextInt(0,9));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Producer{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", owner='" + owner + '\'' +
                '}';
    }
}
