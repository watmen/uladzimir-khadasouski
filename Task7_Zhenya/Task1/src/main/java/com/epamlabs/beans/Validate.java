package com.epamlabs.beans;

import org.apache.log4j.Logger;

@FunctionalInterface
public interface Validate<T> {
    boolean validate(T t, T t1);

    default void defaultMethod(Logger logger){
        logger.info("It's default method");
    }

    static void staticMethod(Logger logger){
        logger.info("It's static method");
    }
}
