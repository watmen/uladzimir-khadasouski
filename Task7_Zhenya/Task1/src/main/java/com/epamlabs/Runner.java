package com.epamlabs;

import com.epamlabs.beans.Producer;
import com.epamlabs.beans.TVSet;
import com.epamlabs.beans.Validate;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
    private static final int MIN_NUMBER_OF_TV_SETS = 3;
    private static final int MAX_NUMBER_OF_TV_SETS = 7;
    private static final int DIAGONAL_OF_TV_SETS_FOR_SOME_CONSUMERS = 90;
    private static final double DISCOUNT = 0.90;
    private static Logger logger = Logger.getRootLogger();

    public static void main(String[] args) {
        List<TVSet> tvSetList = Stream.generate(() -> new TVSet(new Producer())).
                limit(ThreadLocalRandom.current().nextInt(MIN_NUMBER_OF_TV_SETS, MAX_NUMBER_OF_TV_SETS)).
                collect(Collectors.toList());
        logger.info("Sorted TV sets");
        tvSetList.stream().
                sorted(Comparator.comparing(TVSet::getPrice).thenComparing(TVSet::getDiagonal)).forEach(logger::info);

        //Implements of Runnable
        new Thread(()-> {
            logger.info(Thread.currentThread() + "\n Amount of TV sets: " + tvSetList.size());
        }).start();
        //Implements of Predicate
        Predicate<TVSet> tvSetWithDiagonalMoreThen90 = x -> x.getDiagonal() > DIAGONAL_OF_TV_SETS_FOR_SOME_CONSUMERS;
        logger.info("TV set with a diagonal of more than 90");
        tvSetList.stream().filter(tvSetWithDiagonalMoreThen90).forEach(logger::info);
        //Implements of BinaryOperator and BiFunction
        BinaryOperator<Integer> priceOfTVSet = (x, y) -> x + y;
        BiFunction<Integer,TVSet,Integer> tvSetFromJapan = (x,y)-> {
            if(y.getProducer().getAddress().equals("Japan"))
                return x + y.getPrice();
            else
                return x + 0;
        };
        logger.info("Price of TV sets from Japan");
        logger.info(tvSetList.stream().reduce(0, tvSetFromJapan, priceOfTVSet));
        //Implements of Consumer
        Consumer<TVSet> priceWithDiscount = x -> x.setPrice((int)(x.getPrice()* DISCOUNT));
        logger.info("TV set with discount 10%");
        tvSetList.stream().peek(priceWithDiscount).forEach(logger::info);
        //Implements of Supplier
        Supplier<TVSet> createNewTvSet = () -> new TVSet(new Producer());
        logger.info("Random new TV set: \n" + createNewTvSet.get());

        Validate<String> checkThatWordsAreSimilar = String::equals;
        Validate<Integer> checkingGradesForEnteringUniversity = (x,y) -> x+y > 100;
        Validate<LocalDate> checkThatDifferenceBetweenDateIsOneYear = (x,y) -> x.minusYears(1).equals(y);
        logger.info("Words are similar?: \n" + checkThatWordsAreSimilar.validate("VolodNine","StepanKill"));
        logger.info("Have I entered the university?: \n" + checkingGradesForEnteringUniversity.validate(40,59));
        logger.info("Has a year passed since our last meeting?: \n" +
                checkThatDifferenceBetweenDateIsOneYear.validate(LocalDate.of(2019,9,27),
                        LocalDate.of(2018,9,27)));

        Validate.staticMethod(logger);
        checkingGradesForEnteringUniversity.defaultMethod(logger);
    }
}
