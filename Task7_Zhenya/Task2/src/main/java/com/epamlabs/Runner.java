package com.epamlabs;

import com.epamlabs.beans.Author;
import com.epamlabs.beans.Book;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Stream.*;

public class Runner {
    private static final int MIN_SIZE_OF_LIST = 1;
    private static final int MAX_SIZE_OF_LIST = 5;
    private static Logger logger = Logger.getRootLogger();

    private static List<Author> createListOfAuthors() {
        List<Author> authors = null;

        try {
            authors = Files.lines
                    (Paths.get(Runner.class.getResource("/authors.txt").toURI())).
                    flatMap(s -> of(s.split(";"))).
                    map(Author::new).
                    collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        return authors;
    }

    private static List<Book> createListOfBooks() {
        List<Book> books = null;

        try {
            books = Files.lines
                    (Paths.get(Runner.class.getResource("/books.txt").toURI())).
                    flatMap(s -> of(s.split(";"))).
                    map(Book::new).
                    collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        return books;
    }

    public static void main(String[] args) {
        List<Book> books = createListOfBooks();
        List<Author> authors = createListOfAuthors();

        books.forEach(x -> {
            for (int i = 0; i < ThreadLocalRandom.current().nextInt(MIN_SIZE_OF_LIST,MAX_SIZE_OF_LIST); i++) {
                x.addAuthor(authors.get(ThreadLocalRandom.current().nextInt(0,authors.size())));
            }
        });
        authors.forEach(x -> {
            for (int i = 0; i < ThreadLocalRandom.current().nextInt(MIN_SIZE_OF_LIST, MAX_SIZE_OF_LIST); i++) {
                x.addBook(books.get(ThreadLocalRandom.current().nextInt(0,books.size())));
            }
        });
        //1
        logger.info(books.stream().anyMatch(x -> x.getNumberOfPage() > 200));
        //2
        logger.info(books.stream().allMatch(x -> x.getNumberOfPage() > 200));
        //3
        Stream<Book> booksWithMaxAmountOfPage = books.stream().
                filter(x -> x.getNumberOfPage() == books.stream().
                        max(Comparator.comparing(Book::getNumberOfPage)).
                        orElseThrow(RuntimeException::new).
                        getNumberOfPage());
        Stream<Book> booksWithMinAmountOfPage = books.stream().
                filter(x -> x.getNumberOfPage() == books.stream().
                        min(Comparator.comparing(Book::getNumberOfPage)).
                        orElseThrow(RuntimeException::new).
                        getNumberOfPage());
        Stream.concat(booksWithMaxAmountOfPage,booksWithMinAmountOfPage).forEach(logger::info);
        //4
        books.stream().filter(x -> x.getAuthors().size() == 1).forEach(logger::info);
        //5
        books.stream().
                sorted(Comparator.comparing(Book::getNumberOfPage).thenComparing(Book::getName)).
                forEach(logger::info);
        //6
        books.stream().map(Book::getName).distinct().forEach(logger::info);
        //7
        books.stream().filter(x -> x.getNumberOfPage() < 200).
                map(Book::getAuthors).
                distinct().
                forEach(logger::info);
    }


}

