package com.epamlabs.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Book {
    private String name;
    private int numberOfPage;
    private List<Author> authors = new ArrayList<>();

    public Book() {
    }

    public Book(String name, int numberOfPage) {
        this.name = name;
        this.numberOfPage = numberOfPage;
    }

    public Book(String name, int numberOfPage, List<Author> authors) {
        this.name = name;
        this.numberOfPage = numberOfPage;
        this.authors = authors;
    }

    public Book(String line) {
        String[] regex = line.split(" ");
        this.name = regex[0];
        this.numberOfPage = Integer.parseInt(regex[1]);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfPage() {
        return numberOfPage;
    }

    public void setNumberOfPage(int numberOfPage) {
        this.numberOfPage = numberOfPage;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public void addAuthor(Author author){
        authors.add(author);
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", numberOfPage=" + numberOfPage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return name.equals(book.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
