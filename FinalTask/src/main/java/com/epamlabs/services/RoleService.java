package com.epamlabs.services;

import com.epamlabs.model.Role;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RoleService {
    Role findById(Long roleId);
    List<Role> findAll();
}
