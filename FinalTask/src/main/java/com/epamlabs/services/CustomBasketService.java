package com.epamlabs.services;

import com.epamlabs.repositories.BasketRepository;
import com.epamlabs.exceptions.NoProductInBasketException;
import com.epamlabs.exceptions.NoSuchBasketProductException;
import com.epamlabs.model.Basket;
import com.epamlabs.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomBasketService implements BasketService {
    private final BasketRepository basketRepository;

    @Autowired
    public CustomBasketService(BasketRepository basketRepository) {
        this.basketRepository = basketRepository;
    }

    @Override
    public void save(Basket basket) {
        basketRepository.save(basket);
    }

    @Transactional
    @Override
    public void deleteProductByUserId(Long userId) {
        basketRepository.deleteBasketByUserId(userId);
    }

    @Override
    public List<Basket> findAllByUserId(Long userId) {
        List<Basket> baskets = basketRepository.findAllByUserId(userId);
        if (baskets.isEmpty()){
            throw new NoProductInBasketException("No products in your basket");
        }
        return baskets;
    }

    @Override
    public Basket findById(Long basketId) {
        return basketRepository.findById(basketId).
                orElseThrow(() -> new NoSuchBasketProductException("No such basket product"));
    }

    @Override
    public Long countByProductId(Long productId) {
        return basketRepository.countByProductId(productId);
    }

    @Override
    public List<Long> findAllProductsIdByUserId(Long userId) {
        return basketRepository.findAllByUserId(userId).stream().
                map(Basket::getProduct).
                map(Product::getId).
                collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long basketId) {
        basketRepository.deleteById(basketId);
    }
}
