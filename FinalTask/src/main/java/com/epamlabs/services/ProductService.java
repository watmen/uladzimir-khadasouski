package com.epamlabs.services;

import com.epamlabs.model.Category;
import com.epamlabs.model.Product;

import java.util.List;

public interface ProductService {
    List<Product> findAll();
    void save(Product product);
    Product findById(Long productId);
    void reductionNumberOfProduct(Long productId);
    List<Product> findAllByCategory(Category category);
    void delete(Product product);
}
