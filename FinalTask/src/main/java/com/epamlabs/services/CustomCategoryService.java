package com.epamlabs.services;

import com.epamlabs.repositories.CategoryRepository;
import com.epamlabs.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomCategoryService implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CustomCategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(Long categoryId) {
        return categoryRepository.findById(categoryId).get();
    }

}
