package com.epamlabs.services;

import com.epamlabs.repositories.ProductRepository;
import com.epamlabs.exceptions.NoSuchProductException;
import com.epamlabs.model.Category;
import com.epamlabs.model.Product;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

@Service
public class CustomProductService implements ProductService {
    private Logger logger = Logger.getRootLogger();
    private final ProductRepository productRepository;
    private final BasketService basketService;

    @Autowired
    public CustomProductService(ProductRepository productRepository, BasketService basketService) {
        this.productRepository = productRepository;
        this.basketService = basketService;
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public void save(Product product) {
        productRepository.save(product);
    }

    @Override
    public Product findById(Long productId) {
        return productRepository.findById(productId).
                orElseThrow(() -> new NoSuchProductException("No such product"));
    }

    @Override
    public void reductionNumberOfProduct(Long productId) {
        Product product = productRepository.findById(productId).
                orElseThrow(() -> new NoSuchProductException("No such product"));
        product.setNumberOfProduct(product.getNumberOfProduct() - basketService.countByProductId(productId));
        productRepository.save(product);
    }

    @Override
    public List<Product> findAllByCategory(Category category) {
        List<Product> products = productRepository.findAllByCategory(category);
        if (products.isEmpty()) {
            logger.error("User try to find product by category "
                    + category +
                    ", but no such product with this category");
            throw new NoSuchProductException("No products with category " + category.getCategory());
        }
        return products;
    }

    @Override
    public void delete(Product product) {
        File file = new File("./" + product.getFileName());
        file.delete();
        productRepository.delete(product);
    }
}
