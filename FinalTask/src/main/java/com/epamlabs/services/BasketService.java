package com.epamlabs.services;

import com.epamlabs.model.Basket;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BasketService {
    void save(Basket basket);
    void deleteProductByUserId(Long userId);
    List<Basket> findAllByUserId(Long userId);
    Basket findById(Long basketId);
    Long countByProductId(Long productId);
    List<Long> findAllProductsIdByUserId(Long userId);
    void deleteById(Long basketId);
}
