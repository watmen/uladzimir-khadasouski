package com.epamlabs.services;

import com.epamlabs.repositories.RoleRepository;
import com.epamlabs.model.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomRoleService implements RoleService {
    private final RoleRepository roleRepository;

    public CustomRoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role findById(Long roleId) {
        return roleRepository.findById(roleId).get();
    }

    @Override
    public List<Role> findAll() {
        return roleRepository.findAll();
    }
}
