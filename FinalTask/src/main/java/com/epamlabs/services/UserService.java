package com.epamlabs.services;

import com.epamlabs.model.User;
import org.springframework.stereotype.Component;

@Component
public interface UserService {
    User findByLogin(String login);
    void save(User user);
}
