package com.epamlabs.services;

import com.epamlabs.model.Category;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CategoryService {
    List<Category> findAll();
    Category findById(Long categoryId);
}
