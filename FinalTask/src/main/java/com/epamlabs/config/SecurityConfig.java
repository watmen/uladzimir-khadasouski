package com.epamlabs.config;

import com.epamlabs.security.CustomAuthProvider;
import com.epamlabs.security.CustomLogoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = "com.epamlabs")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private CustomAuthProvider authProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().
                    antMatchers("/admin/**").hasRole("ADMIN").
                    antMatchers("/user/**").hasRole("USER").
                    antMatchers("/login","/registration","error").anonymous().
                    antMatchers("/","/main").authenticated().
                    antMatchers("/img/**").permitAll().
                    antMatchers("/css/**").permitAll().
                and().
                    formLogin().
                        usernameParameter("login").
                        loginPage("/login").
                        loginProcessingUrl("/doLogin").
                        successForwardUrl("/main").
                and().
                    exceptionHandling().
                    accessDeniedPage("/main").
                and().
                    sessionManagement().
                    invalidSessionUrl("/login").
                and().
                    logout().
                    addLogoutHandler(new CustomLogoutHandler()).
                    logoutSuccessUrl("/login").
                and().
                    csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/css/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }

    @Bean(name = "sessionRegistry")
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
