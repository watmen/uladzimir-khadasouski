package com.epamlabs.utils;

import com.epamlabs.exceptions.SuchUserExistException;
import com.epamlabs.exceptions.UserBadPasswordException;
import com.epamlabs.model.User;
import com.epamlabs.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserValidator {
    private Logger logger = Logger.getLogger(UserValidator.class);
    private UserService userService;

    public UserValidator() {
    }

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    public void validate(User user) {
        if (userService.findByLogin(user.getLogin()) != null) {
            logger.error("User is trying to register an existing account: " + user.getLogin());
            throw new SuchUserExistException("This login in use");
        }

        if (!user.getPassword().matches("^(([a-zA-Z].*[0-9])|([0-9].*[a-zA-Z]))")) {
            logger.error("User is trying to register an account with password, which does not fit the pattern ");
            throw new UserBadPasswordException("Password should contains letters and numbers");
        }

    }
}
