package com.epamlabs.listener;

import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.http.HttpSessionEvent;

public class SessionListener extends HttpSessionEventPublisher {

    public static final int LIFETIME_OF_SESSION = 3;

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        super.sessionCreated(event);

        event.getSession().setMaxInactiveInterval(60*LIFETIME_OF_SESSION);
    }

}
