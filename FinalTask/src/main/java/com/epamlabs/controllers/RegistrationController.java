package com.epamlabs.controllers;

import com.epamlabs.exceptions.SuchUserExistException;
import com.epamlabs.exceptions.UserBadPasswordException;
import com.epamlabs.model.User;
import com.epamlabs.services.CustomRoleService;
import com.epamlabs.services.UserService;
import com.epamlabs.utils.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegistrationController {
    private static final String ATTRIBUTE_USER = "user";
    public static final long ID_OF_USERS_ROLE = 2L;
    private Logger logger = Logger.getLogger(RegistrationController.class);
    private final UserService userService;
    private final UserValidator userValidator;
    private final CustomRoleService roleService;

    @Autowired
    public RegistrationController(UserService userService, UserValidator userValidator, CustomRoleService roleService) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.roleService = roleService;
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute(ATTRIBUTE_USER, new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("user") @Valid User user) {
        user.setRole(roleService.findById(ID_OF_USERS_ROLE));
        userValidator.validate(user);
        userService.save(user);
        logger.info("User register new account: " + user.getLogin());
        return "redirect:/login";
    }

    @ExceptionHandler({UserBadPasswordException.class,
            SuchUserExistException.class})
    public ModelAndView viewError(RuntimeException exception) {
        ModelAndView model = new ModelAndView("registration");
        model.addObject(ATTRIBUTE_USER, new User());
        model.addObject("error", exception.getMessage());
        return model;
    }
}