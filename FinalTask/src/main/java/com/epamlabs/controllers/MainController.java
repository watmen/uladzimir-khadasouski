package com.epamlabs.controllers;

import com.epamlabs.model.Product;
import com.epamlabs.services.CustomCategoryService;
import com.epamlabs.services.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MainController {
    private Logger logger = Logger.getLogger(MainController.class);
    private final ProductService productService;
    private final CustomCategoryService categoryService;

    @Autowired
    public MainController(ProductService productService, CustomCategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping("/")
    public String welcome() {
        return "redirect:/main";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/main", method = {RequestMethod.GET,RequestMethod.POST})
    public String viewMain(Model model) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        logger.info("User: " + user.getUsername() + " log in");
        String role = user.getAuthorities().stream().findAny().get().getAuthority();
        model.addAttribute("categories", categoryService.findAll());
        if (role.equals("ROLE_USER")) {
            model.addAttribute("products", productService.findAll());
            return "/user/userShopPage";
        }
        List<Product> products = productService.findAll().stream().
                filter(x -> x.getNumberOfProduct() > 0).
                collect(Collectors.toList());
        model.addAttribute("products", products);
        return "/admin/adminShopPage";
    }

}
