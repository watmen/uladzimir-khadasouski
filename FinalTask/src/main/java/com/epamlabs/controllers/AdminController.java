package com.epamlabs.controllers;

import com.epamlabs.exceptions.BadNumberOfProductException;
import com.epamlabs.exceptions.NoProductInBasketException;
import com.epamlabs.model.Product;
import com.epamlabs.services.CategoryService;
import com.epamlabs.services.ProductService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;


@Controller
public class AdminController {
    private Logger logger = Logger.getLogger(AdminController.class);
    private final CategoryService categoryService;
    private final ProductService productService;

    @Autowired
    public AdminController(CategoryService categoryService, ProductService productService) {
        this.categoryService = categoryService;
        this.productService = productService;
    }

    @GetMapping("/admin/addProduct")
    public String view(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("product", new Product());
        return "/admin/newProduct";
    }

    @PostMapping("/admin/addProduct")
    public String addProduct(@ModelAttribute("product") Product product,
                             @RequestParam("file") MultipartFile file,
                             @RequestParam("categoryId") Long categoryId) throws IOException {
        String resultFilename = "";
        if (file != null && !file.getOriginalFilename().isEmpty()) {
            resultFilename = file.getOriginalFilename();
            file.transferTo(new File("./" + resultFilename));
            product.setFileName(resultFilename);
        }
        product.setCategory(categoryService.findById(categoryId));
        productService.save(product);
        logger.info("Admin add new product: " + product);
        return "redirect:/main";
    }



    @GetMapping("/admin/editProduct")
    public String editProduct(@RequestParam("productId") Long productId,
                              Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("product", productService.findById(productId));
        return "/admin/editProduct";
    }

    @GetMapping("/admin/deleteProduct")
    public String deleteProduct(@RequestParam("productId") Long productId) {
        logger.info("Admin delete product: " + productService.findById(productId));
        productService.delete(productService.findById(productId));
        return "redirect:/main";
    }

    @ExceptionHandler({
            BadNumberOfProductException.class,
            NoProductInBasketException.class})
    public ModelAndView viewError(RuntimeException exception) {
        ModelAndView model = new ModelAndView("/errors/error");
        model.addObject("error", exception.getMessage());
        return model;
    }


}
