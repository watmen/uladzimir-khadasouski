package com.epamlabs.controllers;

import com.epamlabs.exceptions.BadNumberOfProductException;
import com.epamlabs.exceptions.NoProductInBasketException;
import com.epamlabs.exceptions.NoSuchProductException;
import com.epamlabs.model.Basket;
import com.epamlabs.services.BasketService;
import com.epamlabs.services.CategoryService;
import com.epamlabs.services.ProductService;
import com.epamlabs.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
    private Logger logger = Logger.getLogger(UserController.class);
    private final CategoryService categoryService;
    private final ProductService productService;
    private final BasketService basketService;
    private final UserService userService;

    public UserController(CategoryService categoryService, ProductService productService, BasketService basketService, UserService userService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.basketService = basketService;
        this.userService = userService;
    }

    @PostMapping("/user/sortedByCategory")
    public String viewMainSortedByCategory(@RequestParam(name = "categoryId") Long categoryId,
                                           Model model) {
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("products", productService.findAllByCategory(categoryService.findById(categoryId)));
        return "/user/userShopPage";
    }

    @GetMapping("/user/basket")
    public String viewBasket(Model model) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("baskets", basketService.findAllByUserId(
                userService.findByLogin(user.getUsername()).getId()));
        return "/user/basket";
    }

    @PostMapping("/user/buyProduct")
    public String dropAllProductsFromBasket() {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        basketService.findAllProductsIdByUserId(userService.findByLogin(user.getUsername()).getId()).stream().
                distinct().
                forEach(productService::reductionNumberOfProduct);
        logger.info("User: " + user.getUsername() + "buy products: ");
        basketService.deleteProductByUserId(userService.findByLogin(user.getUsername()).getId());
        return "redirect:/main";
    }

    @GetMapping("/user/removeFromBasket")
    public String dropProductFromBasket(@RequestParam("basketId") Long basketId) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        logger.info("User: " + user.getUsername() + "remove product " +
                basketService.findById(basketId).getProduct() + " from basket");
        basketService.deleteById(basketId);
        return "redirect:/user/basket";
    }

    @GetMapping("/user/addToBasket")
    public String addProductToBasket(@RequestParam("productId") Long productId) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Basket basket = new Basket(productService.findById(productId), userService.findByLogin(user.getUsername()));
        if (productService.findById(productId).getNumberOfProduct() <= basketService.countByProductId(productId)){
            throw new BadNumberOfProductException("No more products.");
        }
        logger.info("User: " + user.getUsername() + "add product" + productService.findById(productId) + "to basket");
        basketService.save(basket);
        return "redirect:/main";
    }

    @ExceptionHandler({
            BadNumberOfProductException.class,
            NoProductInBasketException.class,
            NoSuchProductException.class})
    public ModelAndView viewError(RuntimeException exception) {
        ModelAndView model = new ModelAndView("/errors/error");
        model.addObject("error", exception.getMessage());
        return model;
    }

}
