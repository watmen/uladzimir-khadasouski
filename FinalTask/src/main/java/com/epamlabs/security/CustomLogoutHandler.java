package com.epamlabs.security;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomLogoutHandler implements LogoutHandler {
    private Logger logger = Logger.getLogger(CustomLogoutHandler.class);

    @Override
    public void logout(HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse,
                       Authentication authentication) {
        if (authentication != null ) {
            UserDetails user = (UserDetails) authentication.getPrincipal();
            logger.info("User " + user.getUsername() + " log out");
        }
    }
}
