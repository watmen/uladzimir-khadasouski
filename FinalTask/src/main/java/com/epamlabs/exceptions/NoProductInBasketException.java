package com.epamlabs.exceptions;

public class NoProductInBasketException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoProductInBasketException() {
    }

    public NoProductInBasketException(String message) {
        super(message);
    }

    public NoProductInBasketException(Throwable cause) {
        super(cause);
    }

    public NoProductInBasketException(String message, Throwable cause) {
        super(message, cause);
    }
}