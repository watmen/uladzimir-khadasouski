package com.epamlabs.exceptions;

public class UserBadPasswordException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UserBadPasswordException() {
    }

    public UserBadPasswordException(String message) {
        super(message);
    }

    public UserBadPasswordException(Throwable cause) {
        super(cause);
    }

    public UserBadPasswordException(String message, Throwable cause) {
        super(message, cause);
    }
}
