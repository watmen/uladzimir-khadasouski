package com.epamlabs.exceptions;

public class SuchUserExistException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SuchUserExistException() {
    }

    public SuchUserExistException(String message) {
        super(message);
    }

    public SuchUserExistException(Throwable cause) {
        super(cause);
    }

    public SuchUserExistException(String message, Throwable cause) {
        super(message, cause);
    }
}