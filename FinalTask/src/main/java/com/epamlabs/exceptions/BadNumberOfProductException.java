package com.epamlabs.exceptions;

public class BadNumberOfProductException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public BadNumberOfProductException() {
    }

    public BadNumberOfProductException(String message) {
        super(message);
    }

    public BadNumberOfProductException(Throwable cause) {
        super(cause);
    }

    public BadNumberOfProductException(String message, Throwable cause) {
        super(message, cause);
    }
}