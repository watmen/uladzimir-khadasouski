package com.epamlabs.exceptions;

public class NoSuchProductException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoSuchProductException() {
    }

    public NoSuchProductException(String message) {
        super(message);
    }

    public NoSuchProductException(Throwable cause) {
        super(cause);
    }

    public NoSuchProductException(String message, Throwable cause) {
        super(message, cause);
    }
}