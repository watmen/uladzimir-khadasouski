package com.epamlabs.exceptions;

public class NoSuchBasketProductException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NoSuchBasketProductException() {
    }

    public NoSuchBasketProductException(String message) {
        super(message);
    }

    public NoSuchBasketProductException(Throwable cause) {
        super(cause);
    }

    public NoSuchBasketProductException(String message, Throwable cause) {
        super(message, cause);
    }
}