package com.epamlabs.repositories;

import com.epamlabs.model.Basket;
import com.epamlabs.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasketRepository extends JpaRepository<Basket, Long> {
    void deleteBasketByUserId(Long userId);
    List<Basket> findAllByUserId(Long userId);
    Long countByProductId(Long productId);
    Basket findByUser(User user);
}
