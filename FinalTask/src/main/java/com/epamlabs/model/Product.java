package com.epamlabs.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private Long price;
    @Column(name = "numberOfProduct")
    private Long numberOfProduct;
    @ManyToOne
    @JoinColumn(name = "categoryId")
    private Category category;
    @Size(max = 20000000)
    @Column(name = "description")
    private String description;
    @Column(name = "fileName")
    private String fileName;


    public Product() {
    }
    public Product(String name) {
        this.name = name;
    }

    public Product(String name, Long price, Long numberOfProduct, Category category, String description, String fileName) {
        this.name = name;
        this.price = price;
        this.numberOfProduct = numberOfProduct;
        this.category = category;
        this.description = description;
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getNumberOfProduct() {
        return numberOfProduct;
    }

    public void setNumberOfProduct(Long numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", numberOfProduct=" + numberOfProduct +
                ", category=" + category +
                ", description='" + description + '\'' +
                '}';
    }
}
