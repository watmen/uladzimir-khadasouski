package com.tests.testControllers;

import com.epamlabs.config.DBConfig;
import com.epamlabs.config.SecurityConfig;
import com.epamlabs.config.WebConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration( classes = {DBConfig.class, SecurityConfig.class, WebConfig.class})
@WebAppConfiguration
public class MainControllerTest {
    @Autowired
    private WebApplicationContext wac;

    private MockMvc mvc;

    static RequestPostProcessor testAdminModel() {return user("VolodNine").roles("ADMIN");}
    static RequestPostProcessor testUserModel() {return user("Ilya").roles("USER");}

    @BeforeEach
    public void setup(){
        mvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
    }

    @Test
    public void homeShouldRedirectToMainPage() throws Exception {
        mvc.perform(get("/")
                .with(testUserModel())).
                andDo(print()).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/main"));
    }

    @Test
    public void testViewUserPageMethodPost() throws Exception{
        mvc.perform(post("/main").
                with(testUserModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/user/userShopPage"));

    }

    @Test
    public void testViewUserPageMethodGet() throws Exception{
        mvc.perform(get("/main").
                with(testAdminModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/admin/adminShopPage"));

    }

    @Test
    public void testViewAdminPageMethodPost() throws Exception{
        mvc.perform(post("/main").
                with(testAdminModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/admin/adminShopPage"));

    }

    @Test
    public void testViewAdminPageMethodGet() throws Exception{
        mvc.perform(get("/main").
                with(testAdminModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/admin/adminShopPage"));

    }

    @Test
    public void testViewLoginPage() throws Exception{
        mvc.perform(get("/login")).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("login"));

    }

}
