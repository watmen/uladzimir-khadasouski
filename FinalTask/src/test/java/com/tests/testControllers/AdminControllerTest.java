package com.tests.testControllers;

import com.epamlabs.config.DBConfig;
import com.epamlabs.config.SecurityConfig;
import com.epamlabs.config.WebConfig;
import com.epamlabs.repositories.ProductRepository;
import com.epamlabs.model.Product;
import com.epamlabs.services.CustomProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DBConfig.class, SecurityConfig.class, WebConfig.class})
@WebAppConfiguration
public class AdminControllerTest {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CustomProductService customProductService;

    private MockMvc mvc;

    static RequestPostProcessor testAdminModel() {
        return user("Ilya").roles("ADMIN");
    }

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
    }

    @Test
    public void testViewAddProductPage() throws Exception {
        mvc.perform(get("/admin/addProduct").
                with(testAdminModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/admin/newProduct"));
    }

    @Test
    public void testAddProduct() throws Exception{
        MockMultipartFile file = new MockMultipartFile("file","text.png",
                                            "test/plain","test xml".getBytes());
        mvc.perform(MockMvcRequestBuilders.multipart("/admin/addProduct").
                file(file).
                with(testAdminModel()).
                param("name","testProduct").
                param("numberOfProduct","3").
                param("price","26").
                param("description","Good skirt").
                param("categoryId", "1")).
                andDo(print()).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/main"));
        assertNotNull(productRepository.findByName("testProduct"));
        customProductService.delete(productRepository.findByName("testProduct"));
    }

    @Test
    public void testEditProduct() throws Exception {
        mvc.perform(get("/admin/editProduct").
                with(testAdminModel()).
                param("productId", "1")).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/admin/editProduct"));
        assertNotNull(customProductService.findById(1L));
    }

    @Test
    public void testDeleteProduct() throws Exception {
        productRepository.save(new Product("testProduct"));
        mvc.perform(get("/admin/deleteProduct").
                with(testAdminModel()).
                param("productId", String.valueOf(productRepository.findByName("testProduct").getId()))).
                andDo(print()).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/main"));
        assertNull(productRepository.findByName("testProduct"));
    }
}
