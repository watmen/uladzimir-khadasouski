package com.tests.testControllers;

import com.epamlabs.config.DBConfig;
import com.epamlabs.config.SecurityConfig;
import com.epamlabs.config.WebConfig;
import com.epamlabs.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DBConfig.class, SecurityConfig.class, WebConfig.class})
@WebAppConfiguration
public class RegistrationControllerTest {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private UserRepository userRepository;

    private MockMvc mvc;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void testViewRegistrationPage() throws Exception {
        mvc.perform(get("/registration")).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("registration"));
    }

    @Test
    public void testRegisterUser() throws Exception {
        mvc.perform(post("/registration").
                param("login","Test").
                param("password","epam23")).
                andDo(print()).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/login"));
        assertNotNull(userRepository.findByLogin("Test"));
        userRepository.delete(userRepository.findByLogin("Test"));
    }
}
