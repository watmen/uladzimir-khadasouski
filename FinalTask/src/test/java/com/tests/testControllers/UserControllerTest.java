package com.tests.testControllers;

import com.epamlabs.config.DBConfig;
import com.epamlabs.config.SecurityConfig;
import com.epamlabs.config.WebConfig;
import com.epamlabs.repositories.BasketRepository;
import com.epamlabs.repositories.ProductRepository;
import com.epamlabs.repositories.UserRepository;
import com.epamlabs.model.Basket;
import com.epamlabs.model.Product;
import com.epamlabs.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DBConfig.class, SecurityConfig.class, WebConfig.class})
@WebAppConfiguration
public class UserControllerTest {
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private BasketRepository basketRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ProductRepository productRepository;

    private MockMvc mvc;

    static RequestPostProcessor testUserModel() {
        return user("Ilya").roles("USER");
    }

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
    }

    @Test
    public void testViewBasket() throws Exception {
        User user = userRepository.findByLogin("Ilya");
        Product product = productRepository.findById(1L).orElse(new Product());
        Basket basket = new Basket(product, user);
        basketRepository.save(basket);
        mvc.perform(get("/user/basket").
                with(testUserModel())).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/user/basket"));
    }

    @Test
    public void testViewShopPageSortedByCategory() throws Exception {
        mvc.perform(post("/user/sortedByCategory").
                with(testUserModel()).
                param("categoryId", "1")).
                andDo(print()).
                andExpect(status().is2xxSuccessful()).
                andExpect(view().name("/user/userShopPage"));
    }

    @Test
    public void testDropAllProductsFromBasketThroughUserId() throws Exception {
        User user = userRepository.findByLogin("Ilya");
        Product product = productRepository.findById(1L).orElse(new Product());
        Basket basket = new Basket(product, user);
        basketRepository.save(basket);
        mvc.perform(post("/user/buyProduct").
                with(testUserModel())).
                andDo(print()).
                andExpect(status().is3xxRedirection()).
                andExpect(redirectedUrl("/main"));
        assertTrue(basketRepository.findAllByUserId(user.getId()).isEmpty());
    }

    @Test
    public void testDeleteProductBasketThroughBasketId() throws Exception {
        User user = new User("Test", "test123");
        userRepository.save(user);
        Product product = productRepository.findById(1L).orElse(new Product());
        Basket basket = new Basket(product, user);
        basketRepository.save(basket);
        mvc.perform(get("/user/removeFromBasket").
                with(testUserModel()).
                param("basketId", String.valueOf(basketRepository.findByUser(user).getId()))).
                andDo(print()).
                andExpect(status().
                is3xxRedirection()).
                andExpect(redirectedUrl("/user/basket"));
        assertNull(basketRepository.findByUser(user));
        userRepository.delete(user);
    }

    @Test
    public void testAddProductToBasket() throws Exception {
        mvc.perform(get("/user/addToBasket").
                with(testUserModel()).
                param("productId", "1")).
                andDo(print()).
                andExpect(status().
                        is3xxRedirection()).
                andExpect(redirectedUrl("/main"));
        assertNotNull(basketRepository.findById(1L));
    }
}
