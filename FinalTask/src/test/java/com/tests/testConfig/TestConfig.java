package com.tests.testConfig;

import com.epamlabs.repositories.*;
import com.epamlabs.services.*;
import com.epamlabs.utils.UserValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {
    @Bean
    public UserValidator userValidator(){
        return new UserValidator(userService());
    }

    @Bean
    public UserService userService(){
        return mock(UserService.class);
    }

    @Bean
    public CustomUserService customUserService(){
        return new CustomUserService(userRepository(),passwordEncoder());
    }

    @Bean
    public UserRepository userRepository(){
        return mock(UserRepository.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return mock(PasswordEncoder.class);
    }

    @Bean
    public CustomRoleService customRoleService(){
        return new CustomRoleService(roleRepository());
    }

    @Bean
    public RoleRepository roleRepository(){
        return mock(RoleRepository.class);
    }

    @Bean
    public CustomCategoryService customCategoryService(){
        return new CustomCategoryService(categoryRepository());
    }

    @Bean
    public CategoryRepository categoryRepository(){
        return mock(CategoryRepository.class);
    }

    @Bean
    public BasketService basketService(){
        return mock(BasketService.class);
    }

    @Bean
    public ProductRepository productRepository(){
        return mock(ProductRepository.class);
    }

    @Bean
    CustomProductService customProductService(){
        return new CustomProductService(productRepository(),basketService());
    }

    @Bean
    public BasketRepository basketRepository(){
        return mock(BasketRepository.class);
    }

    @Bean
    public CustomBasketService customBasketService(){
        return new CustomBasketService(basketRepository());
    }
}
