package com.tests.testUtils;

import com.epamlabs.exceptions.SuchUserExistException;
import com.epamlabs.exceptions.UserBadPasswordException;
import com.epamlabs.model.User;
import com.epamlabs.services.UserService;
import com.epamlabs.utils.UserValidator;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class UserValidatorTest {
    @Autowired
    private UserValidator userValidator;

    //mock
    @Autowired
    private UserService userService;

    private static final String login = "VolodNine";
    private static final String goodPassword = "abc123";
    private static final String badPassword = "abc";
    private static final User user = mock(User.class);

    @BeforeAll
    public static void setup(){
        when(user.getLogin()).thenReturn(login);
        when(user.getPassword()).thenReturn(goodPassword);
    }

    @Test
    public void validateShouldAcceptUserWithNewLogin(){
        when(userService.findByLogin(login)).thenReturn(null);
        assertDoesNotThrow(() -> userValidator.validate(user));
    }

    @Test
    public void validateShouldAcceptUserWithCorrectPassword(){
        when(user.getPassword()).thenReturn(goodPassword);
        assertDoesNotThrow(()-> userValidator.validate(user));
    }

    @Test
    public void validateShouldThrowExceptionUserWithIncorrectPassword(){
        when(user.getPassword()).thenReturn(badPassword);
        assertThrows(UserBadPasswordException.class, () -> userValidator.validate(user));
    }

    @Test
    public void validateShouldThrowExceptionUserWithExistLogin(){
        when(userService.findByLogin(login)).thenReturn(user);
        assertThrows(SuchUserExistException.class, () -> userValidator.validate(user));
    }
}