package com.tests.testServices;

import com.epamlabs.repositories.CategoryRepository;
import com.epamlabs.model.Category;
import com.epamlabs.services.CustomCategoryService;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class CustomCategoryServiceTest {

    @Autowired
    private CustomCategoryService customCategoryService;

    //mock
    @Autowired
    private CategoryRepository categoryRepository;

    private static final Category category = mock(Category.class);
    @Spy
    private static final List<Category> categories = new ArrayList<>();

    @Test
    void testFindById() {
        when(categoryRepository.findById(any())).thenReturn(java.util.Optional.ofNullable(category));
        assertEquals(category,customCategoryService.findById(0L));
    }

    @Test
    void testFindAll() {
        when(categoryRepository.findAll()).thenReturn(categories);
        assertEquals(categories,customCategoryService.findAll());
    }
}