package com.tests.testServices;

import com.epamlabs.repositories.BasketRepository;
import com.epamlabs.exceptions.NoSuchBasketProductException;
import com.epamlabs.model.Basket;
import com.epamlabs.services.CustomBasketService;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class CustomBasketServiceTest {
    @Autowired
    private CustomBasketService customBasketService;

    //mock
    @Autowired
    private BasketRepository basketRepository;

    private static final Basket basket = mock(Basket.class);
    @Spy
    private static final List<Basket> baskets = new ArrayList<>();

    @Test
    void testSave() {
        customBasketService.save(basket);
        verify(basketRepository, atLeastOnce()).save(basket);
    }

    @Test
    void testDeleteProductByUserId() {
        customBasketService.deleteProductByUserId(any());
        verify(basketRepository, atLeastOnce()).deleteBasketByUserId(any());
    }

    @Test
    void testFindAllByUserId() {
        List<Basket> baskets = Collections.singletonList(new Basket());
        when(basketRepository.findAllByUserId(anyLong())).thenReturn(baskets);
        assertEquals(baskets,customBasketService.findAllByUserId(anyLong()));

    }

    @Test
    void testFindById() {
        when(basketRepository.findById(anyLong())).thenReturn(ofNullable(basket));
        assertEquals(basket, customBasketService.findById(anyLong()));
    }

    @Test
    void testFindByWrongId() {
        when(basketRepository.findById(anyLong())).thenThrow(NoSuchBasketProductException.class);
        assertThrows(NoSuchBasketProductException.class, () -> customBasketService.findById(4L));
    }

    @Test
    void testCountByProductId() {
        customBasketService.countByProductId(any());
        verify(basketRepository, atLeastOnce()).countByProductId(any());
    }

    @Test
    void testFindAllProductsIdByUserId() {
        List<Long> productsId = new ArrayList<>();
        when(basketRepository.findAllByUserId(anyLong())).thenReturn(baskets);
        assertEquals(productsId, customBasketService.findAllProductsIdByUserId(4L));
    }

    @Test
    void testDeleteById() {
        customBasketService.deleteById(any());
        verify(basketRepository,atLeastOnce()).deleteById(any());
    }
}