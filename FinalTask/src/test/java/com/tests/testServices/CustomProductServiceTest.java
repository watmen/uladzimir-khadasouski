package com.tests.testServices;

import com.epamlabs.repositories.ProductRepository;
import com.epamlabs.exceptions.NoSuchProductException;
import com.epamlabs.model.Category;
import com.epamlabs.model.Product;
import com.epamlabs.services.BasketService;
import com.epamlabs.services.CustomProductService;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class CustomProductServiceTest {
    @Autowired
    private CustomProductService customProductService;

    //mock
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BasketService basketService;

    private static final Product product = mock(Product.class);
    private static final Category category = mock(Category.class);
    @Spy
    private List<Product> products = new ArrayList<>();

    @Test
    void findAll() {
        when(productRepository.findAll()).thenReturn(products);
        assertEquals(products, customProductService.findAll());
    }

    @Test
    void testSave() {
        customProductService.save(product);
        verify(productRepository, atLeastOnce()).save(product);
    }


    @Test
    void findByWrongId() {
        when(productRepository.findById(anyLong())).thenThrow(NoSuchProductException.class);
        assertThrows(NoSuchProductException.class, () -> customProductService.findById(anyLong()));
    }

    @Test
    void findAllByCategory() {
        List<Product> products = Collections.singletonList(new Product());
        when(category.getCategory()).thenReturn("");
        when(productRepository.findAllByCategory(category)).thenReturn(products);
        assertEquals(products,customProductService.findAllByCategory(category));
    }

    @Test
    void findAllByCategoryWithThrowException() {
        when(category.getCategory()).thenReturn("");
        when(productRepository.findAllByCategory(category)).thenReturn(products);
        assertThrows(NoSuchProductException.class, () -> customProductService.findAllByCategory(category));
    }

    @Test
    void delete() {
        customProductService.delete(product);
        verify(productRepository,atLeastOnce()).delete(product);
    }
}