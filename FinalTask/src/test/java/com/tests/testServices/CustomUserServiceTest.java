package com.tests.testServices;

import com.epamlabs.repositories.UserRepository;
import com.epamlabs.model.User;
import com.epamlabs.services.CustomUserService;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class CustomUserServiceTest {
    @Autowired
    private CustomUserService customUserService;

    //mock
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private static final User user = mock(User.class);

    @Test
    public void testingSaveUser(){
        when(user.getPassword()).thenReturn("abc");
        customUserService.save(user);
        verify(userRepository, atLeastOnce()).save(user);
        verify(passwordEncoder, atLeastOnce()).encode(user.getPassword());
    }

    @Test
    public void testFindByLogin(){
        when(userRepository.findByLogin(any())).thenReturn(user);
        assertEquals(user,customUserService.findByLogin(""));
    }

}