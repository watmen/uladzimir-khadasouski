package com.tests.testServices;

import com.epamlabs.repositories.RoleRepository;
import com.epamlabs.model.Role;
import com.epamlabs.services.CustomRoleService;
import com.tests.testConfig.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class)
class CustomRoleServiceTest {
    @Autowired
    private CustomRoleService customRoleService;

    //mock
    @Autowired
    private RoleRepository roleRepository;

    private static final Role role = mock(Role.class);
    @Spy
    private static final List<Role> roles = new ArrayList<>();

    @Test
    void testFindById() {
        when(roleRepository.findById(any())).thenReturn(ofNullable(role));
        assertEquals(role,customRoleService.findById(anyLong()));
    }

    @Test
    void testFindAll() {
        when(roleRepository.findAll()).thenReturn(roles);
        assertEquals(roles,customRoleService.findAll());
    }
}