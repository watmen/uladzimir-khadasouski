package com.epamlab.warriors;

import com.epamlab.equipment.Weapon;
import com.epamlab.skills.Skill;

/**
 * Class Paladin describes model of paladin in simple game
 * - extends – AbstractWarrior
 * @author Uladzimir Khodosovckiy
 */
public class Paladin extends AbstractWarrior {

    /**
     * This field consist amount of health, which will be added after level up
     */
    private static final int UP_HEALTH_AFTER_LEVEL_UP = 25;
    /**
     * This field consist amount of damage, which will be added after level up
     */
    private static final int UP_DAMAGE_AFTER_LEVEL_UP = 5;
    /**
     * This field consist amount of mana, which will be added after level up
     */
    private static final int UP_MANA_AFTER_LEVEL_UP = 5;

    /**
     * Constructor
     * @param health - health of warrior
     * @param experience - experience of warrior
     * @param level - level of warrior
     * @param weapon - weapon of warrior
     * @param damage - damage of warrior
     * @param mana - mana of warrior
     * @param skill - skill of warrior
     */
    public Paladin(int health, int experience, int level, Weapon weapon, int damage, int mana, Skill skill) {
        super(health, experience, level, weapon, damage, mana, skill);
    }

    /**
     * This method describes logic to magician for level up
     */
    @Override
    public void upLevel(){
        super.upLevel();
        setHealth(getHealth()+ UP_HEALTH_AFTER_LEVEL_UP);
        setDamage(getDamage()+ UP_DAMAGE_AFTER_LEVEL_UP);
        setMana(getMana()+ UP_MANA_AFTER_LEVEL_UP);
    }
}

