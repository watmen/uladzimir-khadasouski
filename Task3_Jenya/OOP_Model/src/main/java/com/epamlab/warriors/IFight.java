package com.epamlab.warriors;

import com.epamlab.monsters.AbstractMonster;

public interface IFight {
    void killMonster(AbstractMonster monster);
}
