package com.epamlab.equipment;

/**
 * Class Weapon describes model of weapon in simple game
 * @author Uladzimir Khodosovckiy
 */
public class Weapon{
    /**
     * This field contains information about type of weapon
     */
    private String type;
    /**
     * This field contains information about damage of weapon
     */
    private int damage;

    /**
     * Constructor without params
     */
    public Weapon(){}
    /**
     * Constructor
     * @param type - health of weapon
     * @param damage - damage of weapon
     */
    public Weapon(String type, int damage) {
        this.type = type;
        this.damage = damage;
    }

    /**
     * getter
     * @return type of weapon
     */
    public String getType() {
        return type;
    }

    /**
     * setter
     * @param type - type of weapon
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * getter
     * @return damage of weapon
     */
    public int getDamage() {
        return damage;
    }

    /**
     * setter
     * @param damage - damage of weapon
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }
}
