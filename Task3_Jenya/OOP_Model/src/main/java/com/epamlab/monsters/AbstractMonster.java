package com.epamlab.monsters;

import com.epamlab.constants.Constants;

/**
 * Class AbstractMonster describes model of monster in simple game
 * @author Uladzimir Khodosovckiy
 */
public abstract class AbstractMonster {

    /**
     * This field contains message to user, which shows health of monster
     */
    private static final String MESSAGE_HEALTH_OF_MONSTER = "Health of the monster: ";
    /**
     * This field contains message to user, which shows damage of monster
     */
    private static final String MESSAGE_DAMAGE_OF_MONSTER = "Damage of the monster: ";

    /**
     * This field contains information about level of the monster
     */
    private int health;
    /**
     * This field contains information about damage of the monster
     */
    private int damage;
    /**
     * This field contains information about experience after kill the monster
     */
    private int experienceAfterKill;

    /**
     * Constructor without params
     */
    public AbstractMonster(){}
    /**
     * Constructor
     * @param health - health of monster
     * @param damage - damage of monster
     * @param experienceAfterKill - experience after kill the monster
     */
    public AbstractMonster(int health, int damage,int experienceAfterKill) {
        this.health = health;
        this.damage = damage;
        this.experienceAfterKill = experienceAfterKill;
    }

    /**
     * getter
     * @return health of monster
     */
    public int getHealth() {
        return health;
    }

    /**
     * setter
     * @param health - health of monster
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * getter
     * @return damage of monster
     */
    public int getDamage() {
        return damage;
    }

    /**
     * setter
     * @param damage - damage of monster
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * getter
     * @return experience after kill the monster
     */
    public int getExperienceAfterKill() {
        return experienceAfterKill;
    }

    /**
     * setter
     * @param experienceAfterKill - experience after kill the monster
     */
    public void setExperienceAfterKill(int experienceAfterKill) {
        this.experienceAfterKill = experienceAfterKill;
    }

    /**
     * This method check health of monster
     * @return monster died or alive
     */
    public boolean monsterAlive(){
        return getHealth() > Constants.POSITIVE_LIMIT;
    }

    /**
     * This method print health and damage of monster
     */
    public void printHealthAndDamageOfMonster(){
        Constants.log.info(MESSAGE_HEALTH_OF_MONSTER + health + Constants.SEMICOLON +
                MESSAGE_DAMAGE_OF_MONSTER + damage);
    }

    /**
     * This method shows state of monster
     * @return object state
     */
    @Override
    public String toString() {
        return new StringBuilder(MESSAGE_HEALTH_OF_MONSTER).
                append(health + Constants.SEMICOLON).
                append(MESSAGE_DAMAGE_OF_MONSTER).
                append(damage)
                .toString();
    }
}
