package com.epamlab.skills;

import com.epamlab.constants.Constants;

/**
 * Class Skill describes model of skill in simple game
 * @author Uladzimir Khodosovckiy
 */
public class Skill {
    /**
     * This field contains message to user, which shows name of skill
     */
    private static final String MESSAGE_ABOUT_NAME_OF_SKILL = "Name of skill: ";
    /**
     * This field contains message to user, which shows mana cost of skill
     */
    private static final String MESSAGE_ABOUT_MANA_COST_OF_SKILL = "Mana cost of skill: ";
    /**
     * This field contains message to user, which shows damage of skill
     */
    private static final String MESSAGE_ABOUT_DAMAGE_OF_SKILL = "Damage of skill: ";

    /**
     * This field contains information about name of skill
     */
    private String name;
    /**
     * This field contains information about manaCost of skill
     */
    private int manaCost;
    /**
     * This field contains information about damage of skill
     */
    private int damage;

    /**
     * Constructor without params
     */
    public Skill(){}
    /**
     * Constructor
     * @param name - name of skill
     * @param damage - damage of skill
     * @param manaCost - manaCost of skill
     */
    public Skill(String name, int manaCost, int damage) {
        this.name = name;
        this.manaCost = manaCost;
        this.damage = damage;
    }

    /**
     * getter
     * @return mana of warrior
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     * @return mana of warrior
     */
    public int getManacost() {
        return manaCost;
    }

    public void setManacost(int manaCost) {
        this.manaCost = manaCost;
    }

    /**
     * getter
     * @return manaCost of skill
     */
    public int getManaCost() {
        return manaCost;
    }

    /**
     * setter
     * @param manaCost - manaCost of skill
     */
    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    /**
     * getter
     * @return damage of skill
     */
    public int getDamage() {
        return damage;
    }

    /**
     * setter
     * @param damage - damage of skill
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * This method shows information about skill
     * @return object information
     */
    @Override
    public String toString() {
        return new StringBuilder(MESSAGE_ABOUT_NAME_OF_SKILL).
                append(name).
                append(Constants.SEMICOLON).
                append(MESSAGE_ABOUT_MANA_COST_OF_SKILL).
                append(manaCost).
                append(Constants.SEMICOLON).
                append(MESSAGE_ABOUT_DAMAGE_OF_SKILL).
                append(damage).
                toString();
    }
}
