package com.epamlab;

import com.epamlab.constants.Constants;
import com.epamlab.factories.MonsterTypeFactory;
import com.epamlab.factories.WarriorTypeFactory;
import com.epamlab.monsters.AbstractMonster;
import com.epamlab.utils.CheckUserAnswer;
import com.epamlab.warriors.AbstractWarrior;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class default_package.Runner shows functional of program
 * @author Uladzimir Khodosovckiy
 */
public class Runner {

    /**
     * This field contains number, which is the last id of warrior
     */
    private static final int RIGHT_LIMIT_OF_WARRIOR_ID = 3;
    /**
     * This field contains number, which is the last id of monster
     */
    private static final int RIGHT_LIMIT_OF_MONSTER_ID = 3;

    /**
     * This field contains message to user that he/she inputs wrong id of warrior
     */
    private static final String MESSAGE_WRONG_ID_OF_WARRIOR = "You input wrong id of warrior.\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user, which offers to choose class of warrior
     */
    private static final String MESSAGE_CHOOSE_WARRIOR = "Choose class of your warrior:\n" +
            "1 - paladin\n" +
            "2 - magician\n" +
            "3 - rogue";
    /**
     * This field contains message to user, which report that user enter the area with mobs
     */
    private static final String MESSAGE_ENTER_THE_AREA = "You entered the area.";
    /**
     * This field contains message to user, which offers to choose type of monster
     */
    private static final String MESSAGE_CHOOSE_TYPE_OF_MONSTER = "Choose type of monster:\n" +
            "1 - weak monster\n" +
            "2 - middle monster\n" +
            "3 - strong monster";
    /**
     * This field contains message to user that he input wrong id of monster
     */
    private static final String MESSAGE_WRONG_ID_OF_MONSTER = "You input wrong id of monster.\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user, which offers continue or finish game
     */
    private static final String MESSAGE_CONTINUE_WORK = "Do you want to continue your adventure?\n" +
            "1 - yes\n" +
            "2 - no";
    /**
     * This field contains message to user that he/she inputs wrong data
     */
    private static final String MESSAGE_INCORRECT_DATA = "You input wrong data!!!";
    /**
     * This field contains message to user, wherein the developer wishes good luck to user
     */
    private static final String MESSAGE_GOOD_LUCK = "Good luck, hero!";


    /**
     * This method check id of warrior, which user inputs
     * @return id of warrior right or not
     */
    private static boolean wrongIdOfWarrior(int idOfWarrior){
        return idOfWarrior < Constants.POSITIVE_LIMIT || idOfWarrior > RIGHT_LIMIT_OF_WARRIOR_ID;
    }

    /**
     * This method check id of monster, which user inputs
     * @return id of monster right or not
     */
    private static boolean wrongIdOfMonster(int idOfMonster){
        return idOfMonster < Constants.POSITIVE_LIMIT || idOfMonster > RIGHT_LIMIT_OF_MONSTER_ID;
    }

    /**
     * This method shows functional
     * @param args - command line array
     */
    public static void main(String[] args){
        try{
            Scanner input = new Scanner(System.in);
            Constants.log.info(MESSAGE_CHOOSE_WARRIOR);
            int idOfWarrior = input.nextInt();
            while(wrongIdOfWarrior(idOfWarrior)){
                Constants.log.error(MESSAGE_WRONG_ID_OF_WARRIOR);
                idOfWarrior = input.nextInt();
            }
            AbstractWarrior warrior = WarriorTypeFactory.getWarrior(idOfWarrior);
            int answer;
            do{
                Constants.log.info(MESSAGE_ENTER_THE_AREA);
                Constants.log.info(MESSAGE_CHOOSE_TYPE_OF_MONSTER);
                int idOfMonster = input.nextInt();
                while(wrongIdOfMonster(idOfMonster)){
                    Constants.log.error(MESSAGE_WRONG_ID_OF_MONSTER);
                    idOfMonster = input.nextInt();
                }
                AbstractMonster monster = MonsterTypeFactory.getMonster(idOfMonster);
                warrior.killMonster(monster);
                if(warrior.canWarriorUpLevel()){
                    warrior.upLevel();
                }
                Constants.log.info(warrior);
                Constants.log.info(MESSAGE_CONTINUE_WORK);
                answer = input.nextInt();
                answer = CheckUserAnswer.inputRightAnswer(answer);
            }while(answer == 1);
            Constants.log.info(MESSAGE_GOOD_LUCK);
        }catch (InputMismatchException e){
            Constants.log.fatal(MESSAGE_INCORRECT_DATA);
        }
    }
}
