package com.epamlab.factories;


import com.epamlab.equipment.Weapon;
import com.epamlab.skills.Skill;
import com.epamlab.warriors.AbstractWarrior;
import com.epamlab.warriors.Magician;
import com.epamlab.warriors.Paladin;
import com.epamlab.warriors.Rogue;

/**
 * Class MonsterTypeFactory describes logic of pattern Factory to create some warrior
 * @author Uladzimir Khodosovckiy
 */
public class WarriorTypeFactory {
    /**
     * This field contains start health of rogue
     */
    private static final int START_HEALTH_OF_ROGUE = 100;
    /**
     * This field contains start experience of rogue
     */
    private static final int START_EXPERIENCE_ROGUE = 0;
    /**
     * This field contains start damage of rogue
     */
    private static final int START_DAMAGE_OF_ROGUE= 10;
    /**
     * This field contains information weapon of rogue
     */
    private static final Weapon START_WEAPON_OF_ROGUE = new Weapon("Dagger",10);
    /**
     * This field contains information about skill of rogue
     */
    private static final Skill SKILL_OF_ROGUE= new Skill("Kick Of",30, 30);
    /**
     * This field contains start mana of rogue
     */
    private static final int START_MANA_OF_ROGUE = 90;

    /**
     * This field contains start health of paladin
     */
    private static final int START_HEALTH_OF_PALADIN = 100;
    /**
     * This field contains start experience of paladin
     */
    private static final int START_EXPERIENCE_PALADIN = 0;
    /**
     * This field contains start damage of paladin
     */
    private static final int START_DAMAGE_OF_PALADIN= 20;
    /**
     * This field contains information weapon of paladin
     */
    private static final Weapon START_WEAPON_OF_PALADIN = new Weapon("SWORD",20);
    /**
     * This field contains information about skill of paladin
     */
    private static final Skill SKILL_OF_PALADIN= new Skill("Sanctification",40, 10);
    /**
     * This field contains start mana of paladin
     */
    private static final int START_MANA_OF_PALADIN = 70;

    /**
     * This field contains start health of magician
     */
    private static final int START_HEALTH_OF_MAGICIAN = 100;
    /**
     * This field contains start experience of magician
     */
    private static final int START_EXPERIENCE_MAGICIAN = 0;
    /**
     * This field contains start damage of magician
     */
    private static final int START_DAMAGE_OF_MAGICIAN = 18;
    /**
     * This field contains information weapon of magician
     */
    private static final Weapon START_WEAPON_OF_MAGICIAN = new Weapon("Stuff",9);
    /**
     * This field contains information about skill of magician
     */
    private static final Skill SKILL_OF_MAGICIAN= new Skill("Hail of stones",30, 50);
    /**
     * This field contains start mana of magician
     */
    private static final int START_MANA_OF_MAGICIAN = 150;

    /**
     * This field contains start level for all com.warriors
     */
    private static final int START_LEVEL= 1;


    public enum TypeOfWarrior {
        PALADIN {
            public AbstractWarrior getWarrior() {
                return new Paladin(START_HEALTH_OF_PALADIN,
                        START_EXPERIENCE_PALADIN,
                        START_LEVEL,
                        START_WEAPON_OF_PALADIN,
                        START_DAMAGE_OF_PALADIN,
                        START_MANA_OF_PALADIN,
                        SKILL_OF_PALADIN);
            }
        },
        MAGICIAN{
            public AbstractWarrior getWarrior(){
                return new Magician(START_HEALTH_OF_MAGICIAN,
                        START_EXPERIENCE_MAGICIAN,
                        START_LEVEL,
                        START_WEAPON_OF_MAGICIAN,
                        START_DAMAGE_OF_MAGICIAN,
                        START_MANA_OF_MAGICIAN,
                        SKILL_OF_MAGICIAN);
            }
        },
        ROGUE{
            public AbstractWarrior getWarrior(){
                return new Rogue(START_HEALTH_OF_ROGUE,
                        START_EXPERIENCE_ROGUE,
                        START_LEVEL,
                        START_WEAPON_OF_ROGUE,
                        START_DAMAGE_OF_ROGUE,
                        START_MANA_OF_ROGUE,
                        SKILL_OF_ROGUE);
            }
        };

        /**
         * This method create object of some warrior
         * @return object of some warrior
         */
        public abstract AbstractWarrior getWarrior();
    }
    /**
     * This choose warrior, which will be create
     * @param typeOfWarrior - id of warrior, which person inputs
     * @return object of some warrior
     */
    public static AbstractWarrior getWarrior(int typeOfWarrior){
        int idOfWarrior = typeOfWarrior - 1;
        return TypeOfWarrior.values()[idOfWarrior].getWarrior();
    }
}
