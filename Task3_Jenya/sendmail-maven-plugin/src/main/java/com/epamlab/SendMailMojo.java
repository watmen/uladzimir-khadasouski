package com.epamlab;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Mojo(name = "send", defaultPhase = LifecyclePhase.PROCESS_CLASSES, threadSafe = true)
public class SendMailMojo extends AbstractMojo {

    @Parameter(property = "login")
    private String login;

    @Parameter(property = "password")
    private String password;

    @Parameter(property = "mail")
    private String mail;

    @Parameter( defaultValue = "${project}", readonly = true )
    private MavenProject project;

    private static final String TEXT_OF_MESSAGE= "Program is work";

    private String THEME_OF_MESSAGE = "EPAM_TASK";

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Start sending...");
        mailSend(login,password,mail);
        getLog().info("Done,check your mail!!!!");
    }

    private void mailSend(String login,String password,String mail){

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "465");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.socketFactory.port", "465");
        prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(login, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(login));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(mail)
            );
            message.setSubject(THEME_OF_MESSAGE);
            message.setText(TEXT_OF_MESSAGE);

            Transport.send(message);


        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}