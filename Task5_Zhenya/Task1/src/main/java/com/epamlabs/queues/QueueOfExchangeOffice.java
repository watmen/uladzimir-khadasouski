package com.epamlabs.queues;

import com.epamlabs.tourists.Tourist;
import com.epamlabs.constants.Constant;

import java.util.ArrayList;
import java.util.List;

public class QueueOfExchangeOffice {

    private List<Tourist> queueOfExchangeOffice;
    private int count;

    public QueueOfExchangeOffice() {

        queueOfExchangeOffice = new ArrayList<Tourist>();
    }

    public synchronized void addToQueueOfExchangeOffice(Tourist tourist){
        try {
            while (count>=3)
            {
                Constant.logger.error("Queue of exchange office is full");
                wait();
            }
            queueOfExchangeOffice.add(tourist);
            count++;
            notify();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public synchronized Tourist getTouristFromQueueOfExchangeOffice(){
        try {
            while (count==0){
                Constant.logger.error("No com.epamlabs.tourist in the queue of exchange office");
                wait();
            }
            Tourist tourist = queueOfExchangeOffice.get(0);
            queueOfExchangeOffice.remove(0);
            count--;
            notify();
            return tourist;

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

}
