package com.epamlabs.queues;

import com.epamlabs.tourists.Tourist;
import com.epamlabs.constants.Constant;

import java.util.ArrayList;
import java.util.List;

public class QueueOfCurrencyExchange {
    private List<Tourist> queueOfCurrencyExchange;
    private int count;

    public QueueOfCurrencyExchange() {
        queueOfCurrencyExchange = new ArrayList<Tourist>();
    }

    public synchronized void addToQueueOfCurrencyExchange(Tourist tourist){
        queueOfCurrencyExchange.add(tourist);
        count++;
        notify();
    }


    public synchronized Tourist getTouristFromQueueOfCurrencyExchange(){
        try {
            while (count==0){
                Constant.logger.error("No com.epamlabs.tourist in the queue of currency exchange");
                wait();
            }
            Tourist tourist = queueOfCurrencyExchange.get(0);
            queueOfCurrencyExchange.remove(0);
            count--;
            notify();
            return tourist;

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
