package com.epamlabs.currencyExchangers;

public interface Exchanges {
    int exchange(int amountToBeChanged);
}
