package com.epamlabs.currencyExchangers;

import com.epamlabs.course.Course;
import com.epamlabs.tourists.Tourist;
import com.epamlabs.constants.Constant;
import com.epamlabs.queues.QueueOfCurrencyExchange;

public class CurrencyExchange implements Exchanges, Runnable {

    private Course course;

    private QueueOfCurrencyExchange queueOfCurrencyExchange;

    public int exchange(int amountToBeChanged) {
        return (int) (amountToBeChanged*(course.getCourse()*0.95));
    }

    public CurrencyExchange(QueueOfCurrencyExchange queueOfCurrencyExchange, Course course) {
        this.queueOfCurrencyExchange = queueOfCurrencyExchange;
        this.course = course;
    }

    public void run() {

        while (System.currentTimeMillis() < Constant.FINISH_OF_WORK) {
            Tourist tourist = queueOfCurrencyExchange.getTouristFromQueueOfCurrencyExchange();
            Constant.logger.info(Thread.currentThread().getName() +": Current course = " + course.getCourse());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Constant.logger.info(Thread.currentThread().getName() +": Tourist have = " + tourist.getMoney() + "$");
            tourist.setMoney(exchange(tourist.getMoney()));
            Constant.logger.info(Thread.currentThread().getName() +": After exchange com.epamlabs.tourist have = " + tourist.getMoney() + "BYN");
        }
    }
}
