package com.epamlabs.currencyExchangers;

import com.epamlabs.course.Course;
import com.epamlabs.tourists.Tourist;
import com.epamlabs.constants.Constant;
import com.epamlabs.queues.QueueOfExchangeOffice;


public class ExchangeOffice implements Exchanges, Runnable {

    private Course course;

    private QueueOfExchangeOffice queueOfExchangeOffice;

    public ExchangeOffice(QueueOfExchangeOffice queueOfExchangeOffice,Course course) {
        this.queueOfExchangeOffice = queueOfExchangeOffice;
        this.course = course;
    }



    public int exchange(int amountToBeChanged) {
        return (int) (amountToBeChanged*course.getCourse());
    }

    public void run() {
        try {
            while (System.currentTimeMillis() < Constant.FINISH_OF_WORK) {
                Tourist tourist = queueOfExchangeOffice.getTouristFromQueueOfExchangeOffice();
                Constant.logger.info(Thread.currentThread().getName() +": Current course = " + course.getCourse());
                Thread.sleep(3000);
                Constant.logger.info(Thread.currentThread().getName() +": Tourist have = " + tourist.getMoney() + "$");
                tourist.setMoney(exchange(tourist.getMoney()));
                Constant.logger.info(Thread.currentThread().getName() +": After exchange com.epamlabs.tourist have = " + tourist.getMoney() + "BYN");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
