package com.epamlabs.tourists;


import com.epamlabs.constants.Constant;

import com.epamlabs.queues.QueueOfCurrencyExchange;
import com.epamlabs.queues.QueueOfExchangeOffice;


public class TouristGenerator implements Runnable {
    private static final int NUMBER_OF_TOURIST = 20;
    private QueueOfExchangeOffice queueOfExchangeOffice;
    private QueueOfCurrencyExchange queueOfCurrencyExchange;

    public TouristGenerator(QueueOfExchangeOffice queueOfExchangeOffice, QueueOfCurrencyExchange queueOfCurrencyExchange) {
        this.queueOfExchangeOffice = queueOfExchangeOffice;
        this.queueOfCurrencyExchange = queueOfCurrencyExchange;
    }

    private int newTouristArrivalTime(){
        int min = 1;
        int max = 10;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private int moneyOfTourist(){
        int min = 100;
        int max = 1000;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public void run() {
        boolean flag = true;

        for (int i = 0; i < NUMBER_OF_TOURIST; i++) {
            Tourist tourist = new Tourist(moneyOfTourist());
            if(flag){
                queueOfExchangeOffice.addToQueueOfExchangeOffice(tourist);
                flag = false;
                Constant.logger.info("Tourist № " + i + " joined the currency exchange office" );
            }else{
                queueOfCurrencyExchange.addToQueueOfCurrencyExchange(tourist);
                flag = true;
                Constant.logger.info("Tourist № " + i + " joined the currency exchange");
            }
            try {
                Thread.sleep(newTouristArrivalTime()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
