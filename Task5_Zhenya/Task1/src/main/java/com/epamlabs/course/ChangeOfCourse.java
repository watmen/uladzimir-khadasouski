package com.epamlabs.course;

import com.epamlabs.constants.Constant;

public class ChangeOfCourse implements Runnable {

    private Course course;

    public ChangeOfCourse(Course course) {
        this.course = course;
    }

    private int courseTimeChange(){
        int min = 5;
        int max = 10;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    @Override
    public void run() {

        while (System.currentTimeMillis() < Constant.FINISH_OF_WORK){
            course.setCourse();
            try {
                Thread.sleep(courseTimeChange()*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
