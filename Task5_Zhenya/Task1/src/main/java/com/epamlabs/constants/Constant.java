package com.epamlabs.constants;

import org.apache.log4j.Logger;

public class Constant {
    public static final Logger logger = Logger.getRootLogger();
    public static final long  FINISH_OF_WORK = System.currentTimeMillis() + 150000;
}
