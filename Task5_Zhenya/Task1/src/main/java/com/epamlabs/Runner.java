package com.epamlabs;

import com.epamlabs.course.ChangeOfCourse;
import com.epamlabs.course.Course;
import com.epamlabs.tourists.TouristGenerator;
import com.epamlabs.currencyExchangers.CurrencyExchange;
import com.epamlabs.currencyExchangers.ExchangeOffice;
import com.epamlabs.queues.QueueOfCurrencyExchange;
import com.epamlabs.queues.QueueOfExchangeOffice;


public class Runner {
    public static void main(String[] args){
        Course course = new Course();
        ChangeOfCourse changeOfCourse = new ChangeOfCourse(course);

        QueueOfExchangeOffice queueOfExchangeOffice = new QueueOfExchangeOffice();
        QueueOfCurrencyExchange queueOfCurrencyExchange = new QueueOfCurrencyExchange();

        TouristGenerator touristGenerator = new TouristGenerator(queueOfExchangeOffice,queueOfCurrencyExchange);

        ExchangeOffice exchangeOffice = new ExchangeOffice(queueOfExchangeOffice,course);
        CurrencyExchange currencyExchange = new CurrencyExchange(queueOfCurrencyExchange,course);

        Thread threadOfTouristGenerator = new Thread(touristGenerator);
        Thread threadOfExchangeOffice= new Thread(exchangeOffice);
        Thread threadOfCurrencyExchange = new Thread(currencyExchange);
        Thread threadOfChangeOfCourse = new Thread(changeOfCourse);

        threadOfChangeOfCourse.start();
        threadOfTouristGenerator.start();
        threadOfExchangeOffice.start();
        threadOfCurrencyExchange.start();
    }
}
