package com.epamlabs;

import com.epamlabs.tourist.TouristManager;

import java.util.concurrent.*;


public class Runner {
    public static void main(String[] args) {
        for (int i = 0; i < 2; i++) {
            Phaser phaser = new Phaser(1);
            Exchanger<Integer> exchangerNumberOfPhoto = new Exchanger<>();

            TouristManager touristManager = new TouristManager(phaser,10,exchangerNumberOfPhoto);

            ExecutorService executorService = Executors.newFixedThreadPool(1);

            executorService.execute(touristManager);
            executorService.shutdown();

            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndDeregister();
        }
    }
}
