package com.epamlabs.tourist;

import com.epamlabs.constants.Constant;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;


public class Tourist implements Runnable{
    private boolean isInTravel;
    private String name;
    private int numberOfPhoto = 10;
    private int eatingTime;
    private Phaser phaser;
    private List<String> listOfName = Arrays.asList("VolodNine", "Yahor",
            "Stepan", "Ivan",
            "Anton", "Zhenya",
            "Ilya", "Egor",
            "Edik", "Vlad");
    private Semaphore semaphorePrague;
    private Semaphore semaphoreParis;
    private CyclicBarrier cyclicBarrier;
    private Exchanger<Integer> exchangerNumberOfPhoto;

    private Semaphore semaphorePhoto;

    private static final Logger logger = Logger.getLogger(Tourist.class);


    public Tourist(Phaser phaser, Exchanger<Integer> exchangerNumberOfPhoto) {
        int randomName = randomName() - 1;

        this.exchangerNumberOfPhoto = exchangerNumberOfPhoto;
        this.phaser = phaser;
        eatingTime = randomEatingTime();
        name = listOfName.get(randomName);
        this.phaser.register();
    }

    public boolean getIsInTravel() {
        return isInTravel;
    }

    public void setIsInTravel(boolean isInTravel) {
        this.isInTravel = isInTravel;
    }

    public int getNumberOfPhoto() {
        return numberOfPhoto;
    }

    public void setNumberOfPhoto(int numberOfPhoto) {
        this.numberOfPhoto = numberOfPhoto;
    }

    public int getEatingTime() {
        return eatingTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEatingTime(int eatingTime) {
        this.eatingTime = eatingTime;
    }

    public Semaphore getSemaphorePrague() {
        return semaphorePrague;
    }

    public void setSemaphorePrague(Semaphore semaphorePrague) {
        this.semaphorePrague = semaphorePrague;
    }

    public Semaphore getSemaphoreParis() {
        return semaphoreParis;
    }

    public void setSemaphoreParis(Semaphore semaphoreParis) {
        this.semaphoreParis = semaphoreParis;
    }

    public CyclicBarrier getCyclicBarrier() {
        return cyclicBarrier;
    }

    public void setCyclicBarrier(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    public Exchanger<Integer> getExchangerNumberOfPhoto() {
        return exchangerNumberOfPhoto;
    }

    public void setExchangerNumberOfPhoto(Exchanger<Integer> exchangerNumberOfPhoto) {
        this.exchangerNumberOfPhoto = exchangerNumberOfPhoto;
    }

    public Semaphore getSemaphorePhoto() {
        return semaphorePhoto;
    }

    public void setSemaphorePhoto(Semaphore semaphorePhoto) {
        this.semaphorePhoto = semaphorePhoto;
    }

    private int randomEatingTime(){
        int min = 1;
        int max = 5;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private int randomRelaxTime(){
        int min = 1;
        int max = 5;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private int randomName(){
        int min = 1;
        int max = 10;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    @Override
    public void run() {
        try {
            Constant.logger.info("Tourist " + name + " take the bus; " + phaser.getPhase());
            setIsInTravel(true);
            setNumberOfPhoto(++numberOfPhoto);
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            semaphorePrague.acquire();
            Constant.logger.info("Tourist " + name + " start drink bear");
            Thread.sleep(randomEatingTime()*1000);
            Constant.logger.info("Tourist "  + name + "  finish drink bear");
            semaphorePrague.release();
            setNumberOfPhoto(numberOfPhoto++);
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            semaphoreParis.acquire();
            Constant.logger.info("Tourist " + name + " watch picture");
            Thread.sleep(1000);
            Constant.logger.info("Tourist " + name + " finish watch picture");
            semaphoreParis.release();
            setNumberOfPhoto(numberOfPhoto++);
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            Constant.logger.info("Tourist " + name + " order food");
            Thread.sleep(eatingTime*1000);
            cyclicBarrier.await();
            setNumberOfPhoto(numberOfPhoto++);
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            Constant.logger.info("Tourist " + name + " start relax");
            Thread.sleep(randomRelaxTime()*1000);
            Constant.logger.info("Tourist " + name + " finish relax");
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            semaphorePhoto.acquire();
            Constant.logger.info("Tourist " + name + " add photo to the cloud; " + phaser.getPhase());
            setNumberOfPhoto(exchangerNumberOfPhoto.exchange(numberOfPhoto));
            semaphorePhoto.release();
            phaser.arriveAndAwaitAdvance();

            Thread.sleep(10);
            Constant.logger.info("Tourist " + name + "got off the bus");
            phaser.arriveAndAwaitAdvance();

        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }

    }
}
