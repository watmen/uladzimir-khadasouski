package com.epamlabs.tourist;

import com.epamlabs.busStops.RomePasta;
import com.epamlabs.busStops.RomePizza;
import com.epamlabs.constants.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class TouristManager implements Runnable {
    private Phaser phaser;
    private int amountOfTourist;
    private List<Tourist> tourists;
    private Exchanger<Integer> exchangerNumberOfPhoto;

    public TouristManager(Phaser phaser, int amountOfTourist, Exchanger<Integer> exchangerNumberOfPhoto) {
        this.exchangerNumberOfPhoto = exchangerNumberOfPhoto;
        this.phaser = phaser;
        this.amountOfTourist = amountOfTourist;
        this.phaser.register();
    }

    private void createTouristAndStartTravel() throws InterruptedException {
        tourists = new ArrayList<>();

        for (int i = 0; i < amountOfTourist; i++) {
            tourists.add(new Tourist(phaser,exchangerNumberOfPhoto));
        }

        ExecutorService executorService = Executors.newFixedThreadPool(40);
        for (Tourist tourist : tourists){
            executorService.execute(tourist);
            Thread.sleep(100);
        }
        executorService.shutdown();
    }

    private void addSemaphorePragueToTourist(Semaphore semaphorePrague){
        for (Tourist tourist : tourists){
            tourist.setSemaphorePrague(semaphorePrague);
        }
    }

    private void addSemaphoreParisToTourist(Semaphore semaphoreParis){
        for (Tourist tourist : tourists){
            tourist.setSemaphoreParis(semaphoreParis);
        }
    }

    private void addSemaphorePhotoToTourist(Semaphore semaphorePhoto){
        for (Tourist tourist : tourists){
            tourist.setSemaphorePhoto(semaphorePhoto);
        }
    }

    private void divisionTouristIntoTwoGroupsAndEatingInCaffe(){
        CyclicBarrier romePizza = new CyclicBarrier(5,new RomePizza());
        CyclicBarrier romePasta = new CyclicBarrier(5,new RomePasta());
        for (int i = 0; i < amountOfTourist; i++) {
            if (i % 2 == 0){
                tourists.get(i).setCyclicBarrier(romePizza);
            }
            else {
                tourists.get(i).setCyclicBarrier(romePasta);
            }
        }
    }

    @Override
    public void run() {
        try {
            Constant.logger.info("Wait com.epamlabs.tourist");
            createTouristAndStartTravel();
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("Bus come to Prague");
            Constant.logger.info(phaser.getPhase());
            Semaphore semaphorePrague = new Semaphore(1);
            addSemaphorePragueToTourist(semaphorePrague);
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("Bus come to Paris");
            Constant.logger.info(phaser.getPhase());
            Semaphore semaphoreParis = new Semaphore(5);
            addSemaphoreParisToTourist(semaphoreParis);
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("Bus come to Rome");
            Constant.logger.info(phaser.getPhase());
            divisionTouristIntoTwoGroupsAndEatingInCaffe();
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("Bus come to Spain");
            Constant.logger.info(phaser.getPhase());
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("Add photo to the cloud");
            AtomicInteger numberOfPhoto = new AtomicInteger();
            Semaphore semaphorePhoto = new Semaphore(1);
            addSemaphorePhotoToTourist(semaphorePhoto);
            for (Tourist tourist : tourists){
                tourist.setExchangerNumberOfPhoto(exchangerNumberOfPhoto);
                numberOfPhoto.addAndGet(exchangerNumberOfPhoto.exchange(0));
            }
            Constant.logger.info("Amount of the photo = " + numberOfPhoto);
            phaser.arriveAndAwaitAdvance();

            Constant.logger.info("We finish our tour!!!!!!");
            phaser.arriveAndAwaitAdvance();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
