package com.epamlabs;

import org.apache.log4j.Logger;

import java.util.concurrent.ForkJoinPool;

public class Runner {
    private static final long END = 1_000_000_000L;
    private static final int START = 0;
    private static final Logger logger = Logger.getRootLogger();

    private static void sum(){
        long sum = 0;
        for (int i = 0; i < END; i++) {
            sum+=i;
        }
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        sum();
        long time = System.currentTimeMillis() - start;
        logger.info("Without Fork/Join = " + time);
        long start1 = System.currentTimeMillis();
        ForkJoinPool forkJoinPool = new ForkJoinPool(1);
        SumOfNumbers sumAction = new SumOfNumbers(START,END);
        forkJoinPool.invoke(sumAction);
        long timeWithOneStream = System.currentTimeMillis() - start1;
        logger.info("With one stream = " + timeWithOneStream);
        long start2 = System.currentTimeMillis();
        ForkJoinPool forkJoinPool2 = new ForkJoinPool(2);
        SumOfNumbers sumAction2 = new SumOfNumbers(START,END);
        forkJoinPool2.invoke(sumAction2);
        long timeWithTwoStreams = System.currentTimeMillis() - start2;
        logger.info("With two streams = " + timeWithTwoStreams);
        long start3 = System.currentTimeMillis();
        ForkJoinPool forkJoinPool3 = new ForkJoinPool(4);
        SumOfNumbers sumAction3 = new SumOfNumbers(START,END);
        forkJoinPool3.invoke(sumAction3);
        long timeWithThreeStreams  = System.currentTimeMillis() - start3;
        logger.info("With four streams = " + timeWithThreeStreams);
        long start4 = System.currentTimeMillis();
        SumOfNumbers sumAction4 = new SumOfNumbers(START,END);
        ForkJoinPool forkJoinPool4 = new ForkJoinPool(8);
        forkJoinPool4.invoke(sumAction4);
        long timeWithEightStreams  = System.currentTimeMillis() - start4;
        logger.info("With eight streams = " + timeWithEightStreams);
    }
}
