package com.epamlabs;

import java.util.concurrent.RecursiveTask;


public class SumOfNumbers extends RecursiveTask<Long> {

    private final long start;
    private final long end;
    private final long threshold = 2_000_000L;


    public SumOfNumbers(final long start, final long end) {
        this.start = start;
        this.end = end;
    }


    @Override
    protected Long compute() {
        if (end - start < threshold) {
            return computeDirectly();
        } else {
            final long middle = (end + start) / 2;
            final SumOfNumbers subTask1 = new SumOfNumbers(start, middle - 1);
            final SumOfNumbers subTask2 = new SumOfNumbers(middle, end);
            invokeAll(subTask1, subTask2);
            return subTask1.join() + subTask2.join();
        }
    }


    private long computeDirectly() {
        long temp = 0;
        for (long i = start; i <= end; i++) {
            temp += i;
        }
        return temp;
    }

}