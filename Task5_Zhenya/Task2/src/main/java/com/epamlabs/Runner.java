package com.epamlabs;

import com.epamlabs.course.ChangeOfCourse;
import com.epamlabs.course.Course;
import com.epamlabs.currencyExchangers.CurrencyExchange;
import com.epamlabs.currencyExchangers.ExchangeOffice;
import com.epamlabs.tourists.Tourist;
import com.epamlabs.tourists.TouristGenerator;

import java.util.concurrent.*;

public class Runner {
    public static void main(String[] args) {
        Course course = new Course();
        ChangeOfCourse changeOfCourse = new ChangeOfCourse(course);

        BlockingQueue<Tourist> queueOfCurrencyOffice = new ArrayBlockingQueue<Tourist> (3,true);
        ConcurrentLinkedQueue<Tourist> queueOfCurrencyExchanger = new ConcurrentLinkedQueue<Tourist>();

        TouristGenerator touristGenerator = new TouristGenerator(queueOfCurrencyOffice,queueOfCurrencyExchanger);

        ExchangeOffice exchangeOffice = new ExchangeOffice(queueOfCurrencyOffice, course);
        CurrencyExchange currencyExchange = new CurrencyExchange(queueOfCurrencyExchanger,course);

        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        executorService.execute(touristGenerator);
        executorService.execute(exchangeOffice);
        executorService.execute(currencyExchange);
        executorService.execute(changeOfCourse);
        executorService.shutdown();
    }
}
