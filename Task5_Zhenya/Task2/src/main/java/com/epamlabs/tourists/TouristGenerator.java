package com.epamlabs.tourists;


import com.epamlabs.constants.Constant;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;


public class TouristGenerator implements Runnable {


    private BlockingQueue<Tourist> queueOfCurrencyOffice;
    private ConcurrentLinkedQueue<Tourist> queueOfCurrencyExchanger;


    public TouristGenerator(BlockingQueue<Tourist> queueOfCurrencyOffice,ConcurrentLinkedQueue<Tourist> queueOfCurrencyExchanger) {
        this.queueOfCurrencyOffice = queueOfCurrencyOffice;
        this.queueOfCurrencyExchanger = queueOfCurrencyExchanger;
    }


    private int newTouristArrivalTime() {
        int min = 1;
        int max = 10;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    private int moneyOfTourist() {
        int min = 100;
        int max = 1000;
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }

    public void run() {
        try {
            boolean flag = false;
            for (int i = 0; i < 20; i++) {
                Tourist tourist = new Tourist(moneyOfTourist());
                if (flag){
                    queueOfCurrencyOffice.put(tourist);
                    flag = false;
                    Constant.logger.info("Tourist № " + i + " joined the currency exchange office" );
                }
                else {
                    queueOfCurrencyExchanger.add(tourist);
                    flag = true;
                    Constant.logger.info("Tourist № " + i + " joined the currency exchange");
                }
                Thread.sleep(newTouristArrivalTime()*1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

