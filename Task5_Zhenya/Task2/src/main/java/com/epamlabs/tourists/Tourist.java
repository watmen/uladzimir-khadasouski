package com.epamlabs.tourists;

public class Tourist {
    private int money;

    public Tourist(int money) {
        this.money = money;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

}
