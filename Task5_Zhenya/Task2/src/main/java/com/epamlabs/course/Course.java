package com.epamlabs.course;

import java.util.concurrent.atomic.AtomicInteger;


public class Course {

    private double course = 1.5;


    public synchronized double getCourse() {
        return course;
    }

    public void setCourse() {
        double min = 1.5;
        double max = 2.6;
        max -= min;
        this.course = Math.round(((Math.random() * ++max) + min) * 10d) / 10d;
    }
}
