package com.epamlabs.currencyExchangers;

import com.epamlabs.constants.Constant;
import com.epamlabs.course.Course;

import com.epamlabs.tourists.Tourist;

import java.util.concurrent.ConcurrentLinkedQueue;

public class CurrencyExchange implements Exchanges, Runnable {

    private Course course;
    private ConcurrentLinkedQueue<Tourist> queueOfCurrencyExchanger;


    public CurrencyExchange(ConcurrentLinkedQueue<Tourist> queueOfCurrencyExchanger,Course course) {
        this.course = course;
        this.queueOfCurrencyExchanger = queueOfCurrencyExchanger;
    }

    public int exchange(int amountToBeChanged) {
        return (int) (amountToBeChanged*(course.getCourse()*0.95));
    }



    public void run() {
        try {
            while (System.currentTimeMillis() < Constant.FINISH_OF_WORK) {
                if(queueOfCurrencyExchanger.isEmpty()){
                    Thread.sleep(2000);
                }
                else {
                    Tourist tourist = queueOfCurrencyExchanger.poll();
                    Constant.logger.info(Thread.currentThread().getName() + ": Current course = " + course.getCourse());
                    Thread.sleep(2000);
                    Constant.logger.info(Thread.currentThread().getName() + ": Tourist have = " + tourist.getMoney() + "$");
                    tourist.setMoney(exchange(tourist.getMoney()));
                    Constant.logger.info(Thread.currentThread().getName() + ": After exchange com.epamlabs.tourist have = " + tourist.getMoney() + "BYN ");
                }

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
