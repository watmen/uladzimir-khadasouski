package com.epamlabs.currencyExchangers;

import com.epamlabs.constants.Constant;
import com.epamlabs.course.Course;

import com.epamlabs.tourists.Tourist;

import java.util.concurrent.BlockingQueue;


public class ExchangeOffice implements Exchanges, Runnable {

    private Course course;
    private BlockingQueue<Tourist> queueOfCurrencyOffice;

    public ExchangeOffice(BlockingQueue<Tourist> queueOfCurrencyOffice,Course course) {
        this.course = course;
        this.queueOfCurrencyOffice = queueOfCurrencyOffice;
    }

    public int exchange(int amountToBeChanged) {
        return (int) (amountToBeChanged*course.getCourse());
    }

    public void run() {
        try {
            while (System.currentTimeMillis() < Constant.FINISH_OF_WORK) {
                Tourist tourist = queueOfCurrencyOffice.take();
                Constant.logger.info(Thread.currentThread().getName() + ": Current course = " + course.getCourse());
                Thread.sleep(3000);
                Constant.logger.info(Thread.currentThread().getName() + ": Tourist have = " + tourist.getMoney() + "$");
                tourist.setMoney(exchange(tourist.getMoney()));
                Constant.logger.info(Thread.currentThread().getName() + ": After exchange tourist have = " + tourist.getMoney() + "BYN");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
