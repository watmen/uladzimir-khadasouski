package com.epamlab;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import static org.junit.Assert.*;


public class SetOperationTest {

    @Test
    public void unionOfSetsTest() {
        HashSet<Integer> setA = new HashSet<Integer>();
        setA.add(1);
        setA.add(2);
        setA.add(7);
        setA.add(5);
        setA.add(11);

        HashSet<Integer> setB = new HashSet<Integer>();
        setB.add(5);
        setB.add(7);
        setB.add(3);
        setB.add(8);
        setB.add(1);

        HashSet<Integer> unionSet = SetOperation.unionOfSets(setA,setB);

        assertNotNull(unionSet);
        assertEquals(unionSet, new HashSet<>(Arrays.asList(1,2,3,5,7,8,11)));

    }

    @Test
    public void intersectionOfSetsTest() {
        HashSet<Integer> setA = new HashSet<Integer>();
        setA.add(3);
        setA.add(8);
        setA.add(7);
        setA.add(9);
        setA.add(13);

        HashSet<Integer> setB = new HashSet<Integer>();
        setB.add(1);
        setB.add(7);
        setB.add(11);
        setB.add(8);
        setB.add(15);

        HashSet<Integer> intersectionSet = SetOperation.intersectionOfSets(setA,setB);

        assertNotNull(intersectionSet);
        assertEquals(intersectionSet, new HashSet<>(Arrays.asList(7,8)));
    }

    @Test
    public void residualOfSetsTest() {
        HashSet<Integer> setA = new HashSet<Integer>();
        setA.add(5);
        setA.add(7);
        setA.add(3);
        setA.add(8);
        setA.add(1);


        HashSet<Integer> setB = new HashSet<Integer>();
        setB.add(1);
        setB.add(2);
        setB.add(7);
        setB.add(5);
        setB.add(11);

        HashSet<Integer>  residualSet = SetOperation.residualOfSets(setA,setB);

        assertNotNull(residualSet);
        assertEquals(residualSet, new HashSet<>(Arrays.asList(3,8)));
    }

    @Test
    public void symmetricDifferenceOfSetsTest() {
        HashSet<Integer> setA = new HashSet<Integer>();
        setA.add(3);
        setA.add(8);
        setA.add(7);
        setA.add(9);
        setA.add(13);

        HashSet<Integer> setB = new HashSet<Integer>();
        setB.add(5);
        setB.add(7);
        setB.add(3);
        setB.add(8);
        setB.add(1);

        HashSet<Integer> symmetricDifferenceSet = SetOperation.symmetricDifferenceOfSets(setA,setB);

        assertNotNull(symmetricDifferenceSet);
        assertEquals(symmetricDifferenceSet, new HashSet<>(Arrays.asList(1,5,9,13)));
    }
}