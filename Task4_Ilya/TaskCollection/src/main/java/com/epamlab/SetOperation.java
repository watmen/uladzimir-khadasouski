package com.epamlab;

import java.util.HashSet;
import java.util.Iterator;


public class SetOperation extends HashSet {


    public static <T>  HashSet unionOfSets(HashSet<T>   setA, HashSet<T>   setB){

        HashSet<T>  union = new HashSet<T> ();

        Iterator<T>  it = setB.iterator();

        T elementOfSet;

        union.addAll(setA);

        while (it.hasNext()){
            elementOfSet = it.next();
            if (!setA.contains(elementOfSet)){
                union.add(elementOfSet);
            }
        }

        return union;
    }

    public static <T>  HashSet intersectionOfSets(HashSet<T>   setA, HashSet<T>   setB){

        HashSet intersection = new HashSet<T> ();

        Iterator<T>  it = setB.iterator();

        T elementOfSet;

        while (it.hasNext()){
            elementOfSet = it.next();
            if (setA.contains(elementOfSet)){
                intersection.add(elementOfSet);
            }
        }


        return intersection;
    }

    public static <T>  HashSet residualOfSets(HashSet<T>   setA, HashSet<T>   setB){

        HashSet<T>  residual = new HashSet<T> ();

        Iterator<T>  it = setA.iterator();

        T elementOfSet;

        while (it.hasNext()){
            elementOfSet = it.next();
            if (!setB.contains(elementOfSet)){
                residual.add(elementOfSet);
            }
        }

        return residual;
    }

    public static <T>  HashSet symmetricDifferenceOfSets(HashSet<T>   setA, HashSet<T>   setB){

        HashSet<T>  residualAB = residualOfSets(setA,setB);

        HashSet<T>  residualBA = residualOfSets(setB,setA);

        HashSet<T>  symmetricDifference = unionOfSets(residualAB,residualBA);

        return symmetricDifference;
    }

}
