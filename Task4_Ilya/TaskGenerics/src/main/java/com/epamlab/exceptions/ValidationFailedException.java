package com.epamlab.exceptions;

public class ValidationFailedException extends Exception{
    private static final long serialVersionUID = 1L;

    public ValidationFailedException() {}
    public ValidationFailedException(String message) {
        super(message);
    }

    public ValidationFailedException(Throwable cause) {
        super(cause);
    }

    public ValidationFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
