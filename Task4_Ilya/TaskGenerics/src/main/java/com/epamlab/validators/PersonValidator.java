package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;
import com.epamlab.byns.Person;

public class PersonValidator implements Validator<Person> {

    private static final int AGE_LIMIT = 16;
    private static final int HEIGHT_LIMIT = 160;

    private boolean personAgeIsLow(int age){
        return age <= AGE_LIMIT;
    }

    private boolean personHeightIsLow(int height){
        return height <= HEIGHT_LIMIT;
    }

    @Override
    public void validate(Person data) throws ValidationFailedException {
        if(personAgeIsLow(data.getAge())){
            throw new ValidationFailedException("Person's age is low");
        }
        if (personHeightIsLow(data.getHeight())){
            throw new ValidationFailedException("Person's height is low");
        }
    }
}
