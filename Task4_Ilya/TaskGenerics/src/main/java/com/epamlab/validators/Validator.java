package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;

public interface Validator<T> {

    void validate(T data) throws ValidationFailedException;
}
