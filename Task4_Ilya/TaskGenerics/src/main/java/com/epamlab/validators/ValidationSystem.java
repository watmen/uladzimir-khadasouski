package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;
import com.epamlab.byns.Person;


public class ValidationSystem {

    private static IntegerValidator integerValidator = new IntegerValidator();
    private static StringValidator stringValidator = new StringValidator();
    private static PersonValidator personValidator = new PersonValidator();


    public static <T> void validate(T data) throws ValidationFailedException {
        if (data instanceof Integer){
            integerValidator.validate((Integer) data);
        }
        else if (data instanceof String){
            stringValidator.validate((String) data);
        }
        else if (data instanceof Person){
            personValidator.validate((Person) data);
        }
    }
}
