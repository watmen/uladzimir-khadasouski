package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;

public class IntegerValidator implements Validator<Integer> {

    private static final double LEFT_LIMIT = 1.1;
    private static final double RIGHT_LIMIT = 15.30;

    private boolean numberOutOfRange(Integer data){
        return data < LEFT_LIMIT || data > RIGHT_LIMIT;
    }

    @Override
    public void validate(Integer data) throws ValidationFailedException {
        if (numberOutOfRange(data)){
            throw new ValidationFailedException("Number out of range");
        }
    }
}
