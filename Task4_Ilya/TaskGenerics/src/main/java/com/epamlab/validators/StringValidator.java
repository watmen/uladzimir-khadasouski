package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;


public class StringValidator implements Validator<String> {

    @Override
    public void validate(String data) throws ValidationFailedException {

        String regex = "^[A-Z].{0,15}$";

        if(!data.matches(regex)){
            throw new ValidationFailedException("Invalid string");
        }
    }
}
