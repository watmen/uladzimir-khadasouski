package com.epamlab.validators;

import com.epamlab.exceptions.ValidationFailedException;
import com.epamlab.byns.Person;
import org.junit.Test;


public class ValidationSystemTest {

    @Test
    public void testValidateInt () throws ValidationFailedException {
        ValidationSystem.validate(3);
        ValidationSystem.validate(10);
        ValidationSystem.validate(7);
        ValidationSystem.validate(14);
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException {
        ValidationSystem.validate(0);
    }

    @Test
    public void testValidateString() throws ValidationFailedException {
        ValidationSystem.validate("Hello world!");
        ValidationSystem.validate("Java Developer");
        ValidationSystem.validate("I love Docker");
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFailsLength() throws ValidationFailedException {
        ValidationSystem.validate("I'm the Iron Man, man");
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFailsBeginningOfLine() throws ValidationFailedException {
        ValidationSystem.validate("wrong line");
    }

    @Test
    public void testValidatePerson() throws ValidationFailedException {
        ValidationSystem.validate(new Person(22,176));
        ValidationSystem.validate(new Person(45,163));
        ValidationSystem.validate(new Person(110,205));
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidatePersonFailsLowAge() throws ValidationFailedException {
        ValidationSystem.validate(new Person(12,180));
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidatePersonFailsLowHeight() throws ValidationFailedException {
        ValidationSystem.validate(new Person(22,150));
    }
}