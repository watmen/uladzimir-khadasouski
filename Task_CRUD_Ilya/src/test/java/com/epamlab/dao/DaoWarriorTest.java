package com.epamlab.dao;

import com.epamlab.equipment.Weapon;
import com.epamlab.skills.Skill;
import com.epamlab.warriors.AbstractWarrior;
import com.epamlab.warriors.Paladin;
import com.epamlab.warriors.Rogue;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DaoWarriorTest {

    @Test
    public void getWarriorThroughId() {
        DaoWarrior daoWarrior = new DaoWarrior();
        int idOfWarrior = 0;
        AbstractWarrior addedWarrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));

        daoWarrior.add(addedWarrior);

        assertNotNull(daoWarrior.get(idOfWarrior));
        assertEquals(addedWarrior,daoWarrior.get(idOfWarrior));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getWarriorThroughWrongId(){
        DaoWarrior daoWarrior = new DaoWarrior();
        int idOfWarrior = 0;
        int wrongId = 30;
        AbstractWarrior addedWarrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));

        daoWarrior.get(wrongId);
    }

    @Test
    public void getAllWarriors() {
        AbstractWarrior warriorRogue = new Rogue(0,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));
        AbstractWarrior warriorPaladin = new Paladin(1,
                "VolodNine",
                10000,
                0,
                0,
                new Weapon(0,"Sword",70),
                2000,
                1000,
                new Skill(0,"Blackhole",5,30));
        List<AbstractWarrior> expectedWarriors = Arrays.asList(warriorRogue,warriorPaladin);
        DaoWarrior daoWarrior = new DaoWarrior();

        daoWarrior.add(warriorRogue);
        daoWarrior.add(warriorPaladin);

        assertNotNull(daoWarrior.getAll());
        assertEquals(expectedWarriors,daoWarrior.getAll());
    }

    @Test
    public void addNewWarriorAndCheckIt() {
        int idOfWarrior = 0;
        AbstractWarrior warriorPaladin = new Paladin(idOfWarrior,
                "VolodNine",
                10000,
                0,
                0,
                new Weapon(0,"Sword",70),
                2000,
                1000,
                new Skill(0,"Blackhole",5,30));
        DaoWarrior daoWarrior = new DaoWarrior();

        daoWarrior.add(warriorPaladin);

        assertNotNull(daoWarrior.get(idOfWarrior));
        assertEquals(warriorPaladin,daoWarrior.get(idOfWarrior));
    }

    @Test
    public void updateNameOfWarriorAndCheckIt() {
        int idOfWarrior = 0;
        AbstractWarrior addedWarrior = new Paladin(idOfWarrior,
                "VolodNine",
                10000,
                0,
                0,
                new Weapon(0,"Sword",70),
                2000,
                1000,
                new Skill(0,"Blackhole",5,30));
        AbstractWarrior updatedWarrior = new Paladin(idOfWarrior,
                "Ilya",
                10000,
                0,
                0,
                new Weapon(0,"Sword",70),
                2000,
                1000,
                new Skill(0,"Blackhole",5,30));
        DaoWarrior daoWarrior = new DaoWarrior();

        daoWarrior.add(addedWarrior);

        assertNotNull(daoWarrior.get(idOfWarrior));
        assertEquals(addedWarrior,daoWarrior.get(idOfWarrior));

        daoWarrior.update(updatedWarrior);

        assertNotNull(daoWarrior.get(idOfWarrior));
        assertEquals(updatedWarrior,daoWarrior.get(idOfWarrior));
    }

    @Test
    public void deleteWarriorAndCheckIt() {
        DaoWarrior daoWarrior  = new DaoWarrior();
        int idOfWarrior = 0;
        int sizeOfListAfterDeleted = 0;
        AbstractWarrior addedWarrior = new Paladin(idOfWarrior,
                "VolodNine",
                10000,
                0,
                0,
                new Weapon(0,"Sword",70),
                2000,
                1000,
                new Skill(0,"Blackhole",5,30));

        daoWarrior.add(addedWarrior);

        assertNotNull(daoWarrior.get(idOfWarrior));
        assertEquals(addedWarrior,daoWarrior.get(idOfWarrior));

        daoWarrior.delete(idOfWarrior);

        assertEquals(sizeOfListAfterDeleted,daoWarrior.getAll().size());
    }
}