package com.epamlab.dao;

import com.epamlab.skills.Skill;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

public class DaoSkillTest {
    @Test
    public void getStandardSkillsThroughId() {
        DaoSkill daoSkill = new DaoSkill();
        int idOfFirstSkill = 0;
        int idOfSecondSkill = 1;
        int idOfThirdSkill = 2;
        Skill firstSkill = new Skill(0, "Kick Of", 30, 30);
        Skill secondSkill = new Skill(1, "Sanctification", 40, 10);
        Skill thirdSkill = new Skill(2, "Hail of stones", 30, 50);

        assertNotNull(daoSkill.get(idOfFirstSkill));
        assertEquals(firstSkill,daoSkill.get(idOfFirstSkill));

        assertNotNull(daoSkill.get(idOfSecondSkill));
        assertEquals(secondSkill,daoSkill.get(idOfSecondSkill));

        assertNotNull(daoSkill.get(idOfThirdSkill));
        assertEquals(thirdSkill,daoSkill.get(idOfThirdSkill));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getStandardSkillsThroughWrongId(){
        DaoSkill daoSkill = new DaoSkill();
        int wrongId = 6;

        daoSkill.get(wrongId);
    }

    @Test
    public void getAllStandardSkills() {
        DaoSkill daoSkill = new DaoSkill();
        List<Skill> skills = Arrays.asList(new Skill(0, "Kick Of", 30, 30),
                new Skill(1, "Sanctification", 40, 10),
                new Skill(2, "Hail of stones", 30, 50));

        assertNotNull(daoSkill.getAll());
        assertEquals(skills,daoSkill.getAll());
    }

    @Test
    public void addNewSkillAndCheckIt() {
        DaoSkill daoSkill = new DaoSkill();
        int idOfNewSkill = 3;
        Skill newSkill = new Skill(idOfNewSkill,"Blackhole",100,20);

        daoSkill.add(newSkill);

        assertNotNull(daoSkill.get(idOfNewSkill));
        assertEquals(newSkill,daoSkill.get(idOfNewSkill));
    }

    @Test
    public void updateNewSkillAndCheckIt() {
        DaoSkill daoSkill = new DaoSkill();
        int idOfSkill = 3;
        Skill newSkill = new Skill(idOfSkill,"Blackhole",100,20);
        Skill updatedSkill = new Skill(idOfSkill,"Hex",50,10);

        daoSkill.add(newSkill);

        assertNotNull(daoSkill.get(idOfSkill));
        assertEquals(newSkill,daoSkill.get(idOfSkill));

        daoSkill.update(updatedSkill);

        assertNotNull(daoSkill.get(idOfSkill));
        assertEquals(updatedSkill,daoSkill.get(idOfSkill));
    }

    @Test
    public void deleteFirstSkillAndCheckIt() {
        DaoSkill daoSkill = new DaoSkill();
        int idOfSkill = 0;
        int sizeOfListAfterDeleted = 2;

        daoSkill.delete(idOfSkill);

        assertEquals(sizeOfListAfterDeleted,daoSkill.getAll().size());
    }
}