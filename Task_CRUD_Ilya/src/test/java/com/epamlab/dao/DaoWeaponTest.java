package com.epamlab.dao;

import com.epamlab.equipment.Weapon;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DaoWeaponTest {
    @Test
    public void getStandardWeaponsThroughId() {
        DaoWeapon daoWeapon = new DaoWeapon();
        int idOfFirstWeapon = 0;
        int idOfSecondWeapon = 1;
        int idOfThirdWeapon = 2;
        Weapon firstWeapon = new Weapon(0, "Stuff", 9);
        Weapon secondWeapon = new Weapon(1, "SWORD", 20);
        Weapon thirdSWeapon = new Weapon(2, "Dagger", 10);

        assertNotNull(daoWeapon.get(idOfFirstWeapon));
        assertEquals(firstWeapon,daoWeapon.get(idOfFirstWeapon));

        assertNotNull(daoWeapon.get(idOfSecondWeapon));
        assertEquals(secondWeapon,daoWeapon.get(idOfSecondWeapon));

        assertNotNull(daoWeapon.get(idOfThirdWeapon));
        assertEquals(thirdSWeapon,daoWeapon.get(idOfThirdWeapon));
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void getStandardWeaponsThroughWrongId(){
        DaoWeapon daoWeapon = new DaoWeapon();
        int wrongId = 15;

        daoWeapon.get(wrongId);
    }

    @Test
    public void getAllStandardWeapons() {
        DaoWeapon daoWeapon = new DaoWeapon();
        List<Weapon> weapons = Arrays.asList(new Weapon(0, "Stuff", 9),
                new Weapon(1, "SWORD", 20),
                new Weapon(2, "Dagger", 10));

        assertNotNull(daoWeapon.getAll());
        assertEquals(weapons,daoWeapon.getAll());
    }

    @Test
    public void addNewWeaponAndCheckIt() {
        DaoWeapon daoWeapon = new DaoWeapon();
        int idOfNewWeapon = 3;
        Weapon newWeapon = new Weapon(idOfNewWeapon,"Gun",100);

        daoWeapon.add(newWeapon);

        assertNotNull(daoWeapon.get(idOfNewWeapon));
        assertEquals(newWeapon,daoWeapon.get(idOfNewWeapon));
    }

    @Test
    public void updateFirstWeaponAndCheckIt() {
        DaoWeapon daoWeapon = new DaoWeapon();
        int idOfWeapon = 3;
        Weapon newWeapon = new Weapon(idOfWeapon,"Gun",100);
        Weapon updatedWeapon = new Weapon(idOfWeapon,"Maul",70);

        daoWeapon.add(newWeapon);

        assertNotNull(daoWeapon.get(idOfWeapon));
        assertEquals(newWeapon,daoWeapon.get(idOfWeapon));

        daoWeapon.update(updatedWeapon);

        assertNotNull(daoWeapon.get(idOfWeapon));
        assertEquals(updatedWeapon,daoWeapon.get(idOfWeapon));
    }

    @Test
    public void deleteThirdWeaponAndCheckIt() {
        DaoWeapon daoWeapon= new DaoWeapon();
        int idOfWeapon = 2;
        int sizeOfListAfterDeleted = 2;

        daoWeapon.delete(idOfWeapon);

        assertEquals(sizeOfListAfterDeleted,daoWeapon.getAll().size());
    }
}