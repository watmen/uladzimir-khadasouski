package com.epamlab.warriors;

import com.epamlab.constants.Constants;
import com.epamlab.equipment.Weapon;
import com.epamlab.monsters.AbstractMonster;
import com.epamlab.monsters.WeakMonster;
import com.epamlab.skills.Skill;
import org.junit.Test;

import static org.junit.Assert.*;

public class AbstractWarriorTest {

    @Test
    public void warriorKillMonsterWithoutUseSkill() {
        int idOfWarrior = 0;
        AbstractWarrior warrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));
        AbstractMonster monster = new WeakMonster(100,50,100);
        int useSkillOrNot = 2;

        Constants.log.info("Test for check fight with monster without use of skill");
        while(monster.monsterAlive()){
            warrior.killMonster(monster,useSkillOrNot);
        }
        if (monster.getHealth() <= Constants.POSITIVE_LIMIT) {
            warrior.setExperience(warrior.getExperience() + monster.getExperienceAfterKill());
            Constants.log.info("You killed the monster");
        }
    }

    @Test
    public void warriorKillMonsterWithUseSkill() {
        int idOfWarrior = 0;
        AbstractWarrior warrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));
        AbstractMonster monster = new WeakMonster(100,50,100);
        int useSkillOrNot = 1;

        Constants.log.info("Test for check fight with monster with use skill");
        while(monster.monsterAlive()){
            warrior.killMonster(monster,useSkillOrNot);
        }
        if (monster.getHealth() <= Constants.POSITIVE_LIMIT) {
            Constants.log.info("You killed the monster");
        }
    }

    @Test
    public void warriorDeadWhenFightWithMonster() {
        int idOfWarrior = 0;
        AbstractWarrior warrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));
        AbstractMonster monster = new WeakMonster(100,1000,100);
        int useSkillOrNot = 1;

        Constants.log.info("Test for check death of warrior");
        while(monster.monsterAlive()){
            warrior.killMonster(monster,useSkillOrNot);
            if (warrior.warriorDead()) {
                Constants.log.info("You died!");
                break;
            }
        }
    }

    @Test
    public void upLevelOfWarriorAndCheckIt() {
        int idOfWarrior = 0;
        AbstractWarrior warrior = new Rogue(idOfWarrior,
                "VolodNine",
                1000,
                0,
                0,
                new Weapon(0,"Gun",40),
                100,
                50,
                new Skill(0,"Hex",10,50));
        AbstractMonster monster = new WeakMonster(40,10,100);
        int useSkillOrNot = 1;
        int levelAfterUp = 1;

        Constants.log.info("Test for check method upLevel");
        while(monster.monsterAlive()){
            warrior.killMonster(monster,useSkillOrNot);
        }
        warrior.addExperience(monster.getExperienceAfterKill());
        if (warrior.canWarriorUpLevel()) {
            warrior.upLevel();
        }

        assertEquals(levelAfterUp,warrior.getLevel());
    }
}