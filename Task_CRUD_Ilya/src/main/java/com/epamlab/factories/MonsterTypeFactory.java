package com.epamlab.factories;

import com.epamlab.monsters.AbstractMonster;
import com.epamlab.monsters.MiddleMonster;
import com.epamlab.monsters.StrongMonster;
import com.epamlab.monsters.WeakMonster;

/**
 * Class MonsterTypeFactory describes logic of pattern Factory to create some monster
 *
 * @author Uladzimir Khodosovckiy
 */
public class MonsterTypeFactory {
    /**
     * This field contains standard health of week monster
     */
    private static final int HEALTH_OF_WEEK_MONSTER = 20;
    /**
     * This field contains standard damage of week monster
     */
    private static final int DAMAGE_OF_WEEK_MONSTER = 5;
    /**
     * This field contains amount of experience after kill weak monster
     */
    private static final int EXPERIENCE_AFTER_KILL_WEAK_MONSTER = 20;
    /**
     * This field contains standard health of middle monster
     */
    private static final int HEALTH_OF_MIDDLE_MONSTER = 80;
    /**
     * This field contains standard damage of middle monster
     */
    private static final int DAMAGE_OF_MIDDLE_MONSTER = 15;
    /**
     * This field contains amount of experience after kill middle monster
     */
    private static final int EXPERIENCE_AFTER_KILL_MIDDLE_MONSTER = 50;
    /**
     * This field contains standard health of strong monster
     */
    private static final int HEALTH_OF_STRONG_MONSTER = 160;
    /**
     * This field contains standard damage of strong monster
     */
    private static final int DAMAGE_OF_STRONG_MONSTER = 30;
    /**
     * This field contains amount of experience after kill strong monster
     */
    private static final int EXPERIENCE_AFTER_KILL_STRONG_MONSTER = 100;

    public enum TypeOfMonster {
        WEAK_MONSTER {
            public AbstractMonster getMonster() {
                return new WeakMonster(HEALTH_OF_WEEK_MONSTER,
                        DAMAGE_OF_WEEK_MONSTER,
                        EXPERIENCE_AFTER_KILL_WEAK_MONSTER);
            }
        },
        MIDDLE_MONSTER {
            public AbstractMonster getMonster() {
                return new MiddleMonster(HEALTH_OF_MIDDLE_MONSTER,
                        DAMAGE_OF_MIDDLE_MONSTER,
                        EXPERIENCE_AFTER_KILL_MIDDLE_MONSTER);
            }
        },
        STRONG_MONSTER {
            public AbstractMonster getMonster() {
                return new StrongMonster(HEALTH_OF_STRONG_MONSTER,
                        DAMAGE_OF_STRONG_MONSTER,
                        EXPERIENCE_AFTER_KILL_STRONG_MONSTER);
            }
        };

        /**
         * This method create object of some monster
         *
         * @return object of some monster
         */
        public abstract AbstractMonster getMonster();
    }

    /**
     * This choose monster, which will be create
     *
     * @param typeOfMonster - id of monster, which person inputs
     * @return object of some monster
     */
    public static AbstractMonster getMonster(int typeOfMonster) {
        int idOfMonster = typeOfMonster - 1;

        return TypeOfMonster.values()[idOfMonster].getMonster();
    }
}
