package com.epamlab.warriors;


import com.epamlab.constants.Constants;
import com.epamlab.equipment.Weapon;
import com.epamlab.monsters.AbstractMonster;
import com.epamlab.skills.Skill;
import com.epamlab.utils.CheckUserAnswer;

import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

/**
 * Class AbstractWarrior describes model of warrior in simple game
 * - implements Boost, Fight
 *
 * @author Uladzimir Khodosovckiy
 */
public abstract class AbstractWarrior implements Boost, Fight {

    /**
     * This field amount of experience to up level of the warrior
     */
    private static final int AMOUNT_OF_EXPERIENCE_TO_LEVEL_UP = 100;
    /**
     * This field amount of experience after level up
     */
    private static final int ELEMENTARY_EXPERIENCE = 0;
    /**
     * This field contains message to user, which reports that warrior doesn't have enough mana
     */
    private static final String MESSAGE_NOT_ENOUGH_MANA = "You don't have enough mana!";
    /**
     * This field contains message to user, which reports that the warrior died
     */
    /**
     * This field contains message to user, which shows health of the warrior
     */
    private static final String MESSAGE_YOUR_HEALTH = "Your health: ";
    /**
     * This field contains message to user, which shows experience of the warrior
     */
    private static final String MESSAGE_YOUR_EXPERIENCE = "Your experience: ";
    /**
     * This field contains message to user, which shows level of the warrior
     */
    private static final String MESSAGE_YOUR_LEVEL = "Your level: ";
    /**
     * This field contains message to user, which shows weapon of the warrior
     */
    private static final String MESSAGE_YOUR_WEAPON = "Your weapon: ";
    /**
     * This field contains message to user, which shows damage of the warrior
     */
    private static final String MESSAGE_YOUR_DAMAGE = "Your damage: ";
    /**
     * This field contains message to user, which shows mana of the warrior
     */
    private static final String MESSAGE_YOUR_MANA = "Your mana: ";
    /**
     * This field contains message to user, which shows name of the warrior
     */
    private static final String MESSAGE_NAME_OF_WARRIOR = "Name of warrior: ";
    /**
     * This field contains message to user, which shows id of the warrior
     */
    private static final String MESSAGE_ID_OF_WARRIOR = "Id of warrior: ";
    /**
     * This field contains id of the warrior
     */
    private int id;
    /**
     * This field contains name of the warrior
     */
    private String name;
    /**
     * This field contains information about health of the warrior
     */
    private int health;
    /**
     * This field contains information about experience of the warrior
     */
    private int experience;
    /**
     * This field contains information about level of the warrior
     */
    private int level;
    /**
     * This field contains information about weapon of the warrior
     */
    private Weapon weapon;
    /**
     * This field contains information about damage of the warrior
     */
    private int damage;
    /**
     * This field contains information about mana of the warrior
     */
    private int mana;
    /**
     * This field contains information about skill of warrior
     */
    private Skill skill;

    /**
     * Constructor without params
     */
    public AbstractWarrior() {
    }

    /**
     * Constructor
     *
     * @param health     - health of warrior
     * @param experience - experience of warrior
     * @param level      - level of warrior
     * @param weapon     - weapon of warrior
     * @param damage     - damage of warrior
     * @param mana       - mana of warrior
     * @param skill      - skill of warrior
     */
    public AbstractWarrior(int id, String name, int health,
                           int experience, int level,
                           Weapon weapon, int damage,
                           int mana, Skill skill) {
        this.id = id;
        this.name = name;
        this.health = health;
        this.experience = experience;
        this.level = level;
        this.weapon = weapon;
        this.damage = damage;
        this.mana = mana;
        this.skill = skill;
    }

    /**
     * getter
     *
     * @return id of warrior
     */
    public int getId() {
        return id;
    }

    /**
     * setter
     *
     * @param id - id of warrior
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getter
     *
     * @return name of warrior
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     *
     * @param name - name of warrior
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     *
     * @return mana of warrior
     */
    public int getMana() {
        return mana;
    }

    /**
     * setter
     *
     * @param mana - mana of warrior
     */
    public void setMana(int mana) {
        this.mana = mana;
    }

    /**
     * getter
     *
     * @return health of warrior
     */
    public int getHealth() {
        return health;
    }

    /**
     * setter
     *
     * @param health - health of warrior
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * getter
     *
     * @return damage of warrior + damage of his weapon
     */
    public int getDamage() {
        return damage + weapon.getDamage();
    }

    /**
     * setter
     *
     * @param damage - damage of warrior
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * getter
     *
     * @return experience of warrior
     */
    public int getExperience() {
        return experience;
    }

    /**
     * setter
     *
     * @param experience - experience of warrior
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * getter
     *
     * @return level of warrior
     */
    public int getLevel() {
        return level;
    }

    /**
     * setter
     *
     * @param level - level of warrior
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * getter
     *
     * @return weapon of warrior
     */
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * setter
     *
     * @param weapon - weapon of warrior
     */
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    /**
     * getter
     *
     * @return skill of warrior
     */
    public Skill getSkill() {
        return skill;
    }

    /**
     * setter
     *
     * @param skill - skill of warrior
     */
    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    /**
     * This method check health of warrior
     *
     * @return warrior died or alive
     */
    public boolean warriorDead() {
        return getHealth() <= Constants.POSITIVE_LIMIT;
    }

    /**
     * This method recover health and mana of warrior to standard number
     *
     * @param startHealth - full health of warrior
     * @param startMana   - full mana of warrior
     */
    public void healthAndManaRecovery(int startHealth, int startMana) {
        setHealth(startHealth);
        setMana(startMana);
    }

    /**
     * This method check experience of warrior for level up
     *
     * @return warrior can up level or not
     */
    public boolean canWarriorUpLevel() {
        return getExperience() >= AMOUNT_OF_EXPERIENCE_TO_LEVEL_UP;
    }

    /**
     * This method check mana of warrior to use his skill
     *
     * @return warrior can use skill or not
     */
    private boolean checkManaForSkill() {
        return getMana() > skill.getManaCost();
    }

    /**
     * This method print health, mana and damage of warrior
     */
    private void printHealthManaAndDamageOfWarrior() {
        Constants.log.info(MESSAGE_YOUR_HEALTH + health + Constants.SEMICOLON +
                MESSAGE_YOUR_MANA + mana + Constants.SEMICOLON +
                MESSAGE_YOUR_DAMAGE + damage);
    }

    /**
     * This method describes basic logic for level up
     */
    public void upLevel() {
        int currentLevel = getLevel();
        int nextLevel = ++currentLevel;

        setLevel(nextLevel);
        setExperience(ELEMENTARY_EXPERIENCE);
    }

    public void addExperience(int experience){
        setExperience(this.experience + experience);
    }

    /**
     * This method describes basic logic for fight with monster
     *
     * @param monster - monster, which user choose for fight
     * @throws InputMismatchException in case user input letters
     */
    public void killMonster(AbstractMonster monster, int useSkillOrNot) throws InputMismatchException {
            printHealthManaAndDamageOfWarrior();
            monster.printHealthAndDamageOfMonster();
            if (useSkillOrNot == 1) {
                if (checkManaForSkill()) {
                    setDamage(getDamage() + skill.getDamage());
                    setMana(getMana() - skill.getManaCost());
                } else {
                    Constants.log.info(MESSAGE_NOT_ENOUGH_MANA);
                }
            }
            monster.setHealth(monster.getHealth() - getDamage());
            setHealth(getHealth() - monster.getDamage());
    }

    /**
     * This method shows state of warrior
     *
     * @return object state
     */
    @Override
    public String toString() {
        return new StringBuilder(MESSAGE_ID_OF_WARRIOR).
                append(id + Constants.SEMICOLON).
                append(MESSAGE_NAME_OF_WARRIOR).
                append(name + Constants.SEMICOLON).
                append(MESSAGE_YOUR_HEALTH).
                append(health + Constants.SEMICOLON).
                append(MESSAGE_YOUR_EXPERIENCE).
                append(experience + Constants.SEMICOLON).
                append(MESSAGE_YOUR_LEVEL).
                append(level + Constants.SEMICOLON).
                append(MESSAGE_YOUR_WEAPON).
                append(weapon.getType() + Constants.SEMICOLON).
                append(MESSAGE_YOUR_DAMAGE).
                append(getDamage() + Constants.SEMICOLON).
                append(MESSAGE_YOUR_MANA).
                append(mana).
                toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractWarrior warrior = (AbstractWarrior) o;
        return Objects.equals(name, warrior.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
