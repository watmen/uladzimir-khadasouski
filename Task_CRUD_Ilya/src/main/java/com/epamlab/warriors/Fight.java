package com.epamlab.warriors;

import com.epamlab.monsters.AbstractMonster;

public interface Fight {
    void killMonster(AbstractMonster monster,int useSkillOrNot);
}
