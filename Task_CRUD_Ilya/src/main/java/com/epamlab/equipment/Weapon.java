package com.epamlab.equipment;

import com.epamlab.constants.Constants;

import java.util.Objects;

/**
 * Class Weapon describes model of weapon in simple game
 *
 * @author Uladzimir Khodosovckiy
 */
public class Weapon {
    /**
     * This field contains information about type of weapon
     */
    private String type;
    /**
     * This field contains information about damage of weapon
     */
    private int damage;

    /**
     * This field contains id of weapon
     */
    private int id;

    /**
     * Constructor without params
     */
    public Weapon() { }

    /**
     * Constructor
     *
     * @param type   - health of weapon
     * @param damage - damage of weapon
     */
    public Weapon(int id, String type, int damage) {
        this.id = id;
        this.type = type;
        this.damage = damage;
    }

    /**
     * getter
     *
     * @return id of weapon
     */
    public int getId() {
        return id;
    }

    /**
     * setter
     *
     * @param id - id of weapon
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getter
     *
     * @return type of weapon
     */
    public String getType() {
        return type;
    }

    /**
     * setter
     *
     * @param type - type of weapon
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * getter
     *
     * @return damage of weapon
     */
    public int getDamage() {
        return damage;
    }

    /**
     * setter
     *
     * @param damage - damage of weapon
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * This method shows information about weapon
     *
     * @return object information
     */
    @Override
    public String toString() {
        return new StringBuilder("Id of weapon: ").
                append(id + Constants.SEMICOLON).
                append("Name of weapon: ").
                append(type + Constants.SEMICOLON).
                append("Damage of weapon :").
                append(damage).
                toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weapon weapon = (Weapon) o;
        return damage == weapon.damage &&
                id == weapon.id &&
                Objects.equals(type, weapon.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, damage, id);
    }
}
