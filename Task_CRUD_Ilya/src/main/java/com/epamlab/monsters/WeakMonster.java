package com.epamlab.monsters;



/**
 * Class WeakMonster describes model of weak monster in simple game
 * - extends – AbstractMonster
 * @author Uladzimir Khodosovckiy
 */
public class WeakMonster extends AbstractMonster {

    /**
     * Constructor
     * @param health - health of monster
     * @param damage - damage of monster
     * @param experienceAfterKill - experience after kill the monster
     */
    public WeakMonster(int health, int damage, int experienceAfterKill) {
        super(health, damage, experienceAfterKill);
    }


}
