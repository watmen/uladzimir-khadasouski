package com.epamlab;

import com.epamlab.constants.Constants;
import com.epamlab.dao.DaoSkill;
import com.epamlab.dao.DaoWarrior;
import com.epamlab.dao.DaoWeapon;
import com.epamlab.equipment.Weapon;
import com.epamlab.factories.MonsterTypeFactory;
import com.epamlab.factories.WarriorTypeFactory;
import com.epamlab.monsters.AbstractMonster;
import com.epamlab.skills.Skill;
import com.epamlab.utils.CheckUserAnswer;
import com.epamlab.warriors.AbstractWarrior;
import com.epamlab.warriors.Paladin;


import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Class Runner shows functional of program
 *
 * @author Uladzimir Khodosovckiy
 */
public class Runner {
    /**
     * This field contains number, which is the last id of warrior
     */
    private static final int RIGHT_LIMIT_OF_WARRIOR_ID = 3;
    /**
     * This field contains number, which is the last id of monster
     */
    private static final int RIGHT_LIMIT_OF_MONSTER_ID = 3;
    /**
     * This field contains message to user that he/she inputs wrong type of warrior
     */
    private static final String MESSAGE_WRONG_ID_OF_WARRIOR = "You input wrong type of warrior.\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user that he/she inputs wrong id of existing skill
     */
    private static final String MESSAGE_WRONG_ID_OF_EXISTING_SKILL = "Wrong id of existing skills\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user that he/she inputs wrong id of existing weapon
     */
    private static final String MESSAGE_WRONG_ID_OF_EXISTING_WEAPON = "Wrong id of existing skills\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user, which offers to choose class of warrior
     */
    private static final String MESSAGE_CHOOSE_WARRIOR = "Choose class of your warrior:\n" +
            "1 - paladin\n" +
            "2 - magician\n" +
            "3 - rogue";
    /**
     * This field contains message to user, which report that user enter the area with mobs
     */
    private static final String MESSAGE_ENTER_THE_AREA = "You entered the area.";
    /**
     * This field contains message to user, which offers to choose type of monster
     */
    private static final String MESSAGE_CHOOSE_TYPE_OF_MONSTER = "Choose type of monster:\n" +
            "1 - weak monster\n" +
            "2 - middle monster\n" +
            "3 - strong monster";
    /**
     * This field contains message to user that he input wrong id of monster
     */
    private static final String MESSAGE_WRONG_ID_OF_MONSTER = "You input wrong id of monster.\n" +
            "Re-enter, please ";
    /**
     * This field contains message to user, which offers continue or finish game
     */
    private static final String MESSAGE_CONTINUE_ADVENTURE = "Do you want to continue your adventure?\n" +
            "1 - yes\n" +
            "2 - no";
    /**
     * This field contains message to user that he/she inputs wrong data
     */
    private static final String MESSAGE_INCORRECT_DATA = "You input wrong data!!!";
    /**
     * This field contains message to user, wherein the developer wishes good luck to user
     */
    private static final String MESSAGE_GOOD_LUCK = "Good luck, hero!";
    /**
     * This field contains id of new warrior
     */
    private static int idOfNewWarrior = 0;
    /**
     * This field contains id of new skill
     */
    private static int idOfNewSkill = 3;
    /**
     * This field contains id of new weapon
     */
    private static int idOfNewWeapon = 3;
    /**
     * This field contains number of existing warriors
     */
    private static int numberOfExistingWarrior = 0;
    /**
     * This field contains number of existing skills
     */
    private static int numberOfExistingSkills = 2;
    /**
     * This field contains number of existing weapons
     */
    private static int numberOfExistingWeapons = 2;
    /**
     * This field contains dao layer for warrior
     */
    private static DaoWarrior daoWarriors = new DaoWarrior();
    /**
     * This field contains dao layer for skill
     */
    private static DaoSkill daoSkills = new DaoSkill();
    /**
     * This field contains dao layer for weapon
     */
    private static DaoWeapon daoWeapon = new DaoWeapon();

    /**
     * This method check id of warrior, which user inputs
     *
     * @param idOfWarriorType -type of warrior
     * @return id of warrior right or not
     */
    private static boolean wrongIdOfWarrior(int idOfWarriorType) {
        return idOfWarriorType < Constants.POSITIVE_LIMIT || idOfWarriorType > RIGHT_LIMIT_OF_WARRIOR_ID;
    }

    /**
     * This method check id of monster, which user inputs
     *
     * @param idOfMonster - type of monster
     * @return id of monster right or not
     */
    private static boolean wrongIdOfMonster(int idOfMonster) {
        return idOfMonster < Constants.POSITIVE_LIMIT || idOfMonster > RIGHT_LIMIT_OF_MONSTER_ID;
    }

    /**
     * This method check id of smth: id of existing warrior, skill or weapon
     *
     * @param id         - if of smth
     * @param rightLimit - right limit of smth
     * @return id right or not
     */
    private static boolean wrongId(int id, int rightLimit) {
        return id < Constants.POSITIVE_LIMIT || id > rightLimit;
    }

    /**
     * This method check is there  warrior in the List
     *
     * @param warrior - warrior to check
     * @return is there a warrior or not
     */
    private static boolean noSuchWarriorInList(AbstractWarrior warrior) {
        return !daoWarriors.getAll().contains(warrior);
    }

    /**
     * This method show all existing warriors
     *
     * @param warriors - list of all warriors
     */
    private static void showAllWarriors(List<AbstractWarrior> warriors) {
        for (AbstractWarrior warrior : warriors) {
            Constants.log.info(warrior);
        }
    }

    /**
     * This method show all existing skills
     *
     * @param skills - list of all skills
     */
    private static void showAllSkills(List<Skill> skills) {
        for (Skill skill : skills) {
            Constants.log.info(skill);
        }
    }

    /**
     * This method show all existing weapons
     *
     * @param weapons - list of all weapons
     */
    private static void showAllWeapons(List<Weapon> weapons) {
        for (Weapon weapon : weapons) {
            Constants.log.info(weapon);
        }
    }

    /**
     * This method create new warrior
     *
     * @return new warrior
     */
    private static AbstractWarrior createNewWarriors() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int idOfNewSkill;
        int idOfNewWeapon;
        Skill skill;
        Weapon weapon;

        Constants.log.info(MESSAGE_CHOOSE_WARRIOR);
        int idOfWarriorType = input.nextInt();
        while (wrongIdOfWarrior(idOfWarriorType)) {
            Constants.log.error(MESSAGE_WRONG_ID_OF_WARRIOR);
            idOfWarriorType = input.nextInt();
        }
        Constants.log.info("Add name of warrior:");
        String nameOfWarrior = input.next();
        Constants.log.info("Choose id of skill, which you want:");
        showAllSkills(daoSkills.getAll());
        idOfNewSkill = input.nextInt();
        while (wrongId(idOfNewSkill, numberOfExistingSkills)) {
            Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_SKILL);
            idOfNewSkill = input.nextInt();
        }
        skill = daoSkills.get(idOfNewSkill);
        Constants.log.info("Choose id of weapon, which you want");
        idOfNewWeapon = input.nextInt();
        while (wrongId(idOfNewWeapon, numberOfExistingWeapons)) {
            Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_WEAPON);
            idOfNewWeapon = input.nextInt();
        }
        showAllWeapons(daoWeapon.getAll());
        weapon = daoWeapon.get(input.nextInt());
        return WarriorTypeFactory.getWarrior(idOfWarriorType, idOfNewWarrior, nameOfWarrior, skill, weapon);
    }

    /**
     * This method return existing warrior
     *
     * @return existing warrior
     */
    private static AbstractWarrior getExistingWarrior() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int idOfExistingOfWarrior;

        Constants.log.info("Created warriors");
        showAllWarriors(daoWarriors.getAll());
        Constants.log.info("Input id of existing warrior");
        idOfExistingOfWarrior = input.nextInt();
        while (wrongId(idOfExistingOfWarrior, numberOfExistingWarrior)) {
            Constants.log.error("Wrong id of existing warrior\n" +
                    "Re-enter, please ");
            idOfExistingOfWarrior = input.nextInt();
        }
        return daoWarriors.get(idOfExistingOfWarrior);
    }

    /**
     * This method show logic of fight with monster
     *
     * @param warrior - warrior, which will fight with monster
     */
    private static void fightWithMonster(AbstractWarrior warrior) throws InputMismatchException {
        Scanner input = new Scanner(System.in);

        Constants.log.info(MESSAGE_ENTER_THE_AREA);
        Constants.log.info(MESSAGE_CHOOSE_TYPE_OF_MONSTER);
        int idOfMonster = input.nextInt();
        while (wrongIdOfMonster(idOfMonster)) {
            Constants.log.error(MESSAGE_WRONG_ID_OF_MONSTER);
            idOfMonster = input.nextInt();
        }
        AbstractMonster monster = MonsterTypeFactory.getMonster(idOfMonster);
        killMonster(monster,warrior);
        if (warrior.canWarriorUpLevel()) {
            warrior.upLevel();
        }
        Constants.log.info(warrior);
    }

    private static void killMonster(AbstractMonster monster, AbstractWarrior warrior){
        Scanner in = new Scanner(System.in);
        int startHealth = warrior.getHealth();
        int startMana = warrior.getMana();
        int answer;
        int useSkillOrNot;

        while (monster.monsterAlive()) {
            Constants.log.info("Do you want to use your skill: " + warrior.getSkill() + " \n" +
                    "1 - yes\n" +
                    "2 - no");
            answer = in.nextInt();
            useSkillOrNot = CheckUserAnswer.inputRightAnswer(answer);
            warrior.killMonster(monster,useSkillOrNot);
            if (warrior.warriorDead()) {
                Constants.log.info("You died!");
                break;
            }
        }
        if (monster.getHealth() <= Constants.POSITIVE_LIMIT) {
            warrior.addExperience(monster.getExperienceAfterKill());
            Constants.log.info("You killed the monster");
        }
        warrior.healthAndManaRecovery(startHealth, startMana);
    }

    /**
     * This method add new skill
     */
    private static void addNewSkill() {
        Scanner input = new Scanner(System.in);

        Constants.log.info("Add name of skill");
        String nameOfSkill = input.next();
        Constants.log.info("Add damage of skill");
        int damageOfSkill = input.nextInt();
        Constants.log.info("Add mana cost of skill");
        int manaCostOfSKill = input.nextInt();
        daoSkills.add(new Skill(idOfNewSkill, nameOfSkill, damageOfSkill, manaCostOfSKill));
    }

    /**
     * This method update skill
     */
    private static void updateSkill(int idOfSkill) {
        Scanner input = new Scanner(System.in);

        Constants.log.info("Add new name of skill");
        String nameOfSkill = input.next();
        Constants.log.info("Add new damage of skill");
        int damageOfSkill = input.nextInt();
        Constants.log.info("Add new mana cost of skill");
        int manaCostOfSKill = input.nextInt();
        daoSkills.update(new Skill(idOfSkill,nameOfSkill,damageOfSkill,manaCostOfSKill));
    }

    /**
     * This method add new weapon
     */
    private static void addNewWeapon() {
        Scanner input = new Scanner(System.in);

        Constants.log.info("Add name of weapon");
        String nameOfWeapon = input.next();
        Constants.log.info("Add damage of weapon");
        int damageOfWeapon = input.nextInt();
        daoWeapon.add(new Weapon(idOfNewWeapon, nameOfWeapon, damageOfWeapon));
    }

    /**
     * This method update weapon
     */
    private static void updateWeapon(int idOfWeapon) {
        Scanner input = new Scanner(System.in);

        Constants.log.info("Add new name of weapon");
        String nameOfWeapon = input.next();
        Constants.log.info("Add new damage of weapon");
        int damageOfWeapon = input.nextInt();
        daoWeapon.update(new Weapon(idOfWeapon,nameOfWeapon,damageOfWeapon));
    }

    /**
     * This method update warrior
     */
    private static void updateWarrior(int idOfWarrior) {
        Scanner input = new Scanner(System.in);
        Constants.log.info("Enter new name of warrior");
        String newNameOfWarrior = input.next();
        daoWarriors.update(new Paladin(idOfWarrior,newNameOfWarrior,
                0,0,
                0,new Weapon(0,"0",0),
                0,0,
                new Skill(0,"0",0,0)));
    }

    /**
     * This method change settings of skills
     */
    private static void changeSettingsOfSKills() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int typeOfChange;
        int numberOfPossibleChanges = 3;
        int idOfRemoteSkill;
        int idOfChangingSkill;

        Constants.log.info("What you want to do?\n" +
                "1 - add new skill\n" +
                "2 - update existing skill\n" +
                "3 - delete existing skill");
        typeOfChange = input.nextInt();
        while (wrongId(typeOfChange, numberOfPossibleChanges)) {
            Constants.log.info("You input wrong type of changes.\n" +
                    "Re-enter, please ");
            typeOfChange = input.nextInt();
        }
        if (typeOfChange == 1) {
            addNewSkill();
            idOfNewSkill++;
            numberOfExistingSkills++;
        } else if (typeOfChange == 2) {
            showAllSkills(daoSkills.getAll());
            Constants.log.info("Input id of changing skill");
            idOfChangingSkill = input.nextInt();
            while (wrongId(idOfChangingSkill, numberOfExistingSkills)) {
                Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_SKILL);
                idOfChangingSkill = input.nextInt();
            }
            updateSkill(idOfChangingSkill);
        } else {
            showAllSkills(daoSkills.getAll());
            Constants.log.info("Input id of remote skill");
            idOfRemoteSkill = input.nextInt();
            while (wrongId(idOfRemoteSkill, numberOfExistingSkills)) {
                Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_SKILL);
                idOfRemoteSkill = input.nextInt();
            }
            daoSkills.delete(idOfRemoteSkill);
        }
    }

    /**
     * This method change settings of warriors
     */
    private static void changeSettingsOfWarriors() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int typeOfChange;
        int numberOfPossibleChanges = 2;
        int idOfRemoteWarrior;
        int idOfChangingWarrior;

        Constants.log.info("What you want to do?\n" +
                "1 - change name of existing warrior\n" +
                "2 - delete existing warrior");
        typeOfChange = input.nextInt();
        while (wrongId(typeOfChange, numberOfPossibleChanges)) {
            Constants.log.info("You input wrong type of changes.\n" +
                    "Re-enter, please ");
            typeOfChange = input.nextInt();
        }
        if (typeOfChange == 1) {
            showAllWarriors(daoWarriors.getAll());
            Constants.log.info("Enter id of changing warrior");
            idOfChangingWarrior = input.nextInt();
            updateWarrior(idOfChangingWarrior);
        } else {
            showAllWarriors(daoWarriors.getAll());
            Constants.log.info("Enter id of remote warrior");
            idOfRemoteWarrior = input.nextInt();
            daoWarriors.delete(idOfRemoteWarrior);
        }
    }

    /**
     * This method change settings of weapons
     */
    private static void changeSettingsOfWeapons() throws InputMismatchException {
        Scanner input = new Scanner(System.in);
        int typeOfChange;
        int numberOfPossibleChanges = 3;
        int idOfChangingWeapon;
        int idOfRemoteWeapon;

        Constants.log.info("What you want to do?\n" +
                "1 - add new weapon\n" +
                "2 - update existing weapon\n" +
                "3 - delete existing weapon");
        typeOfChange = input.nextInt();
        while (wrongId(typeOfChange, numberOfPossibleChanges)) {
            Constants.log.info("You input wrong type of changes.\n" +
                    "Re-enter, please ");
            typeOfChange = input.nextInt();
        }
        if (typeOfChange == 1) {
            addNewWeapon();
            idOfNewWeapon++;
            numberOfExistingWeapons++;
        } else if (typeOfChange == 2) {
            showAllWeapons(daoWeapon.getAll());
            Constants.log.info("Input id of changing weapon");
            idOfChangingWeapon = input.nextInt();
            while (wrongId(idOfChangingWeapon, numberOfExistingWeapons)) {
                Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_WEAPON);
                idOfChangingWeapon = input.nextInt();
            }
            updateWeapon(idOfChangingWeapon);
        } else {
            showAllWeapons(daoWeapon.getAll());
            Constants.log.info("Input id of remote weapon");
            idOfRemoteWeapon = input.nextInt();
            while (wrongId(idOfRemoteWeapon, numberOfExistingWeapons)) {
                Constants.log.error(MESSAGE_WRONG_ID_OF_EXISTING_WEAPON);
                idOfRemoteWeapon = input.nextInt();
            }
            daoWeapon.delete(idOfRemoteWeapon);
        }
    }

    /**
     * This method shows functional
     *
     * @param args - command line array
     */
    public static void main(String[] args) {
        try {
            int answer;
            Scanner input = new Scanner(System.in);
            AbstractWarrior warrior;

            do {
                Constants.log.info("Do you want create new or use existing? \n" +
                        "1 - create new\n" +
                        "2 - use existing");
                answer = CheckUserAnswer.inputRightAnswer(input.nextInt());
                if (answer == 2) {
                    if (daoWarriors.getAll().isEmpty()) {
                        Constants.log.error("No warriors. You can only create new warrior");
                        answer = 1;
                    }
                }
                if (answer == 1) {
                    warrior = createNewWarriors();
                    idOfNewWarrior++;
                    numberOfExistingWarrior++;
                } else {
                    warrior = getExistingWarrior();
                }

                do {
                    fightWithMonster(warrior);
                    Constants.log.info(MESSAGE_CONTINUE_ADVENTURE);
                    answer = CheckUserAnswer.inputRightAnswer(input.nextInt());
                } while (answer == 1);

                if (noSuchWarriorInList(warrior)) {
                    daoWarriors.add(warrior);
                }

                Constants.log.info("Do you want to change settings of game?\n" +
                        "1 - yes\n" +
                        "2 - no");
                answer = CheckUserAnswer.inputRightAnswer(input.nextInt());
                if (answer == 1) {
                    do {
                        Constants.log.info("What you want ot change?\n" +
                                "1 - skills\n" +
                                "2 - warriors\n" +
                                "3 - weapon");
                        answer = input.nextInt();
                        if (answer == 1) {
                            changeSettingsOfSKills();
                        } else if (answer == 2) {
                            changeSettingsOfWarriors();
                        } else {
                            changeSettingsOfWeapons();
                        }
                        Constants.log.info("Do you want to continue changes?\n" +
                                "1 - yes\n" +
                                "2 - no");
                        answer = CheckUserAnswer.inputRightAnswer(input.nextInt());
                    } while (answer == 1);
                }
                Constants.log.info("Do you want to continue game:\n" +
                        "1 - yes\n" +
                        "2 - no");
                answer = CheckUserAnswer.inputRightAnswer(input.nextInt());
            } while (answer == 1);
            Constants.log.info(MESSAGE_GOOD_LUCK);
        } catch (InputMismatchException e) {
            Constants.log.fatal(MESSAGE_INCORRECT_DATA);
        }
    }
}
