package com.epamlab.skills;

import com.epamlab.constants.Constants;

import java.util.Objects;

/**
 * Class Skill describes model of skill in simple game
 *
 * @author Uladzimir Khodosovckiy
 */
public class Skill {
    /**
     * This field contains message to user, which shows name of skill
     */
    private static final String MESSAGE_ABOUT_NAME_OF_SKILL = "Name of skill: ";
    /**
     * This field contains message to user, which shows mana cost of skill
     */
    private static final String MESSAGE_ABOUT_MANA_COST_OF_SKILL = "Mana cost of skill: ";
    /**
     * This field contains message to user, which shows damage of skill
     */
    private static final String MESSAGE_ABOUT_DAMAGE_OF_SKILL = "Damage of skill: ";
    /**
     * This field contains message to user, which shows id of skill
     */
    private static final String MESSAGE_ID_OF_SKILL = "Id of skill: ";
    /**
     * This field contains information about name of skill
     */
    private String name;
    /**
     * This field contains information about manaCost of skill
     */
    private int manaCost;
    /**
     * This field contains information about damage of skill
     */
    private int damage;
    /**
     * This field contains id of skill
     */
    private int id;

    /**
     * Constructor without params
     */
    public Skill() {
    }

    /**
     * Constructor
     *
     * @param name     - name of skill
     * @param damage   - damage of skill
     * @param manaCost - manaCost of skill
     */
    public Skill(int id, String name, int manaCost, int damage) {
        this.id = id;
        this.name = name;
        this.manaCost = manaCost;
        this.damage = damage;
    }

    /**
     * getter
     *
     * @return id of skill
     */
    public int getId() {
        return id;
    }

    /**
     * setter
     *
     * @param id - id of skill
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * getter
     *
     * @return name of skill
     */
    public String getName() {
        return name;
    }

    /**
     * setter
     *
     * @param name - name of skill
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * getter
     *
     * @return manaCost of skill
     */
    public int getManaCost() {
        return manaCost;
    }

    /**
     * setter
     *
     * @param manaCost - manaCost of skill
     */
    public void setManaCost(int manaCost) {
        this.manaCost = manaCost;
    }

    /**
     * getter
     *
     * @return damage of skill
     */
    public int getDamage() {
        return damage;
    }

    /**
     * setter
     *
     * @param damage - damage of skill
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * This method shows information about skill
     *
     * @return object information
     */
    @Override
    public String toString() {
        return new StringBuilder(MESSAGE_ID_OF_SKILL).
                append(id + Constants.SEMICOLON).
                append(MESSAGE_ABOUT_NAME_OF_SKILL).
                append(name).
                append(Constants.SEMICOLON).
                append(MESSAGE_ABOUT_MANA_COST_OF_SKILL).
                append(manaCost).
                append(Constants.SEMICOLON).
                append(MESSAGE_ABOUT_DAMAGE_OF_SKILL).
                append(damage).
                toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Skill skill = (Skill) o;
        return manaCost == skill.manaCost &&
                damage == skill.damage &&
                id == skill.id &&
                Objects.equals(name, skill.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, manaCost, damage, id);
    }
}
