package com.epamlab.dao;


import com.epamlab.constants.Constants;
import com.epamlab.warriors.AbstractWarrior;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class DaoWarrior describes dao layer of warrior
 * - implements – Dao<T>
 *
 * @author Uladzimir Khodosovckiy
 */
public class DaoWarrior implements Dao<AbstractWarrior> {
    /**
     * This field contains List of warriors
     */
    private List<AbstractWarrior> warriors;

    /**
     * Constructor without params
     */
    public DaoWarrior() {
        warriors = new ArrayList<AbstractWarrior>();
    }

    /**
     * This method return needed warrior
     *
     * @param id - id of warrior, which user want to get
     * @return warrior, which user want
     */
    public AbstractWarrior get(int id) {
        return warriors.get(id);
    }

    /**
     * This method return all warriors
     *
     * @return List of warriors
     */
    public List<AbstractWarrior> getAll() {
        return warriors;
    }

    /**
     * This method add new warrior
     *
     * @param warrior - warrior, which user want to add
     */
    public void add(AbstractWarrior warrior) {
        warriors.add(warrior);
    }

    /**
     * This method update warrior
     *
     * @param warrior - warrior, which user want update
     */
    public void update(AbstractWarrior warrior) {
        warriors.get(warrior.getId()).setName(warrior.getName());
    }

    /**
     * This method delete warrior
     *
     * @param id - id of warrior, which user want to delete
     */
    public void delete(int id) {
        warriors.remove(id);
    }
}
