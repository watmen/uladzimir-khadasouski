package com.epamlab.dao;

import com.epamlab.constants.Constants;
import com.epamlab.skills.Skill;
import com.epamlab.warriors.AbstractWarrior;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Class DaoSkill describes dao layer of skill
 * - implements – Dao<T>
 *
 * @author Uladzimir Khodosovckiy
 */
public class DaoSkill implements Dao<Skill> {
    /**
     * This field contains skill "kick Of"
     */
    private static final Skill KICK_OF = new Skill(0, "Kick Of", 30, 30);
    /**
     * This field contains skill "sanctification"
     */
    private static final Skill SANCTIFICATION = new Skill(1, "Sanctification", 40, 10);
    /**
     * This field contains skill "hail of stones"
     */
    private static final Skill HAIL_OF_STONES = new Skill(2, "Hail of stones", 30, 50);

    /**
     * This field contains List of skills
     */
    private List<Skill> skills;

    /**
     * Constructor without params, which creates standard skills
     */
    public DaoSkill() {
        skills = new ArrayList<>();
        skills.add(KICK_OF);
        skills.add(SANCTIFICATION);
        skills.add(HAIL_OF_STONES);
    }

    /**
     * This method return needed skill
     *
     * @param id - id of skill, which user want to get
     * @return skill, which user want
     */
    public Skill get(int id) {
        return skills.get(id);
    }

    /**
     * This method return all skills
     *
     * @return List of skills
     */
    public List<Skill> getAll() {
        return skills;
    }

    /**
     * This method add new skill
     *
     * @param skill - skill, which user want to add
     */
    public void add(Skill skill) {
        skills.add(skill);
    }

    /**
     * This method update skill
     *
     * @param skill - skill, which user want update
     */
    public void update(Skill skill){
        skills.get(skill.getId()).setName(skill.getName());
        skills.get(skill.getId()).setDamage(skill.getDamage());
        skills.get(skill.getId()).setManaCost(skill.getManaCost());

    }

    /**
     * This method delete skill
     *
     * @param id - id of skill, which user want to delete
     */
    public void delete(int id) {
        skills.remove(id);
    }
}
