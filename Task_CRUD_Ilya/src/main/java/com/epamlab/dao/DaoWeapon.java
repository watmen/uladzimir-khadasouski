package com.epamlab.dao;

import com.epamlab.constants.Constants;
import com.epamlab.equipment.Weapon;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Class DaoWeapon describes dao layer of weapon
 * - implements – Dao<T>
 *
 * @author Uladzimir Khodosovckiy
 */
public class DaoWeapon implements Dao<Weapon> {

    /**
     * This field contains weapon stuff
     */
    private static final Weapon WEAPON_STUFF = new Weapon(0, "Stuff", 9);
    /**
     * This field contains weapon sword
     */
    private static final Weapon WEAPON_SWORD = new Weapon(1, "SWORD", 20);
    /**
     * This field contains weapon dagger
     */
    private static final Weapon WEAPON_DAGGER = new Weapon(2, "Dagger", 10);

    /**
     * This field contains List of weapons
     */
    private List<Weapon> weapons;

    /**
     * Constructor without params, which creates standard weapons
     */
    public DaoWeapon() {
        weapons = new ArrayList<Weapon>();
        weapons.add(WEAPON_STUFF);
        weapons.add(WEAPON_SWORD);
        weapons.add(WEAPON_DAGGER);
    }

    /**
     * This method return needed weapon
     *
     * @param id - id of weapon, which user want to get
     * @return weapon, which user want
     */
    public Weapon get(int id) {
        return weapons.get(id);
    }

    /**
     * This method return all weapons
     *
     * @return List of weapons
     */
    public List<Weapon> getAll() {
        return weapons;
    }

    /**
     * This method add new weapon
     *
     * @param weapon - weapon, which user want to add
     */
    public void add(Weapon weapon) {
        weapons.add(weapon);
    }

    /**
     * This method update weapon
     *
     * @param weapon - weapon, which user want update
     */
    public void update(Weapon weapon){
        weapons.get(weapon.getId()).setType(weapon.getType());
        weapons.get(weapon.getId()).setDamage(weapon.getDamage());
    }

    /**
     * This method delete weapon
     *
     * @param id - id of weapon, which user want to delete
     */
    public void delete(int id) {
        weapons.remove(id);
    }
}
