
package com.epamlabs.xmlMarshalling;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "air",
    "pricingInfo",
    "ticketingInfo"
})
@XmlRootElement(name = "Itinerary", namespace = "VolodNine")
public class Itinerary {

    @XmlElement(name = "Air", namespace = "VolodNine", required = true)
    protected Itinerary.Air air;
    @XmlElement(name = "PricingInfo", namespace = "VolodNine", required = true)
    protected Itinerary.PricingInfo pricingInfo;
    @XmlElement(name = "TicketingInfo", namespace = "VolodNine", required = true)
    protected Itinerary.TicketingInfo ticketingInfo;
    @XmlAttribute(name = "SequenceNumber")
    protected int sequenceNumber;
    @XmlAttribute(name = "RPH")
    protected int rph;
    @XmlAttribute(name = "ItineraryID")
    protected int itineraryID;

    /**
     * Gets the value of the air property.
     *
     * @return
     *     possible object is
     *     {@link Itinerary.Air }
     *
     */
    public Itinerary.Air getAir() {
        return air;
    }

    /**
     * Sets the value of the air property.
     *
     * @param value
     *     allowed object is
     *     {@link Itinerary.Air }
     *
     */
    public void setAir(Itinerary.Air value) {
        this.air = value;
    }

    /**
     * Gets the value of the pricingInfo property.
     *
     * @return
     *     possible object is
     *     {@link Itinerary.PricingInfo }
     *
     */
    public Itinerary.PricingInfo getPricingInfo() {
        return pricingInfo;
    }

    /**
     * Sets the value of the pricingInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link Itinerary.PricingInfo }
     *
     */
    public void setPricingInfo(Itinerary.PricingInfo value) {
        this.pricingInfo = value;
    }

    /**
     * Gets the value of the ticketingInfo property.
     *
     * @return
     *     possible object is
     *     {@link Itinerary.TicketingInfo }
     *
     */
    public Itinerary.TicketingInfo getTicketingInfo() {
        return ticketingInfo;
    }

    /**
     * Sets the value of the ticketingInfo property.
     *
     * @param value
     *     allowed object is
     *     {@link Itinerary.TicketingInfo }
     *
     */
    public void setTicketingInfo(Itinerary.TicketingInfo value) {
        this.ticketingInfo = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setSequenceNumber(int value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the rph property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public int getRPH() {
        return rph;
    }

    /**
     * Sets the value of the rph property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setRPH(int value) {
        this.rph = value;
    }

    /**
     * Gets the value of the itineraryID property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public int getItineraryID() {
        return itineraryID;
    }

    /**
     * Sets the value of the itineraryID property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setItineraryID(int value) {
        this.itineraryID = value;
    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "options",
        "extension"
    })
    public static class Air {

        @XmlElement(name = "Options", namespace = "VolodNine", required = true)
        protected Itinerary.Air.Options options;
        @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
        protected Itinerary.Air.Extension extension;
        @XmlAttribute(name = "Direction")
        protected String direction;
        @XmlAttribute(name = "TotalTravelTime")
        protected String totalTravelTime;

        /**
         * Gets the value of the options property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.Air.Options }
         *
         */
        public Itinerary.Air.Options getOptions() {
            return options;
        }

        /**
         * Sets the value of the options property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.Air.Options }
         *
         */
        public void setOptions(Itinerary.Air.Options value) {
            this.options = value;
        }

        /**
         * Gets the value of the extensionFlight property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.Air.Extension }
         *
         */
        public Itinerary.Air.Extension getExtension() {
            return extension;
        }

        /**
         * Sets the value of the extensionFlight property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.Air.Extension }
         *
         */
        public void setExtension(Itinerary.Air.Extension value) {
            this.extension = value;
        }

        /**
         * Gets the value of the direction property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getDirection() {
            return direction;
        }

        /**
         * Sets the value of the direction property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setDirection(String value) {
            this.direction = value;
        }

        /**
         * Gets the value of the totalTravelTime property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getTotalTravelTime() {
            return totalTravelTime;
        }

        /**
         * Sets the value of the totalTravelTime property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setTotalTravelTime(String value) {
            this.totalTravelTime = value;
        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "attributes"
        })
        public static class Extension {

            @XmlElement(name = "Attributes", namespace = "VolodNine", required = true)
            protected Itinerary.Air.Extension.Attributes attributes;

            /**
             * Gets the value of the attributes property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.Air.Extension.Attributes }
             *
             */
            public Itinerary.Air.Extension.Attributes getAttributes() {
                return attributes;
            }

            /**
             * Sets the value of the attributes property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.Air.Extension.Attributes }
             *
             */
            public void setAttributes(Itinerary.Air.Extension.Attributes value) {
                this.attributes = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Attributes {

                @XmlAttribute(name = "TripType")
                protected String tripType;

                /**
                 * Gets the value of the tripType property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getTripType() {
                    return tripType;
                }

                /**
                 * Sets the value of the tripType property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setTripType(String value) {
                    this.tripType = value;
                }

            }

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "option"
        })
        public static class Options {

            @XmlElement(name = "Option", namespace = "VolodNine", required = true)
            protected Itinerary.Air.Options.Option option;

            /**
             * Gets the value of the option property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.Air.Options.Option }
             *
             */
            public Itinerary.Air.Options.Option getOption() {
                return option;
            }

            /**
             * Sets the value of the option property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.Air.Options.Option }
             *
             */
            public void setOption(Itinerary.Air.Options.Option value) {
                this.option = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "flight",
                "extension"
            })
            public static class Option {

                @XmlElement(name = "Flight", namespace = "VolodNine")
                protected List<Flight> flight;
                @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
                protected Itinerary.Air.Options.Option.Extension extension;

                /**
                 * Gets the value of the flight property.
                 *
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the flight property.
                 *
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getFlight().add(newItem);
                 * </pre>
                 *
                 *
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Itinerary.Air.Options.Option.Flight }
                 *
                 *
                 */
                public List<Flight> getFlight() {
                    if (flight == null) {
                        flight = new ArrayList<Flight>();
                    }
                    return this.flight;
                }

                public void setFlight(List<Flight> flight) {
                    this.flight = flight;
                }

                /**
                 * Gets the value of the extensionFlight property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.Air.Options.Option.Extension }
                 *
                 */
                public Itinerary.Air.Options.Option.Extension getExtension() {
                    return extension;
                }

                /**
                 * Sets the value of the extensionFlight property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.Air.Options.Option.Extension }
                 *
                 */
                public void setExtension(Itinerary.Air.Options.Option.Extension value) {
                    this.extension = value;
                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "inventorySystem",
                    "source"
                })
                public static class Extension {

                    @XmlElement(name = "InventorySystem", namespace = "VolodNine", required = true)
                    protected Itinerary.Air.Options.Option.Extension.InventorySystem inventorySystem;
                    @XmlElement(name = "Source", namespace = "VolodNine", required = true)
                    protected String source;

                    /**
                     * Gets the value of the inventorySystem property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.Air.Options.Option.Extension.InventorySystem }
                     *
                     */
                    public Itinerary.Air.Options.Option.Extension.InventorySystem getInventorySystem() {
                        return inventorySystem;
                    }

                    /**
                     * Sets the value of the inventorySystem property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.Air.Options.Option.Extension.InventorySystem }
                     *
                     */
                    public void setInventorySystem(Itinerary.Air.Options.Option.Extension.InventorySystem value) {
                        this.inventorySystem = value;
                    }

                    /**
                     * Gets the value of the source property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getSource() {
                        return source;
                    }

                    /**
                     * Sets the value of the source property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setSource(String value) {
                        this.source = value;
                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class InventorySystem {

                        @XmlAttribute(name = "Name")
                        protected String name;

                        /**
                         * Gets the value of the name property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Sets the value of the name property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }

                }


                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name="" , propOrder={"extensionFlight", "departureDateTime","arrivalDateTime",
                        "bookCode", "stopQuantity", "rph", "flightNumber", "numberInParty"})
                public static class Flight {

                    @XmlElement(name = "Extension", namespace = "VolodNine")
                    protected List<Extension> extensionFlight;
                    @XmlAttribute(name = "DepartureDateTime")
                    protected Date departureDateTime;
                    @XmlAttribute(name = "ArrivalDateTime")
                    protected Date arrivalDateTime;
                    @XmlAttribute(name = "BookCode")
                    protected String bookCode;
                    @XmlAttribute(name = "StopQuantity")
                    protected String stopQuantity;
                    @XmlAttribute(name = "RPH")
                    protected int rph;
                    @XmlAttribute(name = "FlightNumber")
                    protected int flightNumber;
                    @XmlAttribute(name = "NumberInParty")
                    protected int numberInParty;

                    public Flight() {
                    }

                    public List<Extension> getExtensionFlight() {
                        if (extensionFlight == null) {
                            extensionFlight = new ArrayList<Extension>();
                        }
                        return this.extensionFlight;
                    }

                    public void setExtensionFlight(List<Extension> extensionFlight) {
                        this.extensionFlight = extensionFlight;
                    }

                    /**
                     * Gets the value of the departureDateTime property.
                     *
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *
                     */
                    public Date getDepartureDateTime() {
                        return departureDateTime;
                    }

                    /**
                     * Sets the value of the departureDateTime property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *
                     */
                    public void setDepartureDateTime(Date value) {
                        this.departureDateTime = value;
                    }

                    /**
                     * Gets the value of the arrivalDateTime property.
                     *
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *
                     */
                    public Date getArrivalDateTime() {
                        return arrivalDateTime;
                    }

                    /**
                     * Sets the value of the arrivalDateTime property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *
                     */
                    public void setArrivalDateTime(Date value) {
                        this.arrivalDateTime = value;
                    }

                    /**
                     * Gets the value of the bookCode property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getBookCode() {
                        return bookCode;
                    }

                    /**
                     * Sets the value of the bookCode property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setBookCode(String value) {
                        this.bookCode = value;
                    }

                    /**
                     * Gets the value of the stopQuantity property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getStopQuantity() {
                        return stopQuantity;
                    }

                    /**
                     * Sets the value of the stopQuantity property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setStopQuantity(String value) {
                        this.stopQuantity = value;
                    }

                    /**
                     * Gets the value of the rph property.
                     *
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *
                     */
                    public int getRPH() {
                        return rph;
                    }

                    /**
                     * Sets the value of the rph property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *
                     */
                    public void setRPH(int value) {
                        this.rph = value;
                    }

                    /**
                     * Gets the value of the flightNumber property.
                     *
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *
                     */
                    public int getFlightNumber() {
                        return flightNumber;
                    }

                    /**
                     * Sets the value of the flightNumber property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *
                     */
                    public void setFlightNumber(int value) {
                        this.flightNumber = value;
                    }

                    /**
                     * Gets the value of the numberInParty property.
                     *
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *
                     */
                    public int getNumberInParty() {
                        return numberInParty;
                    }

                    /**
                     * Sets the value of the numberInParty property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *
                     */
                    public void setNumberInParty(int value) {
                        this.numberInParty = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder={"flightSupplementalInfo", "bookingClassAvailability",
                            "eticket","terminalInformation", "inventorySystem"})
                    public static class Extension {
                        @XmlElement(name = "FlightSupplementalInfo")
                        private FlightSupplementalInfo flightSupplementalInfo;
                        @XmlElement(name = "BookingClassAvailability")
                        private BookingClassAvailability bookingClassAvailability;
                        @XmlElement(name = "Eticket")
                        private String eticket;
                        @XmlElement(name = "TerminalInformation")
                        private TerminalInformation terminalInformation;
                        @XmlElement(name = "InventorySystem")
                        private InventorySystem inventorySystem;

                        public Extension() {
                        }

                        public InventorySystem getInventorySystem() {
                            return inventorySystem;
                        }

                        public void setInventorySystem(InventorySystem inventorySystem) {
                            this.inventorySystem = inventorySystem;
                        }

                        public TerminalInformation getTerminalInformation() {
                            return terminalInformation;
                        }

                        public void setTerminalInformation(TerminalInformation terminalInformation) {
                            this.terminalInformation = terminalInformation;
                        }

                        public String getEticket() {
                            return eticket;
                        }

                        public void setEticket(String eticket) {
                            this.eticket = eticket;
                        }

                        public BookingClassAvailability getBookingClassAvailability() {
                            return bookingClassAvailability;
                        }

                        public void setBookingClassAvailability(BookingClassAvailability bookingClassAvailability) {
                            this.bookingClassAvailability = bookingClassAvailability;
                        }

                        public FlightSupplementalInfo getFlightSupplementalInfo() {
                            return flightSupplementalInfo;
                        }

                        public void setFlightSupplementalInfo(FlightSupplementalInfo flightSupplementalInfo) {
                            this.flightSupplementalInfo = flightSupplementalInfo;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class BookingClassAvailability {

                            @XmlAttribute(name = "BookCode")
                            protected String bookCode;
                            @XmlAttribute(name = "BookDesig")
                            protected int bookDesig;
                            @XmlAttribute(name = "Cabin")
                            protected String cabin;

                            /**
                             * Gets the value of the bookCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getBookCode() {
                                return bookCode;
                            }

                            /**
                             * Sets the value of the bookCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setBookCode(String value) {
                                this.bookCode = value;
                            }

                            /**
                             * Gets the value of the bookDesig property.
                             *
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *
                             */
                            public int getBookDesig() {
                                return bookDesig;
                            }

                            /**
                             * Sets the value of the bookDesig property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *
                             */
                            public void setBookDesig(int value) {
                                this.bookDesig = value;
                            }

                            /**
                             * Gets the value of the cabin property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getCabin() {
                                return cabin;
                            }

                            /**
                             * Sets the value of the cabin property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setCabin(String value) {
                                this.cabin = value;
                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class FlightSupplementalInfo {

                            @XmlAttribute(name = "FlyingTime")
                            protected String flyingTime;

                            /**
                             * Gets the value of the flyingTime property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getFlyingTime() {
                                return flyingTime;
                            }

                            /**
                             * Sets the value of the flyingTime property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setFlyingTime(String value) {
                                this.flyingTime = value;
                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class InventorySystem {

                            @XmlAttribute(name = "Name")
                            protected String name;

                            /**
                             * Gets the value of the name property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getName() {
                                return name;
                            }

                            /**
                             * Sets the value of the name property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setName(String value) {
                                this.name = value;
                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "terminal"
                        })
                        public static class TerminalInformation {

                            @XmlElement(namespace = "VolodNine", required = true)
                            protected int terminal;

                            /**
                             * Gets the value of the terminal property.
                             *
                             * @return
                             *     possible object is
                             *     {@link BigInteger }
                             *
                             */
                            public int getTerminal() {
                                return terminal;
                            }

                            /**
                             * Sets the value of the terminal property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link BigInteger }
                             *
                             */
                            public void setTerminal(int value) {
                                this.terminal = value;
                            }

                        }

                    }

                }

            }

        }

    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "total",
        "ptCs",
        "fareInfos",
        "extension"
    })
    public static class PricingInfo {

        @XmlElement(name = "Total", namespace = "VolodNine", required = true)
        protected Itinerary.PricingInfo.Total total;
        @XmlElement(name = "PTCs", namespace = "VolodNine", required = true)
        protected Itinerary.PricingInfo.PTCs ptCs;
        @XmlElement(name = "FareInfos", namespace = "VolodNine", required = true)
        protected Itinerary.PricingInfo.FareInfos fareInfos;
        @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
        protected Itinerary.PricingInfo.Extension extension;
        @XmlAttribute(name = "Source")
        protected String source;

        /**
         * Gets the value of the total property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.PricingInfo.Total }
         *
         */
        public Itinerary.PricingInfo.Total getTotal() {
            return total;
        }

        /**
         * Sets the value of the total property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.PricingInfo.Total }
         *
         */
        public void setTotal(Itinerary.PricingInfo.Total value) {
            this.total = value;
        }

        /**
         * Gets the value of the ptCs property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.PricingInfo.PTCs }
         *
         */
        public Itinerary.PricingInfo.PTCs getPTCs() {
            return ptCs;
        }

        /**
         * Sets the value of the ptCs property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.PricingInfo.PTCs }
         *
         */
        public void setPTCs(Itinerary.PricingInfo.PTCs value) {
            this.ptCs = value;
        }

        /**
         * Gets the value of the fareInfos property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.PricingInfo.FareInfos }
         *
         */
        public Itinerary.PricingInfo.FareInfos getFareInfos() {
            return fareInfos;
        }

        /**
         * Sets the value of the fareInfos property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.PricingInfo.FareInfos }
         *
         */
        public void setFareInfos(Itinerary.PricingInfo.FareInfos value) {
            this.fareInfos = value;
        }

        /**
         * Gets the value of the extensionFlight property.
         *
         * @return
         *     possible object is
         *     {@link Itinerary.PricingInfo.Extension }
         *
         */
        public Itinerary.PricingInfo.Extension getExtension() {
            return extension;
        }

        /**
         * Sets the value of the extensionFlight property.
         *
         * @param value
         *     allowed object is
         *     {@link Itinerary.PricingInfo.Extension }
         *
         */
        public void setExtension(Itinerary.PricingInfo.Extension value) {
            this.extension = value;
        }

        /**
         * Gets the value of the source property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSource() {
            return source;
        }

        /**
         * Sets the value of the source property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSource(String value) {
            this.source = value;
        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "pricingSystem",
            "provider"
        })
        public static class Extension {

            @XmlElement(name = "PricingSystem", namespace = "VolodNine", required = true)
            protected String pricingSystem;
            @XmlElement(name = "Provider", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.Extension.Provider provider;

            /**
             * Gets the value of the pricingSystem property.
             *
             * @return
             *     possible object is
             *     {@link String }
             *
             */
            public String getPricingSystem() {
                return pricingSystem;
            }

            /**
             * Sets the value of the pricingSystem property.
             *
             * @param value
             *     allowed object is
             *     {@link String }
             *
             */
            public void setPricingSystem(String value) {
                this.pricingSystem = value;
            }

            /**
             * Gets the value of the provider property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.Extension.Provider }
             *
             */
            public Itinerary.PricingInfo.Extension.Provider getProvider() {
                return provider;
            }

            /**
             * Sets the value of the provider property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.Extension.Provider }
             *
             */
            public void setProvider(Itinerary.PricingInfo.Extension.Provider value) {
                this.provider = value;
            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Provider {

                @XmlAttribute(name = "ProviderCode")
                protected String providerCode;

                /**
                 * Gets the value of the providerCode property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getProviderCode() {
                    return providerCode;
                }

                /**
                 * Sets the value of the providerCode property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setProviderCode(String value) {
                    this.providerCode = value;
                }

            }

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "fareInfo"
        })
        public static class FareInfos {

            @XmlElement(name = "FareInfo", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.FareInfos.FareInfo fareInfo;

            /**
             * Gets the value of the fareInfo property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.FareInfos.FareInfo }
             *
             */
            public Itinerary.PricingInfo.FareInfos.FareInfo getFareInfo() {
                return fareInfo;
            }

            /**
             * Sets the value of the fareInfo property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.FareInfos.FareInfo }
             *
             */
            public void setFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo value) {
                this.fareInfo = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "departureDate",
                "fareReference",
                "extension"
            })
            public static class FareInfo {

                @XmlElement(name = "DepartureDate", namespace = "VolodNine", required = true)
                @XmlSchemaType(name = "dateTime")
                protected Date departureDate;
                @XmlElement(name = "FareReference", namespace = "VolodNine", required = true)
                protected String fareReference;
                @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.FareInfos.FareInfo.Extension extension;
                @XmlAttribute(name = "Source")
                protected String source;

                /**
                 * Gets the value of the departureDate property.
                 *
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public Date getDepartureDate() {
                    return departureDate;
                }

                /**
                 * Sets the value of the departureDate property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *
                 */
                public void setDepartureDate(Date value) {
                    this.departureDate = value;
                }

                /**
                 * Gets the value of the fareReference property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getFareReference() {
                    return fareReference;
                }

                /**
                 * Sets the value of the fareReference property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setFareReference(String value) {
                    this.fareReference = value;
                }

                /**
                 * Gets the value of the extensionFlight property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension }
                 *
                 */
                public Itinerary.PricingInfo.FareInfos.FareInfo.Extension getExtension() {
                    return extension;
                }

                /**
                 * Sets the value of the extensionFlight property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension }
                 *
                 */
                public void setExtension(Itinerary.PricingInfo.FareInfos.FareInfo.Extension value) {
                    this.extension = value;
                }

                /**
                 * Gets the value of the source property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSource() {
                    return source;
                }

                /**
                 * Sets the value of the source property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSource(String value) {
                    this.source = value;
                }


                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(propOrder={"base", "taxInfos", "feeInfos","total", "segment", "passenger","carrier"})
                public static class Extension {

                    @XmlElement(name = "Base")
                    private Base base;
                    @XmlElement(name = "TaxInfos")
                    private TaxInfos taxInfos;
                    @XmlElement(name = "FeeInfos")
                    private FeeInfos feeInfos;
                    @XmlElement(name = "Total")
                    private Total total;
                    @XmlElement(name = "Segment")
                    private Segment segment;
                    @XmlElement(name = "Passenger")
                    private Passenger passenger;
                    @XmlElement(name = "Carrier")
                    private String carrier;

                    public Extension() {
                    }


                    public Base getBase() {
                        return base;
                    }

                    public void setBase(Base base) {
                        this.base = base;
                    }

                    public TaxInfos getTaxInfos() {
                        return taxInfos;
                    }

                    public void setTaxes(TaxInfos taxInfos) {
                        this.taxInfos = taxInfos;
                    }

                    public FeeInfos getFeeInfos() {
                        return feeInfos;
                    }

                    public void setFeeInfos(FeeInfos feeInfos) {
                        this.feeInfos = feeInfos;
                    }

                    public Total getTotal() {
                        return total;
                    }

                    public void setTotal(Total total) {
                        this.total = total;
                    }

                    public Segment getSegment() {
                        return segment;
                    }

                    public void setSegment(Segment segment) {
                        this.segment = segment;
                    }

                    public Passenger getPassenger() {
                        return passenger;
                    }

                    public void setPassenger(Passenger passenger) {
                        this.passenger = passenger;
                    }

                    public String getCarrier() {
                        return carrier;
                    }

                    public void setCarrier(String carrier) {
                        this.carrier = carrier;
                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Base {

                        @XmlAttribute(name = "Amount")
                        protected Float amount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;

                        /**
                         * Gets the value of the amount property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Float }
                         *
                         */
                        public Float getAmount() {
                            return amount;
                        }

                        /**
                         * Sets the value of the amount property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Float }
                         *
                         */
                        public void setAmount(Float value) {
                            this.amount = value;
                        }

                        /**
                         * Gets the value of the currencyCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Sets the value of the currencyCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "feeInfo"
                    })
                    public static class FeeInfos {

                        @XmlElement(name = "FeeInfo", namespace = "VolodNine")
                        protected List<FeeInfo> feeInfo;


                        public List<FeeInfo> getFeeInfo() {
                            if (feeInfo == null) {
                                feeInfo = new ArrayList<FeeInfo>();
                            }
                            return this.feeInfo;
                        }

                        public void setFeeInfo(List<FeeInfo> feeInfo) {
                            this.feeInfo = feeInfo;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class FeeInfo {

                            @XmlAttribute(name = "TaxCode")
                            protected String taxCode;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the taxCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getTaxCode() {
                                return taxCode;
                            }

                            /**
                             * Sets the value of the taxCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setTaxCode(String value) {
                                this.taxCode = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "remoteSystemPTCs"
                    })
                    public static class Passenger {

                        @XmlElement(name = "RemoteSystemPTCs", namespace = "VolodNine", required = true)
                        protected Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCs;
                        @XmlAttribute(name = "PassengerCode")
                        protected int passengerCode;
                        @XmlAttribute(name = "PassengerReference")
                        protected int passengerReference;
                        @XmlAttribute(name = "PassengerAge")
                        protected int passengerAge;

                        /**
                         * Gets the value of the remoteSystemPTCs property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs }
                         *
                         */
                        public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs getRemoteSystemPTCs() {
                            return remoteSystemPTCs;
                        }

                        /**
                         * Sets the value of the remoteSystemPTCs property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs }
                         *
                         */
                        public void setRemoteSystemPTCs(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs value) {
                            this.remoteSystemPTCs = value;
                        }

                        /**
                         * Gets the value of the passengerCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public int getPassengerCode() {
                            return passengerCode;
                        }

                        /**
                         * Sets the value of the passengerCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setPassengerCode(int value) {
                            this.passengerCode = value;
                        }

                        /**
                         * Gets the value of the passengerReference property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *
                         */
                        public int getPassengerReference() {
                            return passengerReference;
                        }

                        /**
                         * Sets the value of the passengerReference property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *
                         */
                        public void setPassengerReference(int value) {
                            this.passengerReference = value;
                        }

                        /**
                         * Gets the value of the passengerAge property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *
                         */
                        public int getPassengerAge() {
                            return passengerAge;
                        }

                        /**
                         * Sets the value of the passengerAge property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *
                         */
                        public void setPassengerAge(int value) {
                            this.passengerAge = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class RemoteSystemPTCs {

                            @XmlAttribute(name = "PassengerCode")
                            protected String passengerCode;

                            /**
                             * Gets the value of the passengerCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getPassengerCode() {
                                return passengerCode;
                            }

                            /**
                             * Sets the value of the passengerCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setPassengerCode(String value) {
                                this.passengerCode = value;
                            }

                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Segment {

                        @XmlAttribute(name = "FlightReference")
                        protected String flightReference;

                        /**
                         * Gets the value of the flightReference property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getFlightReference() {
                            return flightReference;
                        }

                        /**
                         * Sets the value of the flightReference property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setFlightReference(String value) {
                            this.flightReference = value;
                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "taxInfo"
                    })
                    public static class TaxInfos {

                        @XmlElement(name = "TaxInfo", namespace = "VolodNine")
                        protected List<TaxInfo> taxInfo;

                        public List<TaxInfo> getTaxInfo() {
                            if (taxInfo == null) {
                                taxInfo = new ArrayList<TaxInfo>();
                            }
                            return this.taxInfo;
                        }

                        public void setTaxInfo(List<TaxInfo> taxInfo) {
                            this.taxInfo = taxInfo;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class TaxInfo {

                            @XmlAttribute(name = "TaxCode")
                            protected String taxCode;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the taxCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getTaxCode() {
                                return taxCode;
                            }

                            /**
                             * Sets the value of the taxCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setTaxCode(String value) {
                                this.taxCode = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Total {

                        @XmlAttribute(name = "Amount")
                        protected Float amount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;

                        /**
                         * Gets the value of the amount property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Float }
                         *
                         */
                        public Float getAmount() {
                            return amount;
                        }

                        /**
                         * Sets the value of the amount property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Float }
                         *
                         */
                        public void setAmount(Float value) {
                            this.amount = value;
                        }

                        /**
                         * Gets the value of the currencyCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Sets the value of the currencyCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }

                }

            }

        }


        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "ptc"
        })
        public static class PTCs {

            @XmlElement(name = "PTC", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.PTCs.PTC ptc;

            /**
             * Gets the value of the ptc property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.PTCs.PTC }
             *
             */
            public Itinerary.PricingInfo.PTCs.PTC getPTC() {
                return ptc;
            }

            /**
             * Sets the value of the ptc property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.PTCs.PTC }
             *
             */
            public void setPTC(Itinerary.PricingInfo.PTCs.PTC value) {
                this.ptc = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "passengerQuantity",
                "basisCodes",
                "passengerFare",
                "extension"
            })
            public static class PTC {

                @XmlElement(name = "PassengerQuantity", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity passengerQuantity;
                @XmlElement(name = "BasisCodes", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.PTCs.PTC.BasisCodes basisCodes;
                @XmlElement(name = "PassengerFare", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare passengerFare;
                @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.PTCs.PTC.Extension extension;
                @XmlAttribute(name = "Source")
                protected String source;

                /**
                 * Gets the value of the passengerQuantity property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity }
                 *
                 */
                public Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity getPassengerQuantity() {
                    return passengerQuantity;
                }

                /**
                 * Sets the value of the passengerQuantity property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity }
                 *
                 */
                public void setPassengerQuantity(Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity value) {
                    this.passengerQuantity = value;
                }

                /**
                 * Gets the value of the basisCodes property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.BasisCodes }
                 *
                 */
                public Itinerary.PricingInfo.PTCs.PTC.BasisCodes getBasisCodes() {
                    return basisCodes;
                }

                /**
                 * Sets the value of the basisCodes property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.BasisCodes }
                 *
                 */
                public void setBasisCodes(Itinerary.PricingInfo.PTCs.PTC.BasisCodes value) {
                    this.basisCodes = value;
                }

                /**
                 * Gets the value of the passengerFare property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare }
                 *
                 */
                public Itinerary.PricingInfo.PTCs.PTC.PassengerFare getPassengerFare() {
                    return passengerFare;
                }

                /**
                 * Sets the value of the passengerFare property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare }
                 *
                 */
                public void setPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare value) {
                    this.passengerFare = value;
                }

                /**
                 * Gets the value of the extensionFlight property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension }
                 *
                 */
                public Itinerary.PricingInfo.PTCs.PTC.Extension getExtension() {
                    return extension;
                }

                /**
                 * Sets the value of the extensionFlight property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension }
                 *
                 */
                public void setExtension(Itinerary.PricingInfo.PTCs.PTC.Extension value) {
                    this.extension = value;
                }

                /**
                 * Gets the value of the source property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getSource() {
                    return source;
                }

                /**
                 * Sets the value of the source property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setSource(String value) {
                    this.source = value;
                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "basisCode"
                })
                public static class BasisCodes {

                    @XmlElement(name = "BasisCode", namespace = "VolodNine", required = true)
                    protected String basisCode;

                    /**
                     * Gets the value of the basisCode property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getBasisCode() {
                        return basisCode;
                    }

                    /**
                     * Sets the value of the basisCode property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setBasisCode(String value) {
                        this.basisCode = value;
                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "passenger"
                })
                public static class Extension {

                    @XmlElement(name = "Passenger", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger passenger;

                    /**
                     * Gets the value of the passenger property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger getPassenger() {
                        return passenger;
                    }

                    /**
                     * Sets the value of the passenger property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger }
                     *
                     */
                    public void setPassenger(Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger value) {
                        this.passenger = value;
                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "remoteSystemPTCs"
                    })
                    public static class Passenger {

                        @XmlElement(name = "RemoteSystemPTCs", namespace = "VolodNine", required = true)
                        protected Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCs;
                        @XmlAttribute(name = "PassengerCode")
                        protected String passengerCode;
                        @XmlAttribute(name = "PassengerReference")
                        protected int passengerReference;
                        @XmlAttribute(name = "PassengerAge")
                        protected int passengerAge;

                        /**
                         * Gets the value of the remoteSystemPTCs property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs }
                         *
                         */
                        public Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs getRemoteSystemPTCs() {
                            return remoteSystemPTCs;
                        }

                        /**
                         * Sets the value of the remoteSystemPTCs property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs }
                         *
                         */
                        public void setRemoteSystemPTCs(Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs value) {
                            this.remoteSystemPTCs = value;
                        }

                        /**
                         * Gets the value of the passengerCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getPassengerCode() {
                            return passengerCode;
                        }

                        /**
                         * Sets the value of the passengerCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setPassengerCode(String value) {
                            this.passengerCode = value;
                        }

                        /**
                         * Gets the value of the passengerReference property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *
                         */
                        public int getPassengerReference() {
                            return passengerReference;
                        }

                        /**
                         * Sets the value of the passengerReference property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *
                         */
                        public void setPassengerReference(int value) {
                            this.passengerReference = value;
                        }

                        /**
                         * Gets the value of the passengerAge property.
                         *
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *
                         */
                        public int getPassengerAge() {
                            return passengerAge;
                        }

                        /**
                         * Sets the value of the passengerAge property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *
                         */
                        public void setPassengerAge(int value) {
                            this.passengerAge = value;
                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class RemoteSystemPTCs {

                            @XmlAttribute(name = "PassengerCode")
                            protected String passengerCode;

                            /**
                             * Gets the value of the passengerCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getPassengerCode() {
                                return passengerCode;
                            }

                            /**
                             * Sets the value of the passengerCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setPassengerCode(String value) {
                                this.passengerCode = value;
                            }

                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "base",
                    "taxes",
                    "fees",
                    "total",
                    "extension"
                })
                public static class PassengerFare {

                    @XmlElement(name = "Base", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base base;
                    @XmlElement(name = "Taxes", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes taxes;
                    @XmlElement(name = "Fees", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees fees;
                    @XmlElement(name = "Total", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total total;
                    @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
                    protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension extension;

                    /**
                     * Gets the value of the base property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base getBase() {
                        return base;
                    }

                    /**
                     * Sets the value of the base property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base }
                     *
                     */
                    public void setBase(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base value) {
                        this.base = value;
                    }

                    /**
                     * Gets the value of the taxes property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes getTaxes() {
                        return taxes;
                    }

                    /**
                     * Sets the value of the taxes property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes }
                     *
                     */
                    public void setTaxes(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes value) {
                        this.taxes = value;
                    }

                    /**
                     * Gets the value of the fees property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees getFees() {
                        return fees;
                    }

                    /**
                     * Sets the value of the fees property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees }
                     *
                     */
                    public void setFees(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees value) {
                        this.fees = value;
                    }

                    /**
                     * Gets the value of the total property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total getTotal() {
                        return total;
                    }

                    /**
                     * Sets the value of the total property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total }
                     *
                     */
                    public void setTotal(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total value) {
                        this.total = value;
                    }

                    /**
                     * Gets the value of the extensionFlight property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension }
                     *
                     */
                    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension getExtension() {
                        return extension;
                    }

                    /**
                     * Sets the value of the extensionFlight property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension }
                     *
                     */
                    public void setExtension(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension value) {
                        this.extension = value;
                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Base {

                        @XmlAttribute(name = "Amount")
                        protected Float amount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;

                        /**
                         * Gets the value of the amount property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Float }
                         *
                         */
                        public Float getAmount() {
                            return amount;
                        }

                        /**
                         * Sets the value of the amount property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Float }
                         *
                         */
                        public void setAmount(Float value) {
                            this.amount = value;
                        }

                        /**
                         * Gets the value of the currencyCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Sets the value of the currencyCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "taxInfos",
                        "feeInfos"
                    })
                    public static class Extension {

                        @XmlElement(name = "TaxInfos", namespace = "VolodNine", required = true)
                        protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos taxInfos;
                        @XmlElement(name = "FeeInfos", namespace = "VolodNine", required = true)
                        protected Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos feeInfos;

                        /**
                         * Gets the value of the taxInfos property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos }
                         *
                         */
                        public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos getTaxInfos() {
                            return taxInfos;
                        }

                        /**
                         * Sets the value of the taxInfos property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos }
                         *
                         */
                        public void setTaxInfos(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos value) {
                            this.taxInfos = value;
                        }

                        /**
                         * Gets the value of the feeInfos property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos }
                         *
                         */
                        public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos getFeeInfos() {
                            return feeInfos;
                        }

                        /**
                         * Sets the value of the feeInfos property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos }
                         *
                         */
                        public void setFeeInfos(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos value) {
                            this.feeInfos = value;
                        }



                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "feeInfo"
                        })
                        public static class FeeInfos {

                            @XmlElement(name = "FeeInfo", namespace = "VolodNine")
                            protected List<FeeInfo> feeInfo;


                            public List<FeeInfo> getFeeInfo() {
                                if (feeInfo == null) {
                                    feeInfo = new ArrayList<FeeInfo>();
                                }
                                return this.feeInfo;
                            }

                            public void setFeeInfo(List<FeeInfo> feeInfo) {
                                this.feeInfo = feeInfo;
                            }

                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class FeeInfo {

                                @XmlAttribute(name = "TaxCode")
                                protected String taxCode;
                                @XmlAttribute(name = "CurrencyCode")
                                protected String currencyCode;

                                /**
                                 * Gets the value of the taxCode property.
                                 *
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *
                                 */
                                public String getTaxCode() {
                                    return taxCode;
                                }

                                /**
                                 * Sets the value of the taxCode property.
                                 *
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *
                                 */
                                public void setTaxCode(String value) {
                                    this.taxCode = value;
                                }

                                /**
                                 * Gets the value of the currencyCode property.
                                 *
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *
                                 */
                                public String getCurrencyCode() {
                                    return currencyCode;
                                }

                                /**
                                 * Sets the value of the currencyCode property.
                                 *
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *
                                 */
                                public void setCurrencyCode(String value) {
                                    this.currencyCode = value;
                                }

                            }

                        }


                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "taxInfo"
                        })
                        public static class TaxInfos {

                            @XmlElement(name = "TaxInfo", namespace = "VolodNine")
                            protected List<TaxInfo> taxInfo;


                            public List<TaxInfo> getTaxInfo() {
                                if (taxInfo == null) {
                                    taxInfo = new ArrayList<TaxInfo>();
                                }
                                return this.taxInfo;
                            }

                            public void setTaxInfo(List<TaxInfo> taxInfo) {
                                this.taxInfo = taxInfo;
                            }

                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class TaxInfo {

                                @XmlAttribute(name = "TaxCode")
                                protected String taxCode;
                                @XmlAttribute(name = "CurrencyCode")
                                protected String currencyCode;

                                /**
                                 * Gets the value of the taxCode property.
                                 *
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *
                                 */
                                public String getTaxCode() {
                                    return taxCode;
                                }

                                /**
                                 * Sets the value of the taxCode property.
                                 *
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *
                                 */
                                public void setTaxCode(String value) {
                                    this.taxCode = value;
                                }

                                /**
                                 * Gets the value of the currencyCode property.
                                 *
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *
                                 */
                                public String getCurrencyCode() {
                                    return currencyCode;
                                }

                                /**
                                 * Sets the value of the currencyCode property.
                                 *
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *
                                 */
                                public void setCurrencyCode(String value) {
                                    this.currencyCode = value;
                                }

                            }

                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "fee"
                    })
                    public static class Fees {

                        @XmlElement(name = "Fee", namespace = "VolodNine")
                        protected List<Fee> fee;


                        public List<Fee> getFee() {
                            if (fee == null) {
                                fee = new ArrayList<Fee>();
                            }
                            return this.fee;
                        }

                        public void setFee(List<Fee> fee) {
                            this.fee = fee;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Fee {

                            @XmlAttribute(name = "TaxCode")
                            protected String taxCode;
                            @XmlAttribute(name = "Amount")
                            protected Float amount;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the taxCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getTaxCode() {
                                return taxCode;
                            }

                            /**
                             * Sets the value of the taxCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setTaxCode(String value) {
                                this.taxCode = value;
                            }

                            /**
                             * Gets the value of the amount property.
                             *
                             * @return
                             *     possible object is
                             *     {@link Float }
                             *
                             */
                            public Float getAmount() {
                                return amount;
                            }

                            /**
                             * Sets the value of the amount property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link Float }
                             *
                             */
                            public void setAmount(Float value) {
                                this.amount = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "tax"
                    })
                    public static class Taxes {

                        @XmlElement(name = "Tax", namespace = "VolodNine")
                        protected List<Tax> tax;


                        public List<Tax> getTax() {
                            if (tax == null) {
                                tax = new ArrayList<Tax>();
                            }
                            return this.tax;
                        }

                        public void setTax(List<Tax> tax) {
                            this.tax = tax;
                        }

                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class Tax {

                            @XmlAttribute(name = "Amount")
                            protected Float amount;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;
                            @XmlAttribute(name = "TaxCode")
                            protected String taxCode;

                            /**
                             * Gets the value of the amount property.
                             *
                             * @return
                             *     possible object is
                             *     {@link Float }
                             *
                             */
                            public Float getAmount() {
                                return amount;
                            }

                            /**
                             * Sets the value of the amount property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link Float }
                             *
                             */
                            public void setAmount(Float value) {
                                this.amount = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                            /**
                             * Gets the value of the taxCode property.
                             *
                             * @return
                             *     possible object is
                             *     {@link String }
                             *
                             */
                            public String getTaxCode() {
                                return taxCode;
                            }

                            /**
                             * Sets the value of the taxCode property.
                             *
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *
                             */
                            public void setTaxCode(String value) {
                                this.taxCode = value;
                            }

                        }

                    }



                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Total {

                        @XmlAttribute(name = "Amount")
                        protected Float amount;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;

                        /**
                         * Gets the value of the amount property.
                         *
                         * @return
                         *     possible object is
                         *     {@link Float }
                         *
                         */
                        public Float getAmount() {
                            return amount;
                        }

                        /**
                         * Sets the value of the amount property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link Float }
                         *
                         */
                        public void setAmount(Float value) {
                            this.amount = value;
                        }

                        /**
                         * Gets the value of the currencyCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Sets the value of the currencyCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }

                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class PassengerQuantity {

                    @XmlAttribute(name = "Code")
                    protected String code;
                    @XmlAttribute(name = "Quantity")
                    protected int quantity;

                    /**
                     * Gets the value of the code property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCode() {
                        return code;
                    }

                    /**
                     * Sets the value of the code property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCode(String value) {
                        this.code = value;
                    }

                    /**
                     * Gets the value of the quantity property.
                     *
                     * @return
                     *     possible object is
                     *     {@link BigInteger }
                     *
                     */
                    public int getQuantity() {
                        return quantity;
                    }

                    /**
                     * Sets the value of the quantity property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link BigInteger }
                     *
                     */
                    public void setQuantity(int value) {
                        this.quantity = value;
                    }

                }

            }

        }



        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "base",
            "taxes",
            "fees",
            "total",
            "extension"
        })
        public static class Total {

            @XmlElement(name = "Base", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.Total.Base base;
            @XmlElement(name = "Taxes", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.Total.Taxes taxes;
            @XmlElement(name = "Fees", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.Total.Fees fees;
            @XmlElement(name = "Total", namespace = "VolodNine", required = true)
            protected Total2 total;
            @XmlElement(name = "Extension", namespace = "VolodNine", required = true)
            protected Itinerary.PricingInfo.Total.Extension extension;

            /**
             * Gets the value of the base property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.Total.Base }
             *
             */
            public Itinerary.PricingInfo.Total.Base getBase() {
                return base;
            }

            /**
             * Sets the value of the base property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.Total.Base }
             *
             */
            public void setBase(Itinerary.PricingInfo.Total.Base value) {
                this.base = value;
            }

            /**
             * Gets the value of the taxes property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.Total.Taxes }
             *
             */
            public Itinerary.PricingInfo.Total.Taxes getTaxes() {
                return taxes;
            }

            /**
             * Sets the value of the taxes property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.Total.Taxes }
             *
             */
            public void setTaxes(Itinerary.PricingInfo.Total.Taxes value) {
                this.taxes = value;
            }

            /**
             * Gets the value of the fees property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.Total.Fees }
             *
             */
            public Itinerary.PricingInfo.Total.Fees getFees() {
                return fees;
            }

            /**
             * Sets the value of the fees property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.Total.Fees }
             *
             */
            public void setFees(Itinerary.PricingInfo.Total.Fees value) {
                this.fees = value;
            }

            /**
             * Gets the value of the total property.
             *
             * @return
             *     possible object is
             *     {@link Total2 }
             *
             */
            public Total2 getTotal() {
                return total;
            }

            /**
             * Sets the value of the total property.
             *
             * @param value
             *     allowed object is
             *     {@link Total2 }
             *
             */
            public void setTotal(Total2 value) {
                this.total = value;
            }

            /**
             * Gets the value of the extensionFlight property.
             *
             * @return
             *     possible object is
             *     {@link Itinerary.PricingInfo.Total.Extension }
             *
             */
            public Itinerary.PricingInfo.Total.Extension getExtension() {
                return extension;
            }

            /**
             * Sets the value of the extensionFlight property.
             *
             * @param value
             *     allowed object is
             *     {@link Itinerary.PricingInfo.Total.Extension }
             *
             */
            public void setExtension(Itinerary.PricingInfo.Total.Extension value) {
                this.extension = value;
            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Base {

                @XmlAttribute(name = "Amount")
                protected Float amount;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;

                /**
                 * Gets the value of the amount property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Float }
                 *
                 */
                public Float getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Float }
                 *
                 */
                public void setAmount(Float value) {
                    this.amount = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 *
                 * @return
                 *     possible object is
                 *     {@link String }
                 *
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "taxInfos",
                "feeInfos"
            })
            public static class Extension {

                @XmlElement(name = "TaxInfos", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.Total.Extension.TaxInfos taxInfos;
                @XmlElement(name = "FeeInfos", namespace = "VolodNine", required = true)
                protected Itinerary.PricingInfo.Total.Extension.FeeInfos feeInfos;

                /**
                 * Gets the value of the taxInfos property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.Total.Extension.TaxInfos }
                 *
                 */
                public Itinerary.PricingInfo.Total.Extension.TaxInfos getTaxInfos() {
                    return taxInfos;
                }

                /**
                 * Sets the value of the taxInfos property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.Total.Extension.TaxInfos }
                 *
                 */
                public void setTaxInfos(Itinerary.PricingInfo.Total.Extension.TaxInfos value) {
                    this.taxInfos = value;
                }

                /**
                 * Gets the value of the feeInfos property.
                 *
                 * @return
                 *     possible object is
                 *     {@link Itinerary.PricingInfo.Total.Extension.FeeInfos }
                 *
                 */
                public Itinerary.PricingInfo.Total.Extension.FeeInfos getFeeInfos() {
                    return feeInfos;
                }

                /**
                 * Sets the value of the feeInfos property.
                 *
                 * @param value
                 *     allowed object is
                 *     {@link Itinerary.PricingInfo.Total.Extension.FeeInfos }
                 *
                 */
                public void setFeeInfos(Itinerary.PricingInfo.Total.Extension.FeeInfos value) {
                    this.feeInfos = value;
                }



                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "feeInfo"
                })
                public static class FeeInfos {

                    @XmlElement(name = "FeeInfo", namespace = "VolodNine")
                    protected List<FeeInfo> feeInfo;

                    public List<FeeInfo> getFeeInfo() {
                        if (feeInfo == null) {
                            feeInfo = new ArrayList<FeeInfo>();
                        }
                        return this.feeInfo;
                    }

                    public void setFeeInfo(List<FeeInfo> feeInfo) {
                        this.feeInfo = feeInfo;
                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class FeeInfo {

                        @XmlAttribute(name = "TaxCode")
                        protected String taxCode;
                        @XmlAttribute(name = "CurrencyCode")
                        protected String currencyCode;

                        /**
                         * Gets the value of the taxCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getTaxCode() {
                            return taxCode;
                        }

                        /**
                         * Sets the value of the taxCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setTaxCode(String value) {
                            this.taxCode = value;
                        }

                        /**
                         * Gets the value of the currencyCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getCurrencyCode() {
                            return currencyCode;
                        }

                        /**
                         * Sets the value of the currencyCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setCurrencyCode(String value) {
                            this.currencyCode = value;
                        }

                    }

                }


                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "taxInfo"
                })
                public static class TaxInfos {

                    @XmlElement(name = "TaxInfo", namespace = "VolodNine")
                    protected List<TaxInfo> taxInfo;

                    public List<TaxInfo> getTaxInfo() {
                        if (taxInfo == null) {
                            taxInfo = new ArrayList<TaxInfo>();
                        }
                        return this.taxInfo;
                    }

                    public void setTaxInfo(List<TaxInfo> taxInfo) {
                        this.taxInfo = taxInfo;
                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class TaxInfo {

                        @XmlAttribute(name = "TaxCode")
                        protected String taxCode;
                        @XmlAttribute(name = "BaseCurrency")
                        protected String baseCurrency;

                        /**
                         * Gets the value of the taxCode property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getTaxCode() {
                            return taxCode;
                        }

                        /**
                         * Sets the value of the taxCode property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setTaxCode(String value) {
                            this.taxCode = value;
                        }

                        /**
                         * Gets the value of the baseCurrency property.
                         *
                         * @return
                         *     possible object is
                         *     {@link String }
                         *
                         */
                        public String getBaseCurrency() {
                            return baseCurrency;
                        }

                        /**
                         * Sets the value of the baseCurrency property.
                         *
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *
                         */
                        public void setBaseCurrency(String value) {
                            this.baseCurrency = value;
                        }

                    }

                }

            }


            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "fee"
            })
            public static class Fees {

                @XmlElement(name = "Fee", namespace = "VolodNine")
                protected List<Fee> fee;


                public List<Fee> getFee() {
                    if (fee == null) {
                        fee = new ArrayList<Fee>();
                    }
                    return this.fee;
                }

                public void setFee(List<Fee> fee) {
                    this.fee = fee;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Fee {

                    @XmlAttribute(name = "TaxCode")
                    protected String taxCode;
                    @XmlAttribute(name = "Amount")
                    protected Float amount;
                    @XmlAttribute(name = "CurrencyCode")
                    protected String currencyCode;

                    /**
                     * Gets the value of the taxCode property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getTaxCode() {
                        return taxCode;
                    }

                    /**
                     * Sets the value of the taxCode property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setTaxCode(String value) {
                        this.taxCode = value;
                    }

                    /**
                     * Gets the value of the amount property.
                     *
                     * @return
                     *     possible object is
                     *     {@link Float }
                     *
                     */
                    public Float getAmount() {
                        return amount;
                    }

                    /**
                     * Sets the value of the amount property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link Float }
                     *
                     */
                    public void setAmount(Float value) {
                        this.amount = value;
                    }

                    /**
                     * Gets the value of the currencyCode property.
                     *
                     * @return
                     *     possible object is
                     *     {@link String }
                     *
                     */
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    /**
                     * Sets the value of the currencyCode property.
                     *
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *
                     */
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "tax"
            })
            public static class Taxes {

                @XmlElement(name = "Tax", namespace = "VolodNine")
                protected List<Tax> tax;


                public List<Tax> getTax() {
                    if (tax == null) {
                        tax = new ArrayList<Tax>();
                    }
                    return this.tax;
                }

                public void setTax(List<Tax> tax) {
                    this.tax = tax;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Tax {

                    @XmlAttribute(name = "Amount")
                    protected Float amount;
                    @XmlAttribute(name = "CurrencyCode")
                    protected String currencyCode;
                    @XmlAttribute(name = "TaxCode")
                    protected String taxCode;

                    /**
                     * Gets the value of the amount property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Float }
                     *     
                     */
                    public Float getAmount() {
                        return amount;
                    }

                    /**
                     * Sets the value of the amount property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Float }
                     *     
                     */
                    public void setAmount(Float value) {
                        this.amount = value;
                    }

                    /**
                     * Gets the value of the currencyCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getCurrencyCode() {
                        return currencyCode;
                    }

                    /**
                     * Sets the value of the currencyCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setCurrencyCode(String value) {
                        this.currencyCode = value;
                    }

                    /**
                     * Gets the value of the taxCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getTaxCode() {
                        return taxCode;
                    }

                    /**
                     * Sets the value of the taxCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setTaxCode(String value) {
                        this.taxCode = value;
                    }

                }

            }



            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "")
            public static class Total2 {

                @XmlAttribute(name = "Amount")
                protected Float amount;
                @XmlAttribute(name = "CurrencyCode")
                protected String currencyCode;

                /**
                 * Gets the value of the amount property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Float }
                 *     
                 */
                public Float getAmount() {
                    return amount;
                }

                /**
                 * Sets the value of the amount property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Float }
                 *     
                 */
                public void setAmount(Float value) {
                    this.amount = value;
                }

                /**
                 * Gets the value of the currencyCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCurrencyCode() {
                    return currencyCode;
                }

                /**
                 * Sets the value of the currencyCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCurrencyCode(String value) {
                    this.currencyCode = value;
                }

            }

        }

    }



    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class TicketingInfo {

        @XmlAttribute(name = "TicketType")
        protected String ticketType;

        /**
         * Gets the value of the ticketType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTicketType() {
            return ticketType;
        }

        /**
         * Sets the value of the ticketType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTicketType(String value) {
            this.ticketType = value;
        }

    }

}
