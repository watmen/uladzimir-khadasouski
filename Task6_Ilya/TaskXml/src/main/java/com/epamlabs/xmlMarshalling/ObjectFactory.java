
package com.epamlabs.xmlMarshalling;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epamlabs.xmlMarshalling package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epamlabs.xmlMarshalling
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Itinerary }
     * 
     */
    public Itinerary createItinerary() {
        return new Itinerary();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo }
     * 
     */
    public Itinerary.PricingInfo createItineraryPricingInfo() {
        return new Itinerary.PricingInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Extension }
     * 
     */
    public Itinerary.PricingInfo.Extension createItineraryPricingInfoExtension() {
        return new Itinerary.PricingInfo.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos }
     * 
     */
    public Itinerary.PricingInfo.FareInfos createItineraryPricingInfoFareInfos() {
        return new Itinerary.PricingInfo.FareInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo createItineraryPricingInfoFareInfosFareInfo() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension createItineraryPricingInfoFareInfosFareInfoExtension() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger createItineraryPricingInfoFareInfosFareInfoExtensionPassenger() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos createItineraryPricingInfoFareInfosFareInfoExtensionFeeInfos() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos createItineraryPricingInfoFareInfosFareInfoExtensionTaxInfos() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs }
     * 
     */
    public Itinerary.PricingInfo.PTCs createItineraryPricingInfoPTCs() {
        return new Itinerary.PricingInfo.PTCs();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC createItineraryPricingInfoPTCsPTC() {
        return new Itinerary.PricingInfo.PTCs.PTC();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.Extension }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.Extension createItineraryPricingInfoPTCsPTCExtension() {
        return new Itinerary.PricingInfo.PTCs.PTC.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger createItineraryPricingInfoPTCsPTCExtensionPassenger() {
        return new Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare createItineraryPricingInfoPTCsPTCPassengerFare() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension createItineraryPricingInfoPTCsPTCPassengerFareExtension() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos createItineraryPricingInfoPTCsPTCPassengerFareExtensionFeeInfos() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos createItineraryPricingInfoPTCsPTCPassengerFareExtensionTaxInfos() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees createItineraryPricingInfoPTCsPTCPassengerFareFees() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes createItineraryPricingInfoPTCsPTCPassengerFareTaxes() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total }
     * 
     */
    public Itinerary.PricingInfo.Total createItineraryPricingInfoTotal() {
        return new Itinerary.PricingInfo.Total();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Extension }
     * 
     */
    public Itinerary.PricingInfo.Total.Extension createItineraryPricingInfoTotalExtension() {
        return new Itinerary.PricingInfo.Total.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Extension.FeeInfos }
     * 
     */
    public Itinerary.PricingInfo.Total.Extension.FeeInfos createItineraryPricingInfoTotalExtensionFeeInfos() {
        return new Itinerary.PricingInfo.Total.Extension.FeeInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Extension.TaxInfos }
     * 
     */
    public Itinerary.PricingInfo.Total.Extension.TaxInfos createItineraryPricingInfoTotalExtensionTaxInfos() {
        return new Itinerary.PricingInfo.Total.Extension.TaxInfos();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Fees }
     * 
     */
    public Itinerary.PricingInfo.Total.Fees createItineraryPricingInfoTotalFees() {
        return new Itinerary.PricingInfo.Total.Fees();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Taxes }
     * 
     */
    public Itinerary.PricingInfo.Total.Taxes createItineraryPricingInfoTotalTaxes() {
        return new Itinerary.PricingInfo.Total.Taxes();
    }

    /**
     * Create an instance of {@link Itinerary.Air }
     * 
     */
    public Itinerary.Air createItineraryAir() {
        return new Itinerary.Air();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Extension }
     * 
     */
    public Itinerary.Air.Extension createItineraryAirExtension() {
        return new Itinerary.Air.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options }
     * 
     */
    public Itinerary.Air.Options createItineraryAirOptions() {
        return new Itinerary.Air.Options();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option }
     * 
     */
    public Itinerary.Air.Options.Option createItineraryAirOptionsOption() {
        return new Itinerary.Air.Options.Option();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Extension }
     * 
     */
    public Itinerary.Air.Options.Option.Extension createItineraryAirOptionsOptionExtension() {
        return new Itinerary.Air.Options.Option.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight }
     * 
     */
    public Itinerary.Air.Options.Option.Flight createItineraryAirOptionsOptionFlight() {
        return new Itinerary.Air.Options.Option.Flight();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight.Extension }
     * 
     */
    public Itinerary.Air.Options.Option.Flight.Extension createItineraryAirOptionsOptionFlightExtension() {
        return new Itinerary.Air.Options.Option.Flight.Extension();
    }

    /**
     * Create an instance of {@link Itinerary.TicketingInfo }
     * 
     */
    public Itinerary.TicketingInfo createItineraryTicketingInfo() {
        return new Itinerary.TicketingInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Extension.Provider }
     * 
     */
    public Itinerary.PricingInfo.Extension.Provider createItineraryPricingInfoExtensionProvider() {
        return new Itinerary.PricingInfo.Extension.Provider();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base createItineraryPricingInfoFareInfosFareInfoExtensionBase() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total createItineraryPricingInfoFareInfosFareInfoExtensionTotal() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment createItineraryPricingInfoFareInfosFareInfoExtensionSegment() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs createItineraryPricingInfoFareInfosFareInfoExtensionPassengerRemoteSystemPTCs() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo createItineraryPricingInfoFareInfosFareInfoExtensionFeeInfosFeeInfo() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo }
     * 
     */
    public Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo createItineraryPricingInfoFareInfosFareInfoExtensionTaxInfosTaxInfo() {
        return new Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity createItineraryPricingInfoPTCsPTCPassengerQuantity() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.BasisCodes }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.BasisCodes createItineraryPricingInfoPTCsPTCBasisCodes() {
        return new Itinerary.PricingInfo.PTCs.PTC.BasisCodes();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs createItineraryPricingInfoPTCsPTCExtensionPassengerRemoteSystemPTCs() {
        return new Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base createItineraryPricingInfoPTCsPTCPassengerFareBase() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total createItineraryPricingInfoPTCsPTCPassengerFareTotal() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo createItineraryPricingInfoPTCsPTCPassengerFareExtensionFeeInfosFeeInfo() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo createItineraryPricingInfoPTCsPTCPassengerFareExtensionTaxInfosTaxInfo() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee createItineraryPricingInfoPTCsPTCPassengerFareFeesFee() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax }
     * 
     */
    public Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax createItineraryPricingInfoPTCsPTCPassengerFareTaxesTax() {
        return new Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Base }
     * 
     */
    public Itinerary.PricingInfo.Total.Base createItineraryPricingInfoTotalBase() {
        return new Itinerary.PricingInfo.Total.Base();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Total2 }
     * 
     */
    public Itinerary.PricingInfo.Total.Total2 createItineraryPricingInfoTotalTotal() {
        return new Itinerary.PricingInfo.Total.Total2();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo }
     * 
     */
    public Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo createItineraryPricingInfoTotalExtensionFeeInfosFeeInfo() {
        return new Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo }
     * 
     */
    public Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo createItineraryPricingInfoTotalExtensionTaxInfosTaxInfo() {
        return new Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Fees.Fee }
     * 
     */
    public Itinerary.PricingInfo.Total.Fees.Fee createItineraryPricingInfoTotalFeesFee() {
        return new Itinerary.PricingInfo.Total.Fees.Fee();
    }

    /**
     * Create an instance of {@link Itinerary.PricingInfo.Total.Taxes.Tax }
     * 
     */
    public Itinerary.PricingInfo.Total.Taxes.Tax createItineraryPricingInfoTotalTaxesTax() {
        return new Itinerary.PricingInfo.Total.Taxes.Tax();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Extension.Attributes }
     * 
     */
    public Itinerary.Air.Extension.Attributes createItineraryAirExtensionAttributes() {
        return new Itinerary.Air.Extension.Attributes();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Extension.InventorySystem }
     * 
     */
    public Itinerary.Air.Options.Option.Extension.InventorySystem createItineraryAirOptionsOptionExtensionInventorySystem() {
        return new Itinerary.Air.Options.Option.Extension.InventorySystem();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo }
     * 
     */
    public Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo createItineraryAirOptionsOptionFlightExtensionFlightSupplementalInfo() {
        return new Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability }
     * 
     */
    public Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability createItineraryAirOptionsOptionFlightExtensionBookingClassAvailability() {
        return new Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation }
     * 
     */
    public Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation createItineraryAirOptionsOptionFlightExtensionTerminalInformation() {
        return new Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation();
    }

    /**
     * Create an instance of {@link Itinerary.Air.Options.Option.Flight.Extension.InventorySystem }
     * 
     */
    public Itinerary.Air.Options.Option.Flight.Extension.InventorySystem createItineraryAirOptionsOptionFlightExtensionInventorySystem() {
        return new Itinerary.Air.Options.Option.Flight.Extension.InventorySystem();
    }

}
