package com.epamlabs;

import com.epamlabs.xmlMarshalling.Itinerary;
import com.epamlabs.xmlMarshalling.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Date;

public class Runner {

    private static void createItinerary(Itinerary.Air air, Itinerary.PricingInfo pricingInfo, Itinerary.TicketingInfo ticketingInfo, Itinerary itinerary) {
        itinerary.setTicketingInfo(ticketingInfo);
        itinerary.setPricingInfo(pricingInfo);
        itinerary.setAir(air);
        itinerary.setRPH(1);
        itinerary.setSequenceNumber(1);
        itinerary.setItineraryID(0);
    }

    private static void createTicketingInfo(Itinerary.TicketingInfo ticketingInfo) {
        ticketingInfo.setTicketType("aDS");
    }

    private static void createPricingInfo(Itinerary.PricingInfo.PTCs ptCs, Itinerary.PricingInfo.Total totalPricingInfo, Itinerary.PricingInfo.FareInfos fareInfos, Itinerary.PricingInfo.Extension extensionPricingInfo, Itinerary.PricingInfo pricingInfo) {
        pricingInfo.setSource("VVVV");
        pricingInfo.setPTCs(ptCs);
        pricingInfo.setTotal(totalPricingInfo);
        pricingInfo.setFareInfos(fareInfos);
        pricingInfo.setExtension(extensionPricingInfo);
    }

    private static void createExtensionPricingInfo(Itinerary.PricingInfo.Extension.Provider provider, Itinerary.PricingInfo.Extension extensionPricingInfo) {
        extensionPricingInfo.setPricingSystem("DSAL{AS");
        extensionPricingInfo.setProvider(provider);
    }

    private static void createProvider(Itinerary.PricingInfo.Extension.Provider provider) {
        provider.setProviderCode("aSDAS");
    }

    private static void createFareInfos(Itinerary.PricingInfo.FareInfos.FareInfo fareInfo, Itinerary.PricingInfo.FareInfos fareInfos) {
        fareInfos.setFareInfo(fareInfo);
    }

    private static void createFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension extensionFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo fareInfo) {
        fareInfo.setDepartureDate(new Date(System.currentTimeMillis()));
        fareInfo.setFareReference("BNML");
        fareInfo.setSource("Private");
        fareInfo.setExtension(extensionFareInfo);
    }

    private static void createExtensionFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base baseFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos taxInfosFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos feeInfosFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total totalFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment segment, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger passengerFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension extensionFareInfo) {
        extensionFareInfo.setBase(baseFareInfo);
        extensionFareInfo.setCarrier("ADS");
        extensionFareInfo.setFeeInfos(feeInfosFareInfo);
        extensionFareInfo.setPassenger(passengerFareInfo);
        extensionFareInfo.setTaxes(taxInfosFareInfo);
        extensionFareInfo.setSegment(segment);
        extensionFareInfo.setTotal(totalFareInfo);
    }

    private static void createPassengerFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCsFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger passengerFareInfo) {
        passengerFareInfo.setPassengerAge(32);
        passengerFareInfo.setPassengerCode(3);
        passengerFareInfo.setPassengerReference(3341);
        passengerFareInfo.setRemoteSystemPTCs(remoteSystemPTCsFareInfo);
    }

    private static void createRemoteSystemPTCsFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCsFareInfo) {
        remoteSystemPTCsFareInfo.setPassengerCode("LOP");
    }

    private static void createSegment(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment segment) {
        segment.setFlightReference("AdASD");
    }

    private static void createTotalFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total totalFareInfo) {
        totalFareInfo.setAmount(37192.3F);
        totalFareInfo.setCurrencyCode("ADSA");
    }

    private static void createFeeInfosFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo feeInfoFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos feeInfosFareInfo) {
        feeInfosFareInfo.setFeeInfo(Arrays.asList(feeInfoFareInfo));
    }

    private static void createFeeInfoFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo feeInfoFareInfo) {
        feeInfoFareInfo.setCurrencyCode("ADASD");
        feeInfoFareInfo.setTaxCode("OPI");
    }

    private static void createTaxInfosFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo taxInfoFareInfo, Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos taxInfosFareInfo) {
        taxInfosFareInfo.setTaxInfo(Arrays.asList(taxInfoFareInfo));
    }

    private static void createTaxInfoFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo taxInfoFareInfo) {
        taxInfoFareInfo.setCurrencyCode("ADSAD");
        taxInfoFareInfo.setTaxCode("LKOP");
    }

    private static void createBaseFareInfo(Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base baseFareInfo) {
        baseFareInfo.setAmount(33.4F);
        baseFareInfo.setCurrencyCode("ASD");
    }

    private static void createTotalPricingInfo(Itinerary.PricingInfo.Total.Base baseTotal, Itinerary.PricingInfo.Total.Fees feesTotal, Itinerary.PricingInfo.Total.Taxes taxesTotal, Itinerary.PricingInfo.Total.Extension extensionTotal, Itinerary.PricingInfo.Total.Total2 total2, Itinerary.PricingInfo.Total totalPricingInfo) {
        totalPricingInfo.setBase(baseTotal);
        totalPricingInfo.setFees(feesTotal);
        totalPricingInfo.setTaxes(taxesTotal);
        totalPricingInfo.setTotal(total2);
        totalPricingInfo.setExtension(extensionTotal);
    }

    private static void createTotalInTotal(Itinerary.PricingInfo.Total.Total2 total2) {
        total2.setAmount(23.33F);
        total2.setCurrencyCode("ASDA");
    }

    private static void createExtensionTotal(Itinerary.PricingInfo.Total.Extension.FeeInfos feeInfosTotal, Itinerary.PricingInfo.Total.Extension.TaxInfos taxInfosTotal, Itinerary.PricingInfo.Total.Extension extensionTotal) {
        extensionTotal.setFeeInfos(feeInfosTotal);
        extensionTotal.setTaxInfos(taxInfosTotal);
    }

    private static void createTaxInfosTotal(Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo taxInfoTotal, Itinerary.PricingInfo.Total.Extension.TaxInfos taxInfosTotal) {
        taxInfosTotal.setTaxInfo(Arrays.asList(taxInfoTotal));
    }

    private static void createTaxInfoTotal(Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo taxInfoTotal) {
        taxInfoTotal.setBaseCurrency("DSKSA");
        taxInfoTotal.setTaxCode("BNM");
    }

    private static void createFeeInfosTotal(Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo feeInfoTotal, Itinerary.PricingInfo.Total.Extension.FeeInfos feeInfosTotal) {
        feeInfosTotal.setFeeInfo(Arrays.asList(feeInfoTotal));
    }

    private static void createFeeInfoTotal(Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo feeInfoTotal) {
        feeInfoTotal.setCurrencyCode("DD");
        feeInfoTotal.setTaxCode("NNN");
    }

    private static void createTaxesTotal(Itinerary.PricingInfo.Total.Taxes.Tax taxTotal, Itinerary.PricingInfo.Total.Taxes taxesTotal) {
        taxesTotal.setTax(Arrays.asList(taxTotal));
    }

    private static void createTaxTotal(Itinerary.PricingInfo.Total.Taxes.Tax taxTotal) {
        taxTotal.setAmount(4423.3F);
        taxTotal.setCurrencyCode("FFFF");
        taxTotal.setTaxCode("DDD");
    }

    private static void createfFeesTotal(Itinerary.PricingInfo.Total.Fees.Fee feeTotal, Itinerary.PricingInfo.Total.Fees.Fee feeTotal1, Itinerary.PricingInfo.Total.Fees feesTotal) {
        feesTotal.setFee(Arrays.asList(feeTotal,feeTotal1));
    }

    private static void createBaseTotal(Itinerary.PricingInfo.Total.Base baseTotal) {
        baseTotal.setAmount(23.5F);
        baseTotal.setCurrencyCode("VV");
    }

    private static void createFeeTotal(Itinerary.PricingInfo.Total.Fees.Fee feeTotal) {
        feeTotal.setTaxCode("AD");
        feeTotal.setCurrencyCode("DDD");
        feeTotal.setAmount(65.3F);
    }

    private static void createPTCs(Itinerary.PricingInfo.PTCs.PTC ptc, Itinerary.PricingInfo.PTCs ptCs) {
        ptCs.setPTC(ptc);
    }

    private static void createPTC(Itinerary.PricingInfo.PTCs.PTC.BasisCodes basisCodes, Itinerary.PricingInfo.PTCs.PTC.PassengerFare passengerFare, Itinerary.PricingInfo.PTCs.PTC.Extension extensionPTC, Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity passengerQuantity, Itinerary.PricingInfo.PTCs.PTC ptc) {
        ptc.setSource("MMM");
        ptc.setPassengerFare(passengerFare);
        ptc.setBasisCodes(basisCodes);
        ptc.setExtension(extensionPTC);
        ptc.setPassengerQuantity(passengerQuantity);
    }

    private static void createPassengerQuantity(Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity passengerQuantity) {
        passengerQuantity.setCode("DD");
        passengerQuantity.setQuantity(3);
    }

    private static void createExtensionPTC(Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger passengerPTC, Itinerary.PricingInfo.PTCs.PTC.Extension extensionPTC) {
        extensionPTC.setPassenger(passengerPTC);
    }

    private static void createPassengerPTC(Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCs, Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger passengerPTC) {
        passengerPTC.setPassengerAge(23);
        passengerPTC.setPassengerCode("DASD");
        passengerPTC.setPassengerReference(3);
        passengerPTC.setRemoteSystemPTCs(remoteSystemPTCs);
    }

    private static void createRemoteSystemPTCs(Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCs) {
        remoteSystemPTCs.setPassengerCode("DDDD");
    }

    private static void createPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes taxes, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees fees, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base base, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total totalPassangerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension extensionPassengerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare passengerFare) {
        passengerFare.setTaxes(taxes);
        passengerFare.setBase(base);
        passengerFare.setFees(fees);
        passengerFare.setTotal(totalPassangerFare);
        passengerFare.setExtension(extensionPassengerFare);
    }

    private static void createExtensionPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos feeInfosPassengerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos taxInfosPassengerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension extensionPassengerFare) {
        extensionPassengerFare.setFeeInfos(feeInfosPassengerFare);
        extensionPassengerFare.setTaxInfos(taxInfosPassengerFare);
    }

    private static void createTaxInfosPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo taxInfoPassengerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos taxInfosPassengerFare) {
        taxInfosPassengerFare.setTaxInfo(Arrays.asList(taxInfoPassengerFare));
    }

    private static void createTaxInfoPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo taxInfoPassengerFare) {
        taxInfoPassengerFare.setCurrencyCode("ADSSADSADSA");
        taxInfoPassengerFare.setTaxCode("DKD");
    }

    private static void createFeeInfosPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo feeInfoPassengerFare, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos feeInfosPassengerFare) {
        feeInfosPassengerFare.setFeeInfo(Arrays.asList(feeInfoPassengerFare));
    }

    private static void createFeeInfoPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo feeInfoPassengerFare) {
        feeInfoPassengerFare.setCurrencyCode("aDASD");
        feeInfoPassengerFare.setTaxCode("ADASD");
    }

    private static void createTotalPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total totalPassangerFare) {
        totalPassangerFare.setAmount(23.67F);
        totalPassangerFare.setCurrencyCode("ADASD");
    }

    private static void createBasePassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base base) {
        base.setAmount(23.44F);
        base.setCurrencyCode("VD");
    }

    private static void createFeesPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee fee, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees fees) {
        fees.setFee(Arrays.asList(fee));
    }

    private static void createFeePassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee fee) {
        fee.setAmount(89.1F);
        fee.setCurrencyCode("AD");
        fee.setTaxCode("ADDA");
    }

    private static void createTaxesPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax tax, Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes taxes) {
        taxes.setTax(Arrays.asList(tax));
    }

    private static void createTaxPassengerFare(Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax tax) {
        tax.setAmount(213.00f);
        tax.setTaxCode("DDD");
        tax.setCurrencyCode("333");
    }

    private static void createBasisCodes(Itinerary.PricingInfo.PTCs.PTC.BasisCodes basisCodes) {
        basisCodes.setBasisCode("AML");
    }

    private static void createAir(Itinerary.Air.Options options, Itinerary.Air.Extension extensionAir, Itinerary.Air air) {
        air.setOptions(options);
        air.setExtension(extensionAir);
        air.setDirection("VV");
        air.setTotalTravelTime("BNMVET");
    }

    private static void createExtensionAir(Itinerary.Air.Extension.Attributes attributes, Itinerary.Air.Extension extensionAir) {
        extensionAir.setAttributes(attributes);
    }

    private static void createAttributes(Itinerary.Air.Extension.Attributes attributes) {
        attributes.setTripType("BNK");
    }

    private static void createOptions(Itinerary.Air.Options.Option option, Itinerary.Air.Options options) {
        options.setOption(option);
    }

    private static void createOption(Itinerary.Air.Options.Option.Extension extensionOption, Itinerary.Air.Options.Option.Flight flight, Itinerary.Air.Options.Option.Flight flight2, Itinerary.Air.Options.Option option) {
        option.setFlight(Arrays.asList(flight,flight2));
        option.setExtension(extensionOption);
    }

    private static void createFlight(Itinerary.Air.Options.Option.Flight.Extension extensionFlight, Itinerary.Air.Options.Option.Flight flight, String vv, int i, int i2, int i3, String bkl) {
        flight.setArrivalDateTime(new Date(System.currentTimeMillis()));
        flight.setBookCode(vv);
        flight.setDepartureDateTime(new Date(System.currentTimeMillis()));
        flight.setFlightNumber(i);
        flight.setNumberInParty(i2);
        flight.setRPH(i3);
        flight.setStopQuantity(bkl);
        flight.setExtensionFlight(Arrays.asList(extensionFlight));
    }

    private static void createInventorySystemForExtensionFlight(Itinerary.Air.Options.Option.Flight.Extension.InventorySystem inventorySystem1) {
        inventorySystem1.setName("DDDDD");
    }

    private static void createTerminalInformationForExtensionFlight(Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation terminalInformation) {
        terminalInformation.setTerminal(88);
    }

    private static void createfFightSupplementalInfoForExtensionFlight(Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo flightSupplementalInfo) {
        flightSupplementalInfo.setFlyingTime("DSADDAS");
    }

    private static void createInventorySystemForExtensionOption(Itinerary.Air.Options.Option.Extension.InventorySystem inventorySystem) {
        inventorySystem.setName("ADSD");
    }

    private static void createExtensionOption(Itinerary.Air.Options.Option.Extension.InventorySystem inventorySystem, Itinerary.Air.Options.Option.Extension extensionOption) {
        extensionOption.setInventorySystem(inventorySystem);
        extensionOption.setSource("ADSD");
    }

    private static void createbBookingClassAvailabilityForExtensionflight(Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability bookingClassAvailability) {
        bookingClassAvailability.setBookCode("DAS");
        bookingClassAvailability.setBookDesig(33);
        bookingClassAvailability.setCabin("VALET");
    }

    private static void createExtensionFlight(Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo flightSupplementalInfo, Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability bookingClassAvailability, Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation terminalInformation, Itinerary.Air.Options.Option.Flight.Extension.InventorySystem inventorySystem1, Itinerary.Air.Options.Option.Flight.Extension extensionFlight) {
        extensionFlight.setEticket("aDAS");
        extensionFlight.setBookingClassAvailability(bookingClassAvailability);
        extensionFlight.setFlightSupplementalInfo(flightSupplementalInfo);
        extensionFlight.setInventorySystem(inventorySystem1);
        extensionFlight.setTerminalInformation(terminalInformation);
    }

    public static void main(String[] args) {
        ObjectFactory objectFactory = new ObjectFactory();
        Itinerary.Air.Options.Option.Extension extensionOption = objectFactory.createItineraryAirOptionsOptionExtension();
        Itinerary.Air.Options.Option.Extension.InventorySystem inventorySystem = objectFactory.createItineraryAirOptionsOptionExtensionInventorySystem();
        Itinerary.Air.Options.Option.Flight.Extension.FlightSupplementalInfo flightSupplementalInfo = objectFactory.createItineraryAirOptionsOptionFlightExtensionFlightSupplementalInfo();
        Itinerary.Air.Options.Option.Flight.Extension.BookingClassAvailability bookingClassAvailability = objectFactory.createItineraryAirOptionsOptionFlightExtensionBookingClassAvailability();
        Itinerary.Air.Options.Option.Flight.Extension.TerminalInformation terminalInformation = objectFactory.createItineraryAirOptionsOptionFlightExtensionTerminalInformation();
        Itinerary.Air.Options.Option.Flight.Extension.InventorySystem inventorySystem1 = objectFactory.createItineraryAirOptionsOptionFlightExtensionInventorySystem();
        Itinerary.Air.Options.Option.Flight.Extension extensionFlight = objectFactory.createItineraryAirOptionsOptionFlightExtension();
        Itinerary.Air.Options.Option.Flight flight = objectFactory.createItineraryAirOptionsOptionFlight();
        Itinerary.Air.Options.Option.Flight flight2 = objectFactory.createItineraryAirOptionsOptionFlight();
        Itinerary.Air.Options.Option option = objectFactory.createItineraryAirOptionsOption();
        Itinerary.Air.Extension.Attributes attributes = objectFactory.createItineraryAirExtensionAttributes();
        Itinerary.Air.Options options = objectFactory.createItineraryAirOptions();
        Itinerary.Air.Extension extensionAir = objectFactory.createItineraryAirExtension();
        Itinerary.PricingInfo.PTCs.PTC.BasisCodes basisCodes = objectFactory.createItineraryPricingInfoPTCsPTCBasisCodes();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes.Tax tax = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareTaxesTax();
        Itinerary.Air air = objectFactory.createItineraryAir();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Taxes taxes = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareTaxes();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees.Fee fee = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareFeesFee();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Fees fees = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareFees();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Base base = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareBase();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Total totalPassangerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareTotal();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos.FeeInfo feeInfoPassengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareExtensionFeeInfosFeeInfo();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.FeeInfos feeInfosPassengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareExtensionFeeInfos();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos.TaxInfo taxInfoPassengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareExtensionTaxInfosTaxInfo();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension.TaxInfos taxInfosPassengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareExtensionTaxInfos();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare.Extension extensionPassengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFareExtension();
        Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCs = objectFactory.createItineraryPricingInfoPTCsPTCExtensionPassengerRemoteSystemPTCs();
        Itinerary.PricingInfo.PTCs.PTC.PassengerFare passengerFare = objectFactory.createItineraryPricingInfoPTCsPTCPassengerFare();
        Itinerary.PricingInfo.PTCs.PTC.Extension.Passenger passengerPTC = objectFactory.createItineraryPricingInfoPTCsPTCExtensionPassenger();
        Itinerary.PricingInfo.PTCs.PTC.Extension extensionPTC = objectFactory.createItineraryPricingInfoPTCsPTCExtension();
        Itinerary.PricingInfo.PTCs.PTC.PassengerQuantity passengerQuantity = objectFactory.createItineraryPricingInfoPTCsPTCPassengerQuantity();
        Itinerary.PricingInfo.PTCs.PTC ptc = objectFactory.createItineraryPricingInfoPTCsPTC();
        Itinerary.PricingInfo.PTCs ptCs = objectFactory.createItineraryPricingInfoPTCs();
        Itinerary.PricingInfo.Total.Base baseTotal = objectFactory.createItineraryPricingInfoTotalBase();
        Itinerary.PricingInfo.Total.Fees.Fee feeTotal = objectFactory.createItineraryPricingInfoTotalFeesFee();
        Itinerary.PricingInfo.Total.Fees.Fee feeTotal1 = objectFactory.createItineraryPricingInfoTotalFeesFee();
        Itinerary.PricingInfo.Total.Fees feesTotal = objectFactory.createItineraryPricingInfoTotalFees();
        Itinerary.PricingInfo.Total.Taxes.Tax taxTotal = objectFactory.createItineraryPricingInfoTotalTaxesTax();
        Itinerary.PricingInfo.Total.Taxes taxesTotal = objectFactory.createItineraryPricingInfoTotalTaxes();
        Itinerary.PricingInfo.Total.Extension.FeeInfos.FeeInfo feeInfoTotal = objectFactory.createItineraryPricingInfoTotalExtensionFeeInfosFeeInfo();
        Itinerary.PricingInfo.Total.Extension.FeeInfos feeInfosTotal = objectFactory.createItineraryPricingInfoTotalExtensionFeeInfos();
        Itinerary.PricingInfo.Total.Extension.TaxInfos.TaxInfo taxInfoTotal = objectFactory.createItineraryPricingInfoTotalExtensionTaxInfosTaxInfo();
        Itinerary.PricingInfo.Total.Extension.TaxInfos taxInfosTotal = objectFactory.createItineraryPricingInfoTotalExtensionTaxInfos();
        Itinerary.PricingInfo.Total.Extension extensionTotal = objectFactory.createItineraryPricingInfoTotalExtension();
        Itinerary.PricingInfo.Total.Total2 total2 = objectFactory.createItineraryPricingInfoTotalTotal();
        Itinerary.PricingInfo.Total totalPricingInfo = objectFactory.createItineraryPricingInfoTotal();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Base baseFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionBase();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos.TaxInfo taxInfoFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionTaxInfosTaxInfo();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.TaxInfos taxInfosFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionTaxInfos();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos.FeeInfo feeInfoFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionFeeInfosFeeInfo();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.FeeInfos feeInfosFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionFeeInfos();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Total totalFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionTotal();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Segment segment = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionSegment();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger.RemoteSystemPTCs remoteSystemPTCsFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionPassengerRemoteSystemPTCs();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension.Passenger passengerFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtensionPassenger();
        Itinerary.PricingInfo.FareInfos.FareInfo.Extension extensionFareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfoExtension();
        Itinerary.PricingInfo.FareInfos.FareInfo fareInfo = objectFactory.createItineraryPricingInfoFareInfosFareInfo();
        Itinerary.PricingInfo.FareInfos fareInfos = objectFactory.createItineraryPricingInfoFareInfos();
        Itinerary.PricingInfo.Extension.Provider provider = objectFactory.createItineraryPricingInfoExtensionProvider();
        Itinerary.PricingInfo.Extension extensionPricingInfo = objectFactory.createItineraryPricingInfoExtension();
        Itinerary.PricingInfo pricingInfo = objectFactory.createItineraryPricingInfo();
        Itinerary.TicketingInfo ticketingInfo = objectFactory.createItineraryTicketingInfo();
        Itinerary itinerary = objectFactory.createItinerary();

        createInventorySystemForExtensionOption(inventorySystem);
        createExtensionOption(inventorySystem, extensionOption);
        createfFightSupplementalInfoForExtensionFlight(flightSupplementalInfo);
        createbBookingClassAvailabilityForExtensionflight(bookingClassAvailability);
        createTerminalInformationForExtensionFlight(terminalInformation);
        createInventorySystemForExtensionFlight(inventorySystem1);
        createExtensionFlight(flightSupplementalInfo,
                bookingClassAvailability,
                terminalInformation,
                inventorySystem1,
                extensionFlight);
        createFlight(extensionFlight, flight, "VV", 1111, 3, 3, "BKL");
        createFlight(extensionFlight, flight2, "GJFDSA", 1211, 4, 87, "aSDASD");
        createOption(extensionOption, flight, flight2, option);
        createOptions(option, options);
        createAttributes(attributes);
        createExtensionAir(attributes, extensionAir);
        createAir(options, extensionAir, air);
        createBasisCodes(basisCodes);
        createTaxPassengerFare(tax);
        createTaxesPassengerFare(tax, taxes);
        createFeePassengerFare(fee);
        createFeesPassengerFare(fee, fees);
        createBasePassengerFare(base);
        createTotalPassengerFare(totalPassangerFare);
        createFeeInfoPassengerFare(feeInfoPassengerFare);
        createFeeInfosPassengerFare(feeInfoPassengerFare, feeInfosPassengerFare);
        createTaxInfoPassengerFare(taxInfoPassengerFare);
        createTaxInfosPassengerFare(taxInfoPassengerFare, taxInfosPassengerFare);
        createExtensionPassengerFare(feeInfosPassengerFare, taxInfosPassengerFare, extensionPassengerFare);
        createPassengerFare(taxes, fees, base, totalPassangerFare, extensionPassengerFare, passengerFare);
        createRemoteSystemPTCs(remoteSystemPTCs);
        createPassengerPTC(remoteSystemPTCs, passengerPTC);
        createExtensionPTC(passengerPTC, extensionPTC);
        createPassengerQuantity(passengerQuantity);
        createPTC(basisCodes, passengerFare, extensionPTC, passengerQuantity, ptc);
        createPTCs(ptc, ptCs);
        createBaseTotal(baseTotal);
        createFeeTotal(feeTotal);
        createFeeTotal(feeTotal1);
        createfFeesTotal(feeTotal, feeTotal1, feesTotal);
        createTaxTotal(taxTotal);
        createTaxesTotal(taxTotal, taxesTotal);
        createFeeInfoTotal(feeInfoTotal);
        createFeeInfosTotal(feeInfoTotal, feeInfosTotal);
        createTaxInfoTotal(taxInfoTotal);
        createTaxInfosTotal(taxInfoTotal, taxInfosTotal);
        createExtensionTotal(feeInfosTotal, taxInfosTotal, extensionTotal);
        createTotalInTotal(total2);
        createTotalPricingInfo(baseTotal, feesTotal, taxesTotal, extensionTotal, total2, totalPricingInfo);
        createBaseFareInfo(baseFareInfo);
        createTaxInfoFareInfo(taxInfoFareInfo);
        createTaxInfosFareInfo(taxInfoFareInfo, taxInfosFareInfo);
        createFeeInfoFareInfo(feeInfoFareInfo);
        createFeeInfosFareInfo(feeInfoFareInfo, feeInfosFareInfo);
        createTotalFareInfo(totalFareInfo);
        createSegment(segment);
        createRemoteSystemPTCsFareInfo(remoteSystemPTCsFareInfo);
        createPassengerFareInfo(remoteSystemPTCsFareInfo, passengerFareInfo);
        createExtensionFareInfo(baseFareInfo, taxInfosFareInfo,
                feeInfosFareInfo, totalFareInfo,
                segment, passengerFareInfo, extensionFareInfo);
        createFareInfo(extensionFareInfo, fareInfo);
        createFareInfos(fareInfo, fareInfos);
        createProvider(provider);
        createExtensionPricingInfo(provider, extensionPricingInfo);
        createPricingInfo(ptCs, totalPricingInfo, fareInfos, extensionPricingInfo, pricingInfo);
        createTicketingInfo(ticketingInfo);
        createItinerary(air, pricingInfo, ticketingInfo, itinerary);

        try {
            JAXBContext context = JAXBContext.newInstance(Itinerary.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(itinerary, new FileOutputStream("TaskXml/src/main/resources/marshaling.xml"));

        } catch (JAXBException exception) {
            exception.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
