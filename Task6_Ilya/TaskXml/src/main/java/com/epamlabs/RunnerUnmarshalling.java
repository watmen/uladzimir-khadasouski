package com.epamlabs;

import com.epamlabs.xmlMarshalling.Itinerary;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class RunnerUnmarshalling {
    public static void main(String[] args) {
        try {
            File file = new File("TaskXml/file.xml");
            JAXBContext jContext = null;
            jContext = JAXBContext.newInstance(Itinerary.class);
            Unmarshaller unmarshallerObj = jContext.createUnmarshaller();
            Itinerary itinerary = (Itinerary) unmarshallerObj.unmarshal(file);

            JAXBContext context = JAXBContext.newInstance(Itinerary.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(itinerary, System.out);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
