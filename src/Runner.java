import java.util.Scanner;

public class Runner {
    private final static int FIRST_NUMBER_OF_FIBONACCI_ROW = 1;
    private final static int POSITIVE_LIMIT = 0;
    private final static int RIGHT_LIMIT_OF_ALGORITHM_ID = 2;
    private final static int RIGHT_LIMIT_OF_LOOP_ID = 3;

    private static void calculateFibonacci(int loopType, int rowLength){
        int firstPreviousNumber = 1;
        int secondPreviousNumber = 0;
        int numberOfFibonacciRow;
        int i = 1;
        System.out.print(FIRST_NUMBER_OF_FIBONACCI_ROW + " ");
        switch (loopType) {
            case  (1):
                while (i < rowLength){
                    numberOfFibonacciRow = firstPreviousNumber + secondPreviousNumber;
                    System.out.print(numberOfFibonacciRow + " ");
                    secondPreviousNumber = firstPreviousNumber;
                    firstPreviousNumber = numberOfFibonacciRow;
                    i++;
                }
                break;
            case (2):
                if (rowLength > 1){
                    do {
                        numberOfFibonacciRow = firstPreviousNumber + secondPreviousNumber;
                        System.out.print(numberOfFibonacciRow + " ");
                        secondPreviousNumber = firstPreviousNumber;
                        firstPreviousNumber = numberOfFibonacciRow;
                        i++;
                    }while (i < rowLength);
                }
                break;
            case (3):
                for (; i < rowLength; i++){
                    numberOfFibonacciRow = firstPreviousNumber + secondPreviousNumber;
                    System.out.print(numberOfFibonacciRow + " ");
                    secondPreviousNumber = firstPreviousNumber;
                    firstPreviousNumber = numberOfFibonacciRow;
                }
                break;
        }
        System.out.println();
    }

    private static void calculateFactorial(int loopType, int number){
        long result = 1;
        int i = 1;
        switch (loopType) {
            case  (1):
                while (i <= number){
                    result*=i;
                    i++;
                }
                System.out.print(result);
                break;
            case (2):
                do{
                    result*=i;
                    i++;
                }
                while (i <= number);
                System.out.print(result);
                break;
            case (3):
                for ( ;i <=number; i++){
                    result*=i;
                }
                System.out.print(result);
                break;
        }
        System.out.println();
    }

    private static void algorithm(int algorithmId, int loopType, int number) {
        if (algorithmId == 1) {
            calculateFibonacci(loopType, number);
        }
        else{
            calculateFactorial(loopType,number);
        }
    }

    private static boolean idOfAlgorithmIsWrong(int algorithmId){
        return algorithmId < POSITIVE_LIMIT || algorithmId > RIGHT_LIMIT_OF_ALGORITHM_ID ;
    }

    private static boolean idOfLoopIsWrong(int loopType){
        return loopType < POSITIVE_LIMIT || loopType > RIGHT_LIMIT_OF_LOOP_ID ;
    }

    private static boolean numberIsNegativeOrZero(int number){
        return number <= POSITIVE_LIMIT ;
    }

    public static void main(String[] args){
        int communicationWithUser;
        do{
            Scanner in = new Scanner(System.in);
            int algorithmId;
            int loopType;
            int number;
            System.out.println("Input id of algorithm");
            if(in.hasNextInt()){
                algorithmId = in.nextInt();
                if (idOfAlgorithmIsWrong(algorithmId)){
                    System.out.println("There is no such algorithm");
                }
                else{
                    System.out.println("Input id of cycle");
                    if (in.hasNextInt()){
                        loopType = in.nextInt();
                        if (idOfLoopIsWrong(loopType)){
                            System.out.println("There is no such loop");
                        }
                        else{
                            System.out.println("Input number");
                            if (in.hasNextInt()){
                                number = in.nextInt();
                                if (numberIsNegativeOrZero(number)){
                                   System.out.println("Number is wrong");
                                }
                                else {
                                    algorithm(algorithmId,loopType,number);
                                }

                            }
                            else{
                                System.out.println("You didn't input right format of number");
                            }
                        }
                    }
                    else{
                        System.out.println("You didn't input right format of number");
                    }
                }
            }
            else{
                System.out.println("You didn't input right format of number");
            }
            in = new Scanner(System.in);
            System.out.println("Do you want to continue working with application?\n" +
                    "1-yes\n" +
                    "2-no");
            communicationWithUser = in.nextInt();
        }while (communicationWithUser == 1);
    }
}
