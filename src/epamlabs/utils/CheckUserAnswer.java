package epamlabs.utils;

import epamlabs.constants.Constants;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class CheckUserAnswer describes model to check user answer
 * @author Uladzimir Khodosovckiy
 */
public class CheckUserAnswer {
    /**
     * This field contains number, which is the last from the proposed options
     */
    private static final int RIGHT_LIMIT_OF_USER_ANSWER = 2;
    /**
     * This field contains message to user that he/she inputs wrong answer
     */
    private static final String MESSAGE_WRONG_ANSWER_OF_USER = "You input wrong number.\n" +
            "Re-enter, please ";

    /**
     * This method check answer of user
     * @return user answer right or not
     */
    private static boolean wrongAnswerOfUser(int answer){
        return answer < Constants.POSITIVE_LIMIT || answer > RIGHT_LIMIT_OF_USER_ANSWER;
    }

    /**
     * This ask to user to input right answer
     * @throws InputMismatchException in case user input letters
     * @param answer = answer, which user input
     * @return answer of user
     */
    public static int inputRightAnswer(int answer) throws InputMismatchException {
        Scanner in = new Scanner(System.in);
        while (wrongAnswerOfUser(answer)){
            System.out.println(MESSAGE_WRONG_ANSWER_OF_USER);
            answer = in.nextInt();
        }
        return answer;
    }
}
