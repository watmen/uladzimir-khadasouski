package epamlabs.monsters;

import epamlabs.interfaces.AbstractMonster;

import java.util.Random;

/**
 * Class StrongMonster describes model of strong monster in simple game
 * - extends – AbstractMonster
 * @author Uladzimir Khodosovckiy
 */
public class StrongMonster extends AbstractMonster {
    /**
     * This field contains extra damage of monster
     */
    private static final int EXTRA_DAMAGE = 30;

    /**
     * Constructor
     * @param health - health of monster
     * @param damage - damage of monster
     * @param experienceAfterKill - experience after kill the monster
     */
    public StrongMonster(int health, int damage,int experienceAfterKill) {
        super(health, damage,experienceAfterKill);
    }

    /**
     * getter
     * @return damage of monster + extra damage with some chance
     */
    @Override
    public int getDamage(){
        int resultDamage;
        Random random = new Random();
        if (random.nextBoolean()){
            resultDamage = getDamage() + EXTRA_DAMAGE;
        }
        else{
            resultDamage = super.getDamage();
        }
        return resultDamage;
    }
}
