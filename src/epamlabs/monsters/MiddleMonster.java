package epamlabs.monsters;

import epamlabs.interfaces.AbstractMonster;

/**
 * Class MiddleMonster describes model of middle monster in simple game
 * - extends – AbstractMonster
 * @author Uladzimir Khodosovckiy
 */
public class MiddleMonster extends AbstractMonster {

    /**
     * Constructor
     * @param health - health of monster
     * @param damage - damage of monster
     * @param experienceAfterKill - experience after kill the monster
     */
    public MiddleMonster(int health, int damage,int experienceAfterKill) {
        super(health, damage,experienceAfterKill);
    }
}
