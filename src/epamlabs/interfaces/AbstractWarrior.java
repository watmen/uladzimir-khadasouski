package epamlabs.interfaces;

import epamlabs.constants.Constants;
import epamlabs.equipment.Weapon;
import epamlabs.skills.Skill;
import epamlabs.utils.CheckUserAnswer;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class AbstractWarrior describes model of warrior in simple game
 * - implements IBoost, IFight
 * @author Uladzimir Khodosovckiy
 */
public abstract class AbstractWarrior implements IBoost, IFight {

    /**
     * This field amount of experience to up level of the warrior
     */
    private static final int AMOUNT_OF_EXPERIENCE_TO_LEVEL_UP = 100;
    /**
     * This field amount of experience after level up
     */
    private static final int ELEMENTARY_EXPERIENCE = 0;
    /**
     * This field contains message to user, which reports that warrior doesn't have enough mana
     */
    private static final String MESSAGE_NOT_ENOUGH_MANA = "You don't have enough mana!";
    /**
     * This field contains message to user, which reports that the warrior died
     */
    private static final String MESSAGE_THAT_WARRIOR_DIED = "You died!";
    /**
     * This field contains message to user, which reports that user killed the monster
     */
    private static final String MESSAGE_THAT_WARRIOR_KILLED_MONSTER = "You killed the monster";
    /**
     * This field contains message to user, which shows health of the warrior
     */
    private static final String MESSAGE_YOUR_HEALTH = "Your health: ";
    /**
     * This field contains message to user, which shows experience of the warrior
     */
    private static final String MESSAGE_YOUR_EXPERIENCE = "Your experience: ";
    /**
     * This field contains message to user, which shows level of the warrior
     */
    private static final String MESSAGE_YOUR_LEVEL = "Your level: ";
    /**
     * This field contains message to user, which shows weapon of the warrior
     */
    private static final String MESSAGE_YOUR_WEAPON = "Your weapon: ";
    /**
     * This field contains message to user, which shows damage of the warrior
     */
    private static final String MESSAGE_YOUR_DAMAGE = "Your damage: ";
    /**
     * This field contains message to user, which shows mana of the warrior
     */
    private static final String MESSAGE_YOUR_MANA = "Your mana: ";

    /**
     * This field contains information about health of the warrior
     */
    private int health;
    /**
     * This field contains information about experience of the warrior
     */
    private int experience;
    /**
     * This field contains information about level of the warrior
     */
    private int level;
    /**
     * This field contains information about weapon of the warrior
     */
    private Weapon weapon;
    /**
     * This field contains information about damage of the warrior
     */
    private int damage;
    /**
     * This field contains information about mana of the warrior
     */
    private int mana;
    /**
     * This field contains information about skill of warrior
     */
    private Skill skill;

    /**
     * Constructor without params
     */
    public AbstractWarrior(){}

    /**
     * Constructor
     * @param health - health of warrior
     * @param experience - experience of warrior
     * @param level - level of warrior
     * @param weapon - weapon of warrior
     * @param damage - damage of warrior
     * @param mana - mana of warrior
     * @param skill - skill of warrior
     */
    public AbstractWarrior(int health, int experience, int level, Weapon weapon, int damage, int mana, Skill skill) {
        this.health = health;
        this.experience = experience;
        this.level = level;
        this.weapon = weapon;
        this.damage = damage;
        this.mana = mana;
        this.skill = skill;
    }

    /**
     * getter
     * @return mana of warrior
     */
    public int getMana() {
        return mana;
    }

    /**
     * setter
     * @param mana - mana of warrior
     */
    public void setMana(int mana) {
        this.mana = mana;
    }

    /**
     * getter
     * @return health of warrior
     */
    public int getHealth() {
        return health;
    }

    /**
     * setter
     * @param health - health of warrior
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * getter
     * @return damage of warrior + damage of his weapon
     */
    public int getDamage() {
        return damage + weapon.getDamage();
    }

    /**
     * setter
     * @param damage - damage of warrior
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * getter
     * @return experience of warrior
     */
    public int getExperience() {
        return experience;
    }

    /**
     * setter
     * @param experience - experience of warrior
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * getter
     * @return level of warrior
     */
    public int getLevel() {
        return level;
    }

    /**
     * setter
     * @param level - level of warrior
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * getter
     * @return weapon of warrior
     */
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * setter
     * @param weapon - weapon of warrior
     */
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    /**
     * This method check health of warrior
     * @return warrior died or alive
     */
    private boolean warriorDead(){
        return getHealth() <= Constants.POSITIVE_LIMIT;
    }

    /**
     * This method recover health and mana of warrior to standard number
     * @param startHealth - full health of warrior
     * @param startMana - full mana of warrior
     */
    private void healthAndManaRecovery(int startHealth,int startMana){
        setHealth(startHealth);
        setMana(startMana);
    }

    /**
     * This method check experience of warrior for level up
     * @return warrior can up level or not
     */
    public boolean canWarriorUpLevel(){
        return getExperience() >= AMOUNT_OF_EXPERIENCE_TO_LEVEL_UP;
    }

    /**
     * This method mana of warrior to use his skill
     * @return warrior can use skill or not
     */
    private boolean checkManaForSkill(){
        return getMana() > skill.getManaCost();
    }


    /**
     * This method print health, mana and damage of warrior
     */
    private void printHealthManaAndDamageOfWarrior(){
        System.out.println(MESSAGE_YOUR_HEALTH + health + Constants.SEMICOLON +
                MESSAGE_YOUR_MANA + mana + Constants.SEMICOLON +
                MESSAGE_YOUR_DAMAGE + damage);
    }

    /**
     * This method describes basic logic for level up
     */
    @Override
    public void upLevel(){
        int currentLevel = getLevel();
        int nextLevel = ++currentLevel;
        setLevel(nextLevel);
        setExperience(ELEMENTARY_EXPERIENCE);
    }

    /**
     * This method describes basic logic for fight with monster
     * @throws InputMismatchException in case user input letters
     * @param monster - monster, which user choose for fight
     */
    @Override
    public void killMonster(AbstractMonster monster) throws InputMismatchException {
        Scanner in = new Scanner(System.in);
        int startHealth = getHealth();
        int startMana = getMana();
        while (monster.monsterAlive()){
            printHealthManaAndDamageOfWarrior();
            monster.printHealthAndDamageOfMonster();
            System.out.println("Do you want to use your skill: " + skill + " \n" +
                    "1 - yes\n" +
                    "2 - no");
            int answer = in.nextInt();
            answer = CheckUserAnswer.inputRightAnswer(answer);
            if(answer == 1){
                if (checkManaForSkill()){
                    setDamage(getDamage() + skill.getDamage());
                    setMana(getMana()-skill.getManaCost());
                }
                else{
                    System.out.println(MESSAGE_NOT_ENOUGH_MANA);
                }
            }
            monster.setHealth(monster.getHealth() - getDamage());
            setHealth(getHealth() - monster.getDamage());
            if(warriorDead()){
                System.out.println(MESSAGE_THAT_WARRIOR_DIED);
                break;
            }
        }
        if(monster.getHealth()<=Constants.POSITIVE_LIMIT){
            setExperience(getExperience()+monster.getExperienceAfterKill());
            System.out.println(MESSAGE_THAT_WARRIOR_KILLED_MONSTER);
        }
        healthAndManaRecovery(startHealth,startMana);
    }

    /**
     * This method shows state of warrior
     * @return object state
     */
    @Override
    public String toString(){
        return new StringBuilder(MESSAGE_YOUR_HEALTH).
                append(health + Constants.SEMICOLON).
                append(MESSAGE_YOUR_EXPERIENCE).
                append(experience + Constants.SEMICOLON).
                append(MESSAGE_YOUR_LEVEL).
                append(level + Constants.SEMICOLON).
                append(MESSAGE_YOUR_WEAPON).
                append(weapon.getType() + Constants.SEMICOLON).
                append(MESSAGE_YOUR_DAMAGE).
                append(getDamage() + Constants.SEMICOLON).
                append(MESSAGE_YOUR_MANA).
                append(mana).
                toString();
    }
}
