package epamlabs.constants;

/**
 * Class Constants consists constant, which are used in all program
 * @author Uladzimir Khodosovckiy
 */
public class Constants {
    /**
     * This field contains number, which needs to check number to positivity
     */
    public static final int POSITIVE_LIMIT = 0;
    /**
     * This field contains sing semicolon
     */
    public static final String SEMICOLON = " ;";
}
